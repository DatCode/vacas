function t=y2kSecToSerial(y2ksec)
%% y2kSecToSeria
% 
%   function t=y2kSecToSerial(y2ksec)
% 
% Convierte una cantidad de tiempo en formato j2ksec a formato serial.
%
%  INPUTS:
%  yksec                =Tiempo en j2ksec
%  RETURNS:
%  t = tiempo en formato serial.
    t=(60*60*12.000258806679+y2ksec)/86400+datenum('2000','yyyy');
end