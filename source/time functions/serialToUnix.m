function unix=serialToUnix(serial)
%% serialToUnix
% 
%   function unix=serialToUnix(serial)
% 
% Convierte una cantidad de tiempo en formato serial a formato unix.
%
%  INPUTS:
%  serial                 =Tiempo en serial
%  RETURNS:
%  unix = Tiempo en unix.
    
    unix=(serial-datenum('1970','yyyy'))*86400;
end