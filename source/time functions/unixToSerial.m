function t=unixToSerial(tt)
%% unixToSerial
% 
%   function t=unixToSerial(tt)
% 
% Convierte una cantidad de tiempo en formato unix a formato serial.
%
%  INPUTS:
%  tt                 =Tiempo en unix
%  RETURNS:
%  t = tiempo en formato serial.
    t=double(tt)/3600/24+datenum(1970,1,1,0,0,0);
end
