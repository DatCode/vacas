function t=serialToY2kSec(serial)
%% serialToY2kSec
% 
%   function t=serialToY2kSec(serial)
% 
% Convierte una cantidad de tiempo en formato serial a formato j2ksec.
%
%  INPUTS:
%  serial                 =Tiempo en serial
%  RETURNS:
%  t = tiempo en formato j2ksec.
    fecha = datevec(serial);

    anio=fecha(1);
    mes=fecha(2);
    dia=fecha(3);
    hh=fecha(4);
    mm=fecha(5);
    ss=fecha(6);
    
    t=(datenum(anio,mes,dia,hh,mm,ss)-datenum('2000','yyyy'))*86400-3600*12.000258806679;
end