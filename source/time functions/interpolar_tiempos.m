function serial=interpolar_tiempos(serial)
% valores_unicos = unique(serial);
valores_unicos = serial([true;diff(serial(:))>0]);
serial= interp1(1:3:length(valores_unicos)*3,valores_unicos,1:length(serial));
% for i = 1:length(valores_unicos)
%     idx = serial==valores_unicos(i);
%     cantidad = sum(idx);
%     serial(idx)=linspace(valores_unicos(i),valores_unicos(i)+datenum(0,0,0,0,0,0.99),cantidad)';
% end
end