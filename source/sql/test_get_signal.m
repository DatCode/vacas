%Se espera: select eje_x,eje_y,eje_z,it from vacas_db.acc where (it between
%t1 and t2) and id_monatje = id_escogida

% figure;plot(unixToSerial(fast.epoch),fast.IMU.accel);datetick('x',13,'keeplimits');datestr([min(unixToSerial(fast.epoch)) max(unixToSerial(fast.epoch))])


addpath('../');
addpath('../sql functions');
addpath('../time functions');
request.coly = {'eje_x';'eje_y';'eje_z'};
request.colx = {'it'};

request.tabla = 'acc';
request.db = 'vacas_db';
anio = 2015;
mes = 11;
dia = 08;
hora = 19;
t1 = utcDate(anio,mes,dia,hora,00,00);
t2= utcDate(anio,mes,dia,hora+8,00,00);
% t1 = inicio(1);
% t2=fin(1);
request.range_x=[t1 t2];
request.fuente_val = '5012';
% request.fuente_val = predial{1};
request.fuente = 'predial';

setdbprefs('FetchInBatches','no')

tic;signal_struct = get_signal(request);toc;

figure;
 plot(signal_struct.it,signal_struct.eje_x,'r',...
    signal_struct.it,signal_struct.eje_y,'g',...
    signal_struct.it,signal_struct.eje_z,'b');
legend('x','y','z');
title(request.tabla);
ylabel(request.tabla);
xlabel('tiempo');
datetick('x',13,'keeplimits');
