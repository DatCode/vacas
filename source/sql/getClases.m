function clases = getClases()
sql = 'select id_clase,clase,alias from vacas_db.clase_etiqueta';
global cow_connector;
data = cow_connector.query(sql);
clases=[];
if ~isempty(data)
    clases.id = data(:,1);
    clases.clase = data(:,2);
    clases.alias = data(:,3);
end
end