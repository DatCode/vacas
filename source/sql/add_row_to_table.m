function add_row_to_table(handle,place)
if strcmpi(place,'top')
    d = get(handle,'data');
    d = [cell(1,length(d));d];
    set(handle,'data',d)
else
    d = get(handle,'data');
    d = [d;cell(1,length(d))];
    set(handle,'data',d)
end
end