function predial=getPredialFromRequest(request)
global cow_connector;
predial = [];
switch(lower(request.fuente))
    case 'predial'
        predial = request.fuente_val;
    case 'mac'
        sql = ['select v.id_predial from vacas_db.montaje as m '...
            'inner join vacas_db.vacas as v on m.id_vaca = v.id_sag '...
            'where m.id_nodo = ' request.fuente_val];
        predial = cow_connector.query(sql);
    case 'id montaje'
        sql = ['select v.id_predial from vacas_db.montaje as m '...
            'inner join vacas_db.vacas as v on m.id_vaca = v.id_sag '...
            'where m.id_montaje = ' num2str(request.fuente_val)];
        predial = cow_connector.query(sql);
    case 'SAG'
        sql = ['select id_predial from vacas_db.vacas where id_sag = ' request.fuente_val];
        predial = cow_connector.query(sql);
end
if iscell(predial)
    predial = num2str(predial{1});
end
end