function [con,k] = try_to_connect(dbSource,user,pass,limit,timeout)
    k=1;
    logintimeout(timeout);
    con = database(dbSource,user,pass);
    while(~isconnection(con) && k<limit)
       con = database(dbSource,user,pass); 
       k=k+1;
    end
end