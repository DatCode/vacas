function subclase=getSubclaseByIndex(index)
global cow_connector
results = cow_connector.query(['select id_subclase,id_clase,subclase from '...
    'vacas_db.subclase_etiqueta where id_subclase = ' num2str(index)]);

subclase.id = results{1,1};
subclase.id_clase = results(:,2);
subclase.subclase = results(:,3);
