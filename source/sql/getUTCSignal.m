function signal_struct=getUTCSignal(request)
% global con;
con = vconnector(getTxtProperty('vacas_conf.txt','dbsource'),'guest','helloeverybody','vacas_db');
% con = vconnector('local','jose','59575351','vacas_db');
con.dbname = 'vacas_db';
con.mysql_user = 'guest';
con.mysql_pass = 'helloeverybody';
signal_struct = [];

%Formato de tiempo por defecto
if ~isfield(request,'time_format')
    request.time_format = 'datenum';
end

try
    %% Obtiene el id montaje a partir del id predial
    timestamp_format = 'yyyy-mm-dd HH:MM:SS';
    if(strcmpi(request.time_format,'utc'))
        ranges_datenum = [{datestr(utc2datenum(request.range_x(1)),timestamp_format)} ...
            {datestr( utc2datenum(request.range_x(2)),timestamp_format)}];
        ranges = [{utcToStr(request.range_x(1))} {utcToStr(request.range_x(2))}];
    elseif (strcmpi(request.time_format,'datenum'))
        ranges_datenum = [{datestr(request.range_x(1),timestamp_format)} ...
            {datestr(request.range_x(2),timestamp_format)}];
        ranges = [{utcToStr(datenum2Utc(request.range_x(1)))}...
            {utcToStr(datenum2Utc(request.range_x(2)))}];
    end
    request.fuente = lower(request.fuente);
    switch (request.fuente)
        case 'predial'
            id_montaje = get_idmontaje(con,'mode','default','range',...
                ranges_datenum,'predial',request.fuente_val,'retire','all');
        case 'sag'
            id_montaje = get_idmontaje(con,'mode','default','range',...
                ranges_datenum,'sag',request.fuente_val,'retire','all');
        case 'mac'
            id_montaje = get_idmontaje(con,'mode','default','range',...
                ranges_datenum,'mac',request.fuente_val,'retire','all');
        case 'id montaje'
            id_montaje = request.fuente_val;
    end
    
    % id_montaje = get_idmontaje(con,'predial',request.predial,'range',ranges,'retire','all');
    if iscell(id_montaje)
        if ischar(id_montaje{1})
            if strcmpi(id_montaje(1),'No Data')
                %No hay data
                signal_struct = [];
                warning(['No existe montaje registrado relacionado a la vaca entre las fechas ' ranges_datenum{1} ' y ' ranges_datenum{2}]);
                return;
            end
        end
    end
    %% Obtiene los datos
    where_cols = [request.colx;repmat({'id_montaje'},length(1),1)];
    operators = [{'between'};repmat({'='},length(id_montaje),1)];
    ranges={ranges;id_montaje(:)};
    where = con.build_where(where_cols,operators,ranges,[{'and'};repmat({'or'},length(id_montaje)-2,1)]);
    cols =[request.coly;request.colx];
    select = con.build_select(cols,request.tabla,where);
    datos = con.query(select);
    
    %% Procesa y guarda los datos
    signal_struct.id_montaje = id_montaje;
    
    if(iscell(datos))
        if ischar(datos{1})
            return;
        end
    end
    signal_struct = con.get_struct(datos,cols);
    signal_struct.id_montaje = id_montaje;
    signal_struct.it = utc2datenum(signal_struct.it);
    
catch
    
end
end

