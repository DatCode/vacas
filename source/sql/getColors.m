function colores=getColors(color)
global cow_connector;
sql = 'Select id_color, color from vacas_db.color ';
if nargin>0
   sql = [sql ' where color = "' color '";']; 
end
colores=cow_connector.get_struct(cow_connector.query(sql),{'id_color','color'});
end