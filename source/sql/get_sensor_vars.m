function vars = get_sensor_vars(sensor)
sensor = lower(sensor);
vars = [];
switch(sensor)
    case 'acelerometro'
        vars = {'eje_x','eje_y','eje_z'};
    case 'giroscopio'
        vars = {'eje_x','eje_y','eje_z'};
    case 'magnetometro'
        vars = {'eje_x','eje_y','eje_z'};
    case 'temperatura'
        vars = {'temp_int','temp_ext'};
    case 'bateria'
        vars = {'voltaje','id_est_bateria'};
    case 'posicionamiento'
        vars={'latitud','longitud','velocidad','curso','satelite','hdop'};
    case 'audio'
        vars = {'audio'};
end
end