function varargout = administrador_usuarios(varargin)
% ADMINISTRADOR_USUARIOS MATLAB code for administrador_usuarios.fig
%      ADMINISTRADOR_USUARIOS, by itself, creates a new ADMINISTRADOR_USUARIOS or raises the existing
%      singleton*.
%
%      H = ADMINISTRADOR_USUARIOS returns the handle to a new ADMINISTRADOR_USUARIOS or the handle to
%      the existing singleton*.
%
%      ADMINISTRADOR_USUARIOS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADMINISTRADOR_USUARIOS.M with the given input arguments.
%
%      ADMINISTRADOR_USUARIOS('Property','Value',...) creates a new ADMINISTRADOR_USUARIOS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before administrador_usuarios_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to administrador_usuarios_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help administrador_usuarios

% Last Modified by GUIDE v2.5 26-Apr-2016 17:01:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @administrador_usuarios_OpeningFcn, ...
                   'gui_OutputFcn',  @administrador_usuarios_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before administrador_usuarios is made visible.
function administrador_usuarios_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to administrador_usuarios (see VARARGIN)

% Choose default command line output for administrador_usuarios
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
global cow_connector;
global h1;
h1=handles;
set_cow_connector('jose','59575351');
tabla = cow_connector.query(['SELECT `experto`.`id_exp`,'...
    '`experto`.`nombre`,'...
    '`experto`.`apellido`,'...
    '`experto`.`contrasena`,'...
    '`experto`.`numero_telefono`,'...
    '`experto`.`correo`,'...
    '`experto`.`profesion`,'...
    '`experto`.`d_labor`,'...
    '`categoria`.`categoria`'...
'FROM `vacas_db`.`experto` inner join vacas_db.categoria on '...
'experto.id_cat = categoria.id_cat;']);
columns = {'id_exp','nombre','apellido','contrasena','numero','correo','profesion','descripcion labor','categoria'};
set(handles.uitable1,'data',tabla,'columneditable',logical(1),'fontsize',10,'columnwidth','auto','ColumnName',columns);
set(handles.uitable1,'columnwidth',{26 75 75 75 100 200 170 300 75})


% set(handles.uitable1,'extent',extent);

% UIWAIT makes administrador_usuarios wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = administrador_usuarios_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in eliminar.
function eliminar_Callback(hObject, eventdata, handles)
% hObject    handle to eliminar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in agregar.
function agregar_Callback(hObject, eventdata, handles)
% hObject    handle to agregar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
