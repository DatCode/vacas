function modifyCowEvent(etiqueta)
global cow_connector;

clases = getClases();
% id_exp = cow_connector.user_data.id_exp{1};

for i = 1:length(etiqueta.inicio_senial)
    id_clase = clases.id{strcmpi(clases.alias,etiqueta.clase{i})};
    subclases = getSubclasesByClase(id_clase);
    scl = subclases.id{strcmpi(subclases.subclase,etiqueta.subclase{i})};
    t1=serial_to_str(etiqueta.inicio_senial(i));
    t2=serial_to_str(etiqueta.fin_senial(i));
    vt1 = serial_to_str(etiqueta.inicio_video(i));
    vt2 = serial_to_str(etiqueta.fin_video(i));
    comentario = etiqueta.comentarios{i};
    if isnumeric(etiqueta.id_predial{i})
        id_predial = num2str(etiqueta.id_predial{i});
    else
        id_predial = etiqueta.id_predial{i};
    end
    sag = predial_to_sag('predial',id_predial);    
    
    update = ['update vacas_db.etiqueta set inicio_senial = ' t1 ...
        ', fin_senial = ' t2 ', inicio_video = ' vt1 ...
        ', fin_video = ' vt2 ', comentario = ''' comentario ...
        ''', id_subclase = ' num2str(scl) ', id_vaca = ' num2str(sag)...
        ' where id_etiqueta = ' num2str(etiqueta.id_etiqueta{i})];
    cow_connector.query(update);
end
end