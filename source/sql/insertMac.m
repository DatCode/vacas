function insertMac(mac,verhw)
global cow_connector
%Procesa input
colnames = {'mac_nodo','verhw'};
if nargin<2    
    values = {mac,'test'};
else
    values = {mac,verhw};    
end

%Inserta nodo
table = 'nodo';
cow_connector.insert(table,colnames,values);
end