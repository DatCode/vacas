function sag = getSagFromRequest(request)
global cow_connector;
sag = [];
switch(request.fuente)
    case 'SAG'
        sag = request.fuente_val;
    case 'mac'
        sql = ['select id_vaca from vacas_db.montaje where id_nodo = ' request.fuente_val];
        sag = cow_connector.query(sql);
    case 'id montaje'
        sql = ['select id_vaca from vacas_db.montaje where id_montaje = ' request.fuente_val];
        sag = cow_connector.query(sql);
    case 'predial'
        sag = predial_to_sag(request.fuente_val);
end
end