function s=set_cow_connector(user,pass)
% addpath('../');
% addpath('../../../volcanes/Source/sql/')
% addpath('../../../volcanes/Source/sql/sql functions');
% addpath('../../../volcanes/Source/sql/time functions');
global cow_connector;
cow_connector =vconnector(getTxtProperty('vacas_conf.txt','dbsource'),user,pass,'vacas_db');
s=cow_connector.check_connection();
end