function tabla = get_sensor_table(sensor)
sensor = lower(sensor);
tabla = '';
switch(sensor)
    case 'acelerometro'
        tabla = 'acc';
    case 'giroscopio'
        tabla = 'giro';
    case 'magnetometro'
       tabla = 'mag';
    case 'temperatura'
        tabla = 'temperatura';
    case 'bateria'
        tabla = 'bateria';
    case 'posicionamiento'
        tabla = 'posicionamiento';
    case 'audio'
        tabla = 'audio';
end
end