tic;global cow_connector;
cow_connector = vconnector('local','jose','59575351','vacas_db');
isntfinished = true;
i = 0;
paso = 10000;
addpath('../time functions/');
pivotei = datenum(1970,01,01,00,00,00);
factor = 60*60*24;
% fid = fopen('log_timestamp_transform.txt','w');
while(i<paso*100)
    sql = ['select id_montaje,it,eje_x,eje_y,eje_z from vacas_db.acc limit '...
        num2str(i) ',' num2str(paso)];
    datos = cow_connector.query(sql);
%     if isempty(datos) || ischar(datos)
%        isntfinished = false;
%        break;
%     end
    it=timeInterpolation(DateStr2Num(datos(:,2),31));
    
    it = cellstr(datestr(it,'yyyy-mm-dd HH:MM:SS.FFF'));

    tabla = 'acc6';
    col_alt = {'it','id_montaje','eje_x','eje_y','eje_z'};
    formatspec = '''%s'',''%d'',''%1.4f'',''%1.4f'',''%1.4f''\n';
%     it = arrayfun(@(x) x,it,'uniformoutput',false);
    cow_connector.insert_as_csv(tabla,col_alt,[it,datos(:,1),datos(:,3:5)],formatspec);
    i=i+paso;
end
% fclose(fid);
toc;