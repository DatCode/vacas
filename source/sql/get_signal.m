function signal_struct=get_signal(request)
% global con;
con = conector(getTxtProperty('vacas_conf.txt','dbsource'));
% con = vconnector('local','jose','59575351','vacas_db');
con.dbname = 'vacas_db';
con.mysql_user = 'guest';
con.mysql_pass = 'helloeverybody';
try
    %% Obtiene el id montaje a partir del id predial
    timestamp_format = 'yyyy-mm-dd HH:MM:SS';
    ranges = [{datestr(request.range_x(1),timestamp_format)} ...
        {datestr(request.range_x(2),timestamp_format)}];
%     ranges = [{utcToStr(request.range_x(1))} {utcToStr(request.range_x(2))}];
    request.fuente = lower(request.fuente);
    switch (request.fuente)
        case 'predial'
            id_montaje = get_idmontaje(con,'mode','default','range',...
                ranges,'predial',request.fuente_val,'retire','all');
        case 'sag'
            id_montaje = get_idmontaje(con,'mode','default','range',...
                ranges,'sag',request.fuente_val,'retire','all');
        case 'mac'
            id_montaje = get_idmontaje(con,'mode','default','range',...
                ranges,'mac',request.fuente_val,'retire','all');
        case 'id montaje'
            id_montaje = request.fuente_val;
    end
    
    % id_montaje = get_idmontaje(con,'predial',request.predial,'range',ranges,'retire','all');
    if iscell(id_montaje)
        if ischar(id_montaje{1})
            if strcmpi(id_montaje(1),'No Data')
                %No hay data
                signal_struct = [];
                return;
            end
        end
    end
    %% Obtiene los datos
    where_cols = [request.colx;repmat({'id_montaje'},length(1),1)];
    operators = [{'between'};repmat({'='},length(id_montaje),1)];
    ranges={ranges;id_montaje(:)};
    where = con.build_where(where_cols,operators,ranges,[{'and'};repmat({'or'},length(id_montaje)-2,1)]);
    cols =[request.coly;request.colx];
    select = con.build_select(cols,request.tabla,where);
    datos = con.query(select);
    
    %% Procesa y guarda los datos
    signal_struct = con.get_struct(datos,cols);
%     try
%         signal_struct.it=DateStr2Num(signal_struct.it,31);
%     catch
%         signal_struct.it=datenum(signal_struct.it,timestamp_format);
%     end
%     signal_struct.it = linspace(datenum(signal_struct.it(1),timestamp_format),datenum(signal_struct.it(end),timestamp_format),length(signal_struct.it));
    signal_struct.it = timeInterpolation(datenum(signal_struct.it,timestamp_format));
    signal_struct.id_montaje = id_montaje;
catch
    signal_struct = [];
end
end

