classdef conector
    properties
        dbname;
        dbsource;
        user;
        pass;
        permissions;
        mysql_user;
        mysql_pass;
        session_started;       
    end
    
    methods
        %% METODOS SET Y GET
        function obj = set.dbname(obj,value)
            obj.dbname = value;
        end
        function obj = set.dbsource(obj,value)
            obj.dbsource = value;
        end
        function obj =set.user(obj,user)
            obj.user=user;
        end
        function obj = set.pass(obj,pass)
            obj.pass = pass;
        end
        function obj = set.mysql_user(obj,user)
           obj.mysql_user=user; 
        end
        function obj = set.mysql_pass(obj,pass)
          obj.mysql_pass=pass; 
        end
        function obj = set.permissions(obj,permissions)
           obj.permissions=permissions; 
        end
        function obj = set.session_started(obj,flag)
           obj.session_started=flag; 
        end
        function value = get.user(obj)
            value = obj.user;
        end
        function value = get.pass(obj)
            value= obj.pass;
        end
        function value = get.dbsource(obj)
            value=obj.dbsource;
        end
        function value = get.dbname(obj)
            value=obj.dbname;
        end      
        function value = get.mysql_user(obj)
           value = obj.mysql_user; 
        end
        function value = get.mysql_pass(obj)
           value = obj.mysql_pass; 
        end
        function value = get.permissions(obj)
           value = obj.permissions; 
        end
        function value = get.session_started(obj)
           value = obj.session_started; 
        end
        %% EJECUTA UNA CONSULTA
        function [result,isokay] = query(obj,sql)
            isokay=false;
            result =[];
            while(~isokay)
                isokay=true;
                
                [con,k] = try_to_connect(obj.dbsource,obj.mysql_user,obj.mysql_pass,5,3);
                % conn = database('local','jose','59575351','com.mysql.jdbc.Driver','jdbc:mysql://127.0.0.1:3306');
                if isconnection(con)
                    data =exec(con,sql);
                    if nargout>0
                        data = fetch(data);
                        result = data.data;
                    end
                    obj.createclosetimer(con);
                end
                if strcmpi(lastwarn,'Number of columns in each record are not equal.  Potential data error.')
                    isokay=false;
                end
                warning('');                
            end
            
            if nargout>0
                isokay = comprobarFetch(result);
            end
        end
        %% Crea timer para cerrar la conexion
        function createclosetimer(obj,con)
            str='closecon';
            closetimer = timer('name',str,'StartDelay',2,'userdata',con,'ExecutionMode','singleShot',...
                'busyMode','queue','objectVisibility','on','TimerFcn',{@obj.taskclosefcn});
            start(closetimer);
        end
        
        function taskclosefcn(obj,varargin)
            closetimer = varargin{1};
            con = get(closetimer,'userdata');
            if iscell(con)
                jconector = con{1}.Handle;jconector.close;
            else
                jconector = con.Handle;jconector.close;
            end
            try
                stop(closetimer);
                delete(closetimer);
            catch
            end
            
        end        
        %% CHEQUEA LA CONEXION CON EL SERVIDOR
        function flag = check_connection(obj)
            [con,k] = try_to_connect(obj.dbsource,obj.mysql_user,obj.mysql_pass,5,3);
            if isconnection(con)
                flag=true;
                close(con);
            else
                flag= false;
            end
        end
        
        %% CONSTRUYE Y EJECUTA UN DELETE
        function delete(obj,table,column,operator,value)
            sql =['delete from ' obj.dbname '.' table ' where ' column operator value ];
            obj.query(sql);
        end
        
        %% CONSTRUYE Y EJECUTA UN UPDATE
        function update(obj,table,column,datos,id_col,id)
            sql = ['update ' obj.dbname '.' table ' set '];
            mquote = '`';
            quote = '''';
            for i=1:length(column)
                sql = [sql ' ' mquote column{i} mquote '=' quote datos{i} quote ','];
            end
            sql = [sql(1:end-1) ' where ' mquote id_col mquote '=' quote id quote];
            obj.query(sql);
        end
        
        function user = get_user(obj)
            user = obj.user;
        end
        %% CONSTRUYE Y EJECUTA UN INSERT
        function insert(obj,tabla,columnas,datos,ondk)
            mquote = '`';
            sql = ['insert into ' obj.dbname '.' tabla '('];
            for i=1:length(columnas)
                sql =[sql mquote columnas{i} mquote ','];
            end
            sql = [sql(1:end-1) ') values('];
            quote ='''';
            if isvector(datos) && length(datos)==length(columnas)
                for i=1:length(datos)
                    sql =[sql quote datos{i} quote ','];
                end
                sql = [sql(1:end-1) ')'];
                
                if nargin>4
                   sql = [sql ' on duplicate key update ' strjoin(ondk,',')];
                end
                
                obj.query(sql);
            else
                [nrows,ncols] = size(datos);
                batch=round(500000/ncols);
                quote = '''';
                delimiter = [quote ',' quote];
                for i=1:batch:nrows
                    sql2 = sql(1:end-1);
                    
                    if i+batch>nrows
                        limit = nrows;
                    else
                        limit = batch+i-1;
                    end
                    idx = i:limit;
                    array = arrayfun(@(x) strjoin(datos(x,:),delimiter),idx,'uniformoutput',false);
                    sql2 = [sql2 '(''' strjoin(array,'''),(''') ''')'];
                    obj.query(sql2);
                end
            end
        end
        %% Inserta datos mediante un csv, el cual se crea en el instante
        function insert_as_csv(obj,tablename,columnas,datos,formatspec,columnascte,datoscte)
            
            %Escribe csv
            %             table = cell2table(datos);
            %             table.Properties.VariableNames=columnas;
            folder = tempdir;
            csvname = 'thatcsv.csv';
                        fullpathcsv = strrep(fullfile(folder,csvname),'\','/');
%             fullpathcsv = strrep(fullfile(pwd,csvname),'\','/');
            %             writetable(table,fullpathcsv);
            % xlswrite(csvname,datos);
            obj.csv_write(fullpathcsv,datos,formatspec)
            
            %Sube csv
            if nargin<7
                sql = ['load data local infile "' fullpathcsv '" into table ' obj.dbname '.' tablename...
                    ' fields terminated by "," enclosed by ''\'''' lines terminated by "\n" '...
                    '(' strjoin(columnas,',') ')'];
            else
                sql = ['load data local infile "' fullpathcsv '" into table ' obj.dbname '.' tablename...
                    ' fields terminated by "," enclosed by ''\'''' lines terminated by "\n" '...
                    '(' strjoin([columnas;columnascte],',') ') set '];
            end
            obj.query(sql);
            try
                fclose('all');
            delete(fullpathcsv);
            catch
            end
        end
        function csv_write(obj,filename,data,formatspec)
            fid = fopen(filename,'w');
            [nrows,ncols]=size(data);
            idrow = 1:nrows;
            
            instruction = 'arrayfun(@(x) fprintf(fid,formatspec,';
            for i = 1:ncols
               instruction = [instruction 'data{x,' num2str(i) '},'];
            end
            instruction = [instruction(1:end-1) '),idrow,''uniformoutput'',false);'];
            eval(instruction);
            fclose(fid);
        end
        %% ******************* CONSTRUCTOR DE LA CLASE ******************* 
        function obj = conector(varargin)
            obj.dbsource = varargin{1};
            if nargin == 1                
                obj.user = '';
                obj.pass = '';
                obj.dbname = 'volcanesm1';                
            else
                obj.user=varargin{2};
                obj.pass=varargin{3};
                if nargin>3
                    obj.dbname=varargin{4};
                else
                    obj.dbname='volcanesm1';
                end
            end          
            obj.mysql_user = obj.user;
            obj.mysql_pass = obj.pass;
            obj.session_started = false;
        end
        %% REGISTRA INICIO DE SESION
        function obj = open_session(obj)
            db = obj.dbname;
            procedure = 'authenticate';
            obj.mysql_user = 'authenticator';
            obj.mysql_pass = 'opadmin';
            data = stored_procedure(obj,db,procedure,obj.user,obj.pass);
            mysql_users_list = {'Analista','Sismologo','Desarrollador','Tesista',...
                'Guest','Clasificador'};
            mysql_pass_list = {'rqnr511ye','bsgk483yh','ugoa321ek0olrr','jogh527ys3ocwv',...
                '123456','pabh453ur8nicp'};
            if comprobarFetch(data) && ~isempty(data)
               switch(data{1})
                   case 'Analista'
                       obj.permissions.add_event = 'yes';
                       obj.permissions.modify_event = 'own';
                       obj.permissions.find_event = 'yes';
                       obj.permissions.delete_event = 'own';
                       obj.permissions.get_wave = 'yes';
                       flag = true;
                       idx = strcmpi(mysql_users_list,data{1});
                       obj.mysql_pass=mysql_pass_list{idx};
                       obj.mysql_user=lower(data{1});
                   case 'Sismologo'
                       obj.permissions.add_event = 'yes';
                       obj.permissions.modify_event = 'yes';
                       obj.permissions.find_event = 'yes';
                       obj.permissions.delete_event = 'own';
                       obj.permissions.get_wave = 'yes';
                       flag = true;
                       idx = strcmpi(mysql_users_list,data{1});
                       obj.mysql_pass=mysql_pass_list{idx};
                       obj.mysql_user=lower(data{1});
                   case 'Desarrollador'
                       obj.permissions.add_event = 'yes';
                       obj.permissions.modify_event = 'yes';
                       obj.permissions.find_event = 'yes';
                       obj.permissions.delete_event = 'yes';
                       obj.permissions.get_wave = 'yes';
                       flag = true;
                       idx = strcmpi(mysql_users_list,data{1});
                       obj.mysql_pass=mysql_pass_list{idx};
                       obj.mysql_user=lower(data{1});
                   case 'Tesista o ayudante laborante'
                       obj.permissions.add_event = 'yes';
                       obj.permissions.modify_event = 'own';
                       obj.permissions.find_event = 'yes';
                       obj.permissions.delete_event = 'own';
                       obj.permissions.get_wave = 'yes';
                       flag = true;
                       idx = strcmpi(mysql_users_list,'tesista');
                       obj.mysql_pass=mysql_pass_list{idx};
                       obj.mysql_user='tesista';
                   case 'Otro'
                       obj.permissions.add_event = 'no';
                       obj.permissions.modify_event = 'no';
                       obj.permissions.find_event = 'yes';
                       obj.permissions.delete_event = 'no';
                       obj.permissions.get_wave = 'yes';
                       flag = true;
                       idx = strcmpi(mysql_users_list,'guest');
                       obj.mysql_pass=mysql_pass_list{idx};
                       obj.mysql_user='guest';
                   case 'Clasificador'
                       obj.permissions.add_event = 'yes';
                       obj.permissions.modify_event = 'own';
                       obj.permissions.find_event = 'yes';
                       obj.permissions.delete_event = 'own';
                       obj.permissions.get_wave = 'yes';
                       flag = true;
                       idx = strcmpi(mysql_users_list,data{1});
                       obj.mysql_pass=mysql_pass_list{idx};
                       obj.mysql_user=lower(data{1});
                   case 'connection_failed'
                       flag = false;
                       disp('conector.m: Usuario y/o contrasenia invalida(s)');
                       obj.permissions = [];
                   otherwise
                       disp('conector.m: Open session failed');
                       flag = false;
                       obj.permissions = [];
               end
            else
                flag = false;
                obj.permissions=[];
            end
            obj.session_started = flag;
        end
        %% CIERRA LA SESION
        function close_session(obj)
            db = obj.dbname;
            procedure = 'close';
            stored_procedure(obj,db,procedure,obj.user,obj.pass);
        end
        %% LLAMA A UN STORED PROCEDURE
        function result = stored_procedure(obj,db,procedure,varargin)
            quote = '''';
            sql = ['{call ' db '.' procedure '('];
            inputs = cellfun(@(x) [quote x quote],varargin,'UniformOutput',false);
            if ~isrow(inputs)
               inputs = inputs'; 
            end
            sql = [sql strjoin(inputs,',') ')}'];
            result = obj.query(sql);
        end
        %% CONSTRUYE UN WHERE
        function where=build_where(obj,columns,operators,ranges,logicals)
            where=' where';
            quote = '''';
            mquote = '`';
            for i=1:length(columns)
                
                if strcmpi(operators(i),'between')
                    where = [where ' (' mquote columns{i} mquote];
                    if isnumeric(ranges{i,1})
                        where = [where ' between ' quote num2str(ranges{i,1}(1)) quote ' and ' quote num2str(ranges{i,1}(2)) quote ')'];
                    else
                        where = [where ' between ' quote ranges{i,1}{1} quote ' and ' quote ranges{i,1}{2} quote ')'];
                    end
                else
                    if ischar(ranges{i,1})
                        largo = 1;
                    else
                        largo = length(ranges{i,1});
                    end
                    if largo==1
                        if isnumeric(ranges{i,1})
                            where = [where  ' ' mquote columns{i} mquote operators{i,:} quote num2str(ranges{i,1}) quote];
                        else
                            where = [where ' ' mquote columns{i} mquote operators{i,:} quote char(ranges{i,1}) quote];
                        end
                    else
                        where = [where ' ('];
                        for j = 1:largo
                            if isnumeric(ranges{i,1}(j))
                                where = [where  ' ' mquote columns{i} mquote operators{i,:} quote num2str(ranges{i,1}(j)) quote];
                            else
                                where = [where ' ' mquote columns{i} mquote operators{i,:} quote char(ranges{i,1}(j)) quote];
                            end
                            
                            if j<largo
                               where = [where ' or ']; 
                            end
                        end
                        where = [where ')'];
                    end
                end
                if i< length(columns)
                try
                    where = [where ' ' logicals{i} ' '];
                catch
                end
                end
            end
        end
        
        %% CONSTRUYE UN SELECT
        function select=build_select(obj,columnas,tabla,where)
            mquote = '`';
            quote = '''';
            select = 'select ';
            for i = 1: length(columnas);
                select = [select mquote columnas{i,:} mquote ','];
            end
            select = [select(1:end-1) ' from ' mquote obj.dbname mquote '.' mquote tabla mquote ' ' where];
        end
        
        %% CONSTRUYE UNA ESTRUCTURA DE DATOS
        function estructura = get_struct(obj,datos,columnas)
            s = 0;
            e = 0;
            for i = 1:length(columnas)
                if isnumeric(datos{1,i})
                    if s==0
                        s=i;
                    end
                else
                    if s>0
                        e=i-1;
                        mat = cell2mat(datos(:,s:e));
                        for j = s:e
                            estructura.(columnas{j})=mat(:,j);
                        end
                    end
                    s=0;
                    e=0;
                    estructura.(columnas{i})=datos(:,i);
                end
            end
        end
    end
end