function id_montaje = get_idmontaje(con,varargin)
%Maneja entrada
p = inputParser;

%Valores esperados
expectedModes = {'default','fake'};
expectedUbications = {'corral','campo abierto','campo','galpon'};
colors_struct = getColors();
expectedColors = colors_struct.color;

%Valores por default
defaultUbication = 'corral';
defaultMac = '';
defaultColor = 'rojo';
defaultMode = 'default';

%A�ade parametros de entrada
addParameter(p,'color',defaultColor,...
    @(x) any(validatestring(x,expectedColors)));
addParameter(p,'mode',defaultMode,...
    @(x) any(validatestring(x,expectedModes)));
addParameter(p,'ubication',defaultUbication,...
    @(x) any(validatestring(x,expectedUbications)));
addParameter(p,'mac',defaultMac,...
    @ischar);
addParameter(p,'range','',...
    @(x) iscell(x) && length(x)==2);
addParameter(p,'retire','all',...
    @ischar);
addParameter(p,'predial','',@check_predial);
addParameter(p,'sag','',@check_sag);
addParameter(p,'f_inst','1970-01-01 00:00:00',...
    @ischar);
addParameter(p,'f_ret','1970-01-01 00:00:00',...
    @ischar);

%Obtiene input procesado
parse(p,varargin{:});
resultados = p.Results;
mode = resultados.mode;
ubication = resultados.ubication;
mac = resultados.mac;
range = resultados.range;
retire = resultados.retire;
idpredial = resultados.predial;
f_inst = resultados.f_inst;
f_ret = resultados.f_ret;
sag = resultados.sag;
color = resultados.color;

%Busca sag perteneciente al predial, en caso de no existir le crea

%Busca montaje perteneciante al sag que este sin retirarse
col_montaje ={'id_montaje'};
where = ' where ';
if ~strcmpi(idpredial,'')
    id_sag = predial_to_sag('predial',str2double(idpredial),'mode',mode,'connector',con);
    where = [where 'id_vaca=' num2str(id_sag)];
elseif ~strcmpi(sag,'')
    where = [where ' id_vaca = ' num2str(sag)];
elseif ~strcmpi(mac,'')
     where = [where ' id_nodo = ''' mac ''''];
end

if sum(strcmpi(range,''))==0
   where = [where ' and f_inst<="' range{2} '"']; 
end

if ~strcmpi(f_inst,'1970-01-01 00:00:00')
   where = [where ' and f_inst="' f_inst '"']; 
end

switch(retire)
    case 'all'
        if strcmpi(f_ret,'1970-01-01 00:00:00')
            if sum(strcmpi(range,''))==0
                where = [where ' and (f_ret<="1970-01-01 00:00:01" or f_ret>="' range{1} '")'];
            else
                where = [where ' and f_ret<="1970-01-01 00:00:01"'];
            end
        else
            where = [where ' and (f_ret="' f_ret '"'];
            if sum(strcmpi(range,''))==0
                where = [where ' or f_ret>="' range{1} '"'];
            end
            where = [where ' or f_ret<="1970-01-01 00:00:01")'];
        end
        
    case 'inprogress'
        where = [where ' and f_ret<="1970-01-01 00:00:01"'];
    case 'done'
        where = [where ' and f_ret>"1970-01-01 00:00:01"'];
end

tabla = 'montaje';
select = con.build_select(col_montaje,tabla,where);
id_montaje=con.query(select);

%En caso de no existir, crea el montaje
if ~montaje_exist(id_montaje) && strcmpi(mode,'fake')
    insert_random_montaje(con,id_sag,ubication,mac,f_inst,...
        colors_struct.id_color(strcmpi(colors_struct.color,color)));
    select = con.build_select(col_montaje,tabla,where);
    id_montaje=con.query(select);
end

id_montaje = arrayfun(@(i) num2str(id_montaje{i}),1:length(id_montaje),'UniformOutput',false);
if isrow(id_montaje)
    id_montaje = id_montaje';
end
end

%Crea un montaje imaginario
function insert_random_montaje(con,id_sag,ubication,id_nodo,f_inst,id_color)
%Define parametros
if isnumeric(id_sag)
   id_sag = num2str(id_sag); 
end
colnames = {'id_nodo','id_vaca','id_software','id_color','id_ubicacion','f_inst','f_ret','f_iubi','f_fubi'};
id_software = '1';
% id_color = '1';
if isnumeric(id_color)
   id_color = num2str(id_color); 
end
table = 'montaje';

%Obtiene mac del nodo
% sql = 'select mac_nodo from vacas_db.nodo limit 1';
% id_nodo = con.query(sql);
% if iscell(id_nodo)
%    id_nodo = char(id_nodo); 
% end

%Obtiene id ubicacion
sql = ['select id_ubicacion from vacas_db.ubicacion where ubicacion like "' ubication '";'];
id_ubicacion = con.query(sql);
if iscell(id_ubicacion)
    if isnumeric(id_ubicacion{1})
        id_ubicacion = num2str(id_ubicacion{1});
    end
end

%Inserta el nuevo montaje
con.insert(table,colnames,...
    [{id_nodo};{id_sag};{id_software};{id_color};{id_ubicacion};{f_inst};repmat({'1970-01-01 00:00:00'},3,1)]);
end

%Verifica si efectivamente existe o no el montaje
function flag=montaje_exist(id_montaje)
flag = false;
if ~isempty(id_montaje)
    if iscell(id_montaje)
        if isnumeric(id_montaje{1})
            if id_montaje{1}>0
                flag=true;
            end
        end
    end
end
end

%Comprueba que el input idpredial sea valido
function flag = check_predial(predial)
flag = false;
if isnumeric(predial)
    if predial<=9999 && predial>0
        flag=true;
    end
else
    if ischar(predial)
        if length(predial)<=4 && str2num(predial)>0
            flag = true;
        end
    else
        if iscell(predial)
            if ischar(predial{1})
                predial = char(predial);
                if length(predial)==4 && str2num(predial)>0
                    flag = true;
                end
            end
        end
    end
end
end

%Comprueba que el input idpredial sea valido
function flag = check_sag(sag)
flag = false;
if isnumeric(sag)
    if sag<=999999999 && sag>0
        flag=true;
    end
else
    if ischar(sag)
        if length(sag)<=9 && str2num(sag)>0
            flag = true;
        end
    else
        if iscell(sag)
            if ischar(sag{1})
                sag = char(sag);
                if length(sag)==9 && str2num(sag)>0
                    flag = true;
                end
            end
        end
    end
end
end