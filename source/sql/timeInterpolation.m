function it=timeInterpolation(it)
offset = datenum(0,0,0,0,0,1);
[~,ic,~] = unique(it);
itarray = arrayfun(@(x) single_interpolation(it(it==it(x)),offset),ic,'uniformoutput',false);
it = [itarray{:}];
end

function it=single_interpolation(it,offset)
it = linspace(it(1),it(1)+offset,length(it)+1);
it = it(1:end-1);
end