function signal_struct=sel_signal(t1,t2,predial,sensor,timeformat)
%timeformat: -'utc'
%            -'datenum'
if nargin == 0
    sensor ='acelerometro';
    t1=utcDate(2015,11,23,18,16,00);
    t2=utcDate(2015,11,23,19,20,00);
    predial = '1113';
elseif nargin<5
    timeformat = 'datenum';    
end

options = {'acelerometro';'giroscopio';'magnetometro';'temperatura';'posicionamiento'};
switch(lower(sensor))
    case options{1}
        cols = {'eje_x';'eje_y';'eje_z'};
        tabla = 'acc';
%         tabla = 'acc_traspaso';
    case options{2}
        cols ={'eje_x';'eje_y';'eje_z'};
        tabla = 'giro';
    case options{3}
        cols= {'eje_x';'eje_y';'eje_z'};
        tabla = 'mag';
    case options{4}
        cols = {'temp_ext';'temp_int'};
        tabla = 'temperatura';
    case options{5}
        cols= {'latitud';'longitud';'velocidad';'curso';'satelite';'hdop'};
        tabla = 'posicionamiento';
end
it = {'it'};
request.coly =cols;
request.colx = it;
request.tabla = tabla;
request.db = 'vacas_db';
request.range_x=[t1 t2];
request.predial = predial;
request.fuente = 'predial';
request.fuente_val = predial;
request.time_format = timeformat;
signal_struct = getUTCSignal(request);
% signal_struct = get_signal(request);
end