function mac = insertRandomMac(color)
%Procesa entrada
if nargin<1
    color = 'red';
end
%Inserta mac
mac = char(rand(1,5)*25+97);
insertMac(mac,color);
end