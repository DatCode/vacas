function clases=getClaseByIndex(index)
sql = ['select id_clase,clase,alias from vacas_db.clase_etiqueta where id_clase = '...
    num2str(index)];
global cow_connector;
data = cow_connector.query(sql);
clases=[];
if ~isempty(data)
    clases.id = data(:,1);
    clases.clase = data(:,2);
    clases.alias = data(:,3);
end