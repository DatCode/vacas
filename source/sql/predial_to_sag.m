function sag=predial_to_sag(varargin)
%Maneja entrada
warning off;
con = conector(getTxtProperty('vacas_conf.txt','dbsource'));
con.dbname = 'vacas_db';
warning on;
p = inputParser;
defaultMode = 'default';
expectedModes = {'default','fake'};

addParameter(p,'connector',con,@isobject);
addParameter(p,'predial',@check_predial);
addParameter(p,'mode',defaultMode,...
    @(x) any(validatestring(x,expectedModes)));
parse(p,varargin{:});
resultados = p.Results;
predial = num2str(resultados.predial);
mode = resultados.mode;
connector = resultados.connector;

%Trata de insertar el valor, solo en caso de no ser duplicado
if strcmpi(mode,'fake')
    sql = ['select id_predial from vacas_db.vacas where id_predial = ' predial ' limit 1'];
    nprediales = connector.query(sql);
    if isempty(nprediales)
        insert_random_cow(connector,predial);
    else
        if isnumeric(nprediales)
            if nprediales==0
                insert_random_cow(connector,predial);
            end
        else
            if iscell(nprediales)
                if isnumeric(nprediales{1})
                    if nprediales{1}==0
                        insert_random_cow(connector,predial);
                    end
                else
                    if ischar(nprediales{1})
                        insert_random_cow(connector,predial);
                    end
                end
            end
        end
    end
end

%Obtiene el sag asociado al predial ingresado
db=connector.dbname;
tabla = 'vacas';
sql = ['select id_sag from ' db '.' tabla ' where id_predial=' predial];
datos = connector.query(sql);
sag = datos{:,1};
end

%Crea una vaca aleatoria
function insert_random_cow(con,predial)
%Define parametros
okay = false;
if isnumeric(predial)
    predial = num2str(predial);
end
nombre = 'test';
id_raza = '1';
id_aceptacion = '3';
colnames = {'id_sag','id_predial','nacimiento','nombre','id_raza','id_acept'};
table = 'vacas';

%Busca un sag no utilizado
while ~okay
    randsag = randSag();
    sql = ['select count(*) from vacas_db.vacas where id_sag = ' randsag];
    niguales = con.query(sql);
    if ~isempty(niguales)
        if isnumeric(niguales)
            if niguales ==0
                okay=true;
            end
        else
            if iscell(niguales)
               if isnumeric(niguales{1})
                  if niguales{1}==0
                      okay = true;
                  end
               end
            end
        end
    else
        okay = true;
    end
end
%Realiza la insercion
datos = [{randsag};{predial};{'1970-01-01 00:00:01'};{nombre};{id_raza};{id_aceptacion}];
con.insert(table,colnames,datos);
end

function str = randSag()
str = num2str(floor(rand(1)*890000000+10000000));
end

%Comprueba que el input idpredial sea valido
function flag = check_predial(predial)
flag = false;
if isnumeric(predial)
    if predial<=9999 && predial>0
        flag=true;
    end
else
    if ischar(predial)
        if length(predial)==4 && str2num(predial)>0
            flag = true;
        end
    else
        if iscell(predial)
            if ischar(predial{1})
                predial = char(predial);
                if length(predial)==4 && str2num(predial)>0
                    flag = true;
                end
            end
        end
    end
end
end