function [etiquetas,s]=get_etiquetas(t1,t2,varargin)
global cow_connector;
etiquetas = [];
if ~isempty(cow_connector)
    if cow_connector.check_connection
        p = inputParser;
        
        addParameter(p,'predial','0000',@(x) ischar(x) && length(x)==4);
        parse(p,varargin{:});
        predial=p.Results.predial;
%         timestampfmt = 'yyyy-mm-dd HH:MM:SS';
%         strt1 = datestr(t1,timestampfmt);
%         strt2 = datestr(t2,timestampfmt);
        strt1= serial_to_str(t1);
        strt2=serial_to_str(t2);
        
        sql = ['select e.id_etiqueta, e.inicio_senial, e.fin_senial, e.inicio_video, e.fin_video,'...
            'comentario,exp.correo,exp.nombre,c.alias,s.subclase,e.f_creacion, v.id_predial'...
            ', m.id_montaje,m.f_inst,m.f_ret from vacas_db.etiqueta as e '...           
            'inner join vacas_db.vacas as v on v.id_sag = e.id_vaca '...
            'inner join vacas_db.subclase_etiqueta as s on s.id_subclase = e.id_subclase '...
            'inner join vacas_db.clase_etiqueta as c on c.id_clase = s.id_clase '...
            'inner join vacas_db.experto as exp on exp.id_exp = e.id_exp_creador '...
            'inner join vacas_db.montaje as m on m.id_vaca = v.id_sag '...
            'where '];
        if ~strcmpi(predial,'0000')
            sql=[sql 'v.id_predial = ' predial ' and e.inicio_senial <= "' strt2 '" '...
            ' and e.fin_senial>="' strt1 '"'];
        else
            sql=[sql 'e.inicio_senial <= "' strt2 '" '...
            ' and e.fin_senial>="' strt1 '"'];
        end
        sql = [sql ' having (e.inicio_senial between vacas_db.timestampToDatenum(m.f_inst) and vacas_db.timestampToDatenum(m.f_ret))'...
            ' or (vacas_db.timestampToDatenum(m.f_inst) between e.inicio_senial and e.fin_senial)' ];
        campos = {'id_etiqueta','inicio_senial','fin_senial','inicio_video','fin_video','comentarios',...
            'correo_experto','nombre_experto','clase','subclase','fecha_creacion','id_predial',...
            'id_montaje','f_inst','f_ret'};
        
        try
            resultado = cow_connector.query(sql);
            etiquetas = cow_connector.get_struct(resultado,campos);
%             etiquetas.celo = resultado(:,end);
%             etiquetas.id_predial = resultado(:,end);
%             if isnumeric(etiquetas.id_montaje)
%                etiquetas.id_montaje = num2cell(etiquetas.id_montaje); 
%             end
%             if isnumeric(etiquetas.id_etiqueta)
%                etiquetas.id_etiqueta = num2cell(etiquetas.id_etiqueta); 
%             end
%             etiquetas.inicio_video = timestamp2datenum(etiquetas.inicio_video);
%             etiquetas.fin_video = timestamp2datenum(etiquetas.fin_video);
%             etiquetas.inicio_senial = timestamp2datenum(etiquetas.inicio_senial);
%             etiquetas.fin_senial = timestamp2datenum(etiquetas.fin_senial);
            s=1;
%             etiquetas.id_montaje=resultado(:,13);
        catch
            %Ha habido un error con la consulta mysql
            s=0;
            warning('No se han encontrado etiquetas');
        end
        
        %OBTIENE TIEMPO DE INICIO Y FIN DEL MONTAJE
%         sql = ['select f_inst,f_ret from vacas_db.montaje where id_montaje = '...
%             num2str(etiquetas.id_montaje{1})];
%         etiquetas.fechas_montaje = cow_connector.query(sql);
    else
        %No existe conexion con la base de datos
        s=-1;
        warning('Sin conexion a la base de datos mysql. Revisa tu conexion a internet');
    end
else
    %No ha sido creado el conector global para base de datos de vacas
    s=-2;
    warning('Primero debes crear el conector global para la base de datos. Usa la funcion set_cow_connector');
end
end

function out= timestamp2datenum(t)
timestampfmt = 'yyyy-mm-dd HH:MM:SS';
out=cellfun(@(x) datenum(x,timestampfmt),t,'uniformoutput',false);
end