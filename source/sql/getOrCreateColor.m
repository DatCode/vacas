function id_color = getOrCreateColor(color)
global cow_connector

%Busca un color que se llame igual que el input
sql = ['select id_color from vacas_db.color where color = ''' color ''''];
id_color = cow_connector.query(sql);

%Verifica que exista el color. Si existe entonces retorna el mando.
if ~isempty(id_color)
    if ~ischar(id_color{1})
        id_color = id_color{1};
        return;
    end
end

%Inserta el nuevo color
sql = ['insert into vacas_db.color(color) values(''' color ''');'];
cow_connector.query(sql);
id_color = addColor(cow_connector,color);
end