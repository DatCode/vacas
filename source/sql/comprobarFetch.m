function s=comprobarFetch(datos)

if iscell(datos)
    if ischar(datos{1})
        %Caso en que no existen datos.
        if strcmp(datos{1},'No Data')
%             warning('Error: Inexistencia de tablas en la base de datos');
            s=0;
            return;
        end
    end
else
    if isnumeric(datos)
        %Existe un error en la sintaxis del query.
        if datos == 0
%             warning('Error: Consulta mysql con sintaxis invalida, existe dicha tabla o no se han devuelto datos');
            s=0;
            return;
        end
    end
end

s=1;
end