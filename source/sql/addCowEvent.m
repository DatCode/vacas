function addCowEvent(etiqueta)
global cow_connector;

clases = getClases();
id_exp = cow_connector.user_data.id_exp{1};

for i = 1:length(etiqueta.inicio_senial)
    id_clase = clases.id{strcmpi(clases.alias,etiqueta.clase{i})};
    subclases = getSubclasesByClase(id_clase);
    
    scl = subclases.id{strcmpi(subclases.subclase,etiqueta.subclase{i})};

    if(etiqueta.inicio_senial(1)>etiqueta.fin_senial(i))
        t2=serial_to_str(etiqueta.inicio_senial(i));
        t1=serial_to_str(etiqueta.fin_senial(i));
    else
        t1=serial_to_str(etiqueta.inicio_senial(i));
        t2=serial_to_str(etiqueta.fin_senial(i));
    end
    if etiqueta.inicio_video(i)>etiqueta.fin_video(i)
        vt2 = serial_to_str(etiqueta.inicio_video(i));
        vt1 = serial_to_str(etiqueta.fin_video(i));
    else
        vt1 = serial_to_str(etiqueta.inicio_video(i));
        vt2 = serial_to_str(etiqueta.fin_video(i));
    end
    comentario = etiqueta.comentarios{i};
    if isnumeric(etiqueta.id_predial{i})
        id_predial = num2str(etiqueta.id_predial{i});
    else
        id_predial = etiqueta.id_predial{i};
    end
    sag = predial_to_sag('predial',id_predial);    
    
    insert = ['INSERT INTO `vacas_db`.`etiqueta`'...
        '(`id_vaca`,`inicio_senial`,`fin_senial`,`inicio_video`,`fin_video`,`comentario`,`id_exp_creador`,`id_subclase`)'...
        ' VALUES (' num2str(sag) ',' t1 ',' t2 ',' vt1 ',' vt2 ',''' comentario ''',' num2str(id_exp) ',' num2str(scl) ...
        ')'];
    cow_connector.query(insert);
end
end