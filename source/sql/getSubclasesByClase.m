function subclases = getSubclasesByClase(id_clase)
global cow_connector;
sql = ['select id_subclase,id_clase,subclase from vacas_db.subclase_etiqueta where id_clase = ' num2str(id_clase)];
data = cow_connector.query(sql);
subclases.id=data(:,1);
subclases.id_clase = data(:,2);
subclases.subclase = data(:,3);
end