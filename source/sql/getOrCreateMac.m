function mac = getOrCreateMac(mac)
%Busca un nodo con la mac pedida
quote = '''';
sql = ['select mac_nodo from vacas_db.nodo where mac_nodo = ' quote mac quote];
result = con.query(sql);

%Evalua si es necesario crear el nodo o si ya existe
if ~isempty(result)
    if ~iscell(result)
        if ~(strcmpi(result,'') || strcmpi(result,'no data'))
            return
        end
    else
        if ischar(result{1})
            if ~(strcmpi(result,'') || strcmpi(result,'no data'))
                return
            end
        end
    end
end

%Inserta mac
insertMac(mac);
end