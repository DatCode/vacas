function out=utc2datenum(t)
    pivotei = datenum(1970,01,01,00,00,00);
    factor = 60*60*24;
    out = t/factor+pivotei;
end