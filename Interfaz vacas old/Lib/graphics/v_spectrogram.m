function handle=v_spectrogram(p)
global h;

if nargin<1
    p = get_signal_plot('filtered_signal');
    if isempty(p)
        handle = [];
        return;
    end
end

%Obtiene userdata
userdata = get(h.selected_axes,'userdata');
userdata.name = 'espectrograma';
userdata.display = 'spectrogram';

p_userdata = get(p,'userdata');
% senal=get(p,'ydata');
%     t=get(p,'xdata');
data = get_limited_data(p);
senal = data.ydata;
t = data.xdata;

%Trata de obtener el espectrograma del cache
if ~strcmpi(userdata.scf,'') && strcmpi(userdata.zoom,'off')
    try
        if strcmpi(p_userdata.name,'event_zoom')
            if ~isempty(userdata.events)
                scf = num2str(userdata.events.id_evento{1});
            end
        else
            scf = [userdata.estacion userdata.componente datestr(t(1),'yyyymmdd_HHMMSS') datestr(t(end),'yyyymmdd_HHMMSS')];
        end
        %     scf = userdata.scf;
        try
            load(['cache/' scf]);
        catch
            pathstr = strsplit(pwd,'\');
            if strcmpi(pathstr(end),'Interfaz clasificacion')
                save(['../Interfaz de usuario/cache/' scf],'t','spect_F','DEP_Sdb','kconver');
            else
                idx = senal~=0;
                t=t(idx);
                senal = senal(idx);%*kconver;
                overlap = 295;
                nfft = 512;
                fs = get_fsamp(t(1));
                window = 300;
                [spect_S,spect_F,~,~]=spectrogram(senal,...
                    hamming(window),... %window 300
                    overlap,... %n overlap 295
                    nfft,... %nfft 1024
                    fs,... %fs (fs fija) 100
                    'yaxis');
                DEP_Sdb=10*log10(spect_S.*conj(spect_S));
            end
        end
    catch
        scf = '';
        userdata.scf = scf;
        set(h.selected_axes,'userdata',userdata);
        handle = r_spectrogram(p);
        return;
    end
else
    %Calcula el espectrograma
    %     if t(1) > datenum(2011,06,01)
    %         kconver=(1.9867e-3);
    %     else
    %         kconver = 1e-3;
    %     end
    %     kconver = userdata.kconver;
    
    idx = senal~=0;
    t=t(idx);
    senal = senal(idx);%*kconver;
    overlap = 295;
    nfft = 512;
    fs = get_fsamp(t(1));
    window = 300;
    [spect_S,spect_F,~,~]=spectrogram(senal,...
        hamming(window),... %window 300
        overlap,... %n overlap 295
        nfft,... %nfft 1024
        fs,... %fs (fs fija) 100
        'yaxis');
    DEP_Sdb=10*log10(spect_S.*conj(spect_S));
    if strcmpi(userdata.zoom,'off')
        if strcmpi(p_userdata.name,'event_zoom')
            if ~isempty(userdata.events)
                scf = num2str(userdata.events.id_evento{1});
            end
        else
            scf = [userdata.estacion userdata.componente datestr(t(1),'yyyymmdd_HHMMSS') datestr(t(end),'yyyymmdd_HHMMSS')];
        end
        try
            save(['cache/' scf],'t','spect_F','DEP_Sdb','kconver');
        catch
            pathstr = strsplit(pwd,'\');
            if strcmpi(pathstr(end),'Interfaz clasificacion')
                save(['../Interfaz de usuario/cache/' scf],'t','spect_F','DEP_Sdb','kconver');
            end
        end
        userdata.scf = scf;
    end
end

%Muestra el espectrograma
set(h.fig,'currentaxes',h.selected_axes);
try
    handle=imagesc(t,spect_F,DEP_Sdb,'parent',h.selected_axes);axis xy;
    rango_caxis = [0 100];
    caxis(h.selected_axes,rango_caxis);%Color axis rango 50-100 db
    ylim(h.selected_axes,[0 20]);% Limita 0 - 20Hz
    xlim(h.selected_axes,[t(1) t(end)]);
    set(h.selected_axes,'userdata',userdata);
    s_userdata.display = 'spectrogram';
    s_userdata.refer_plot = p_userdata.name;
    set(handle,'userdata',s_userdata);
catch
    %     delete(['cache/' scf '.mat']);
    userdata.scf = '';
    set(h.selected_axes,'userdata',userdata);
    r_spectrogram(p);
end
% refresh_date_xtick;

end