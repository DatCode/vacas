function ax=vrefresh_cursor()
global h;
global flag;

if isfield(h,'ax')
    ax = is_hover_an_axes(h.selected_axes);
    if ishandle(ax)
%         disp('sobre un axes seleccionado');
        if flag.choosing_axes || flag.choosing_sync_axes
            set(h.fig,'pointer','hand');
        else
            if flag.selecting_signal
                set(h.fig,'pointer','crosshair');
                vrefresh_crosshair_line();
            else
                set(h.fig,'pointer','arrow');
            end
        end
    else
        button = is_hover_a_button();
        if isobject(button) || ishandle(button)
            set(h.fig,'pointer','hand');
        else
            set(h.fig,'pointer','arrow');
        end
        
        ax = is_hover_an_axes();
        if ishandle(ax)
            if flag.choosing_axes || flag.choosing_sync_axes
                set(h.fig,'pointer','hand');
            else
                set(h.fig,'pointer','arrow');
            end
        end
    end
else
    ax = false;
    button = is_hover_a_button();
    if isobject(button) || ishandle(button)
        set(h.fig,'pointer','hand');
    else
        set(h.fig,'pointer','arrow');
    end
end
end