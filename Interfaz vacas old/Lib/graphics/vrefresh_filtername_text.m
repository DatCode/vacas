function vrefresh_filtername_text(ax)
global h;
if nargin<1
    if valid_field(h,'selected_axes',0)
        ax = h.selected_axes;
    else
        return;
    end
end
ud = get(ax,'userdata');
filtername = ud.filtername;
handle = get_text_handle(ax,'filtername');
limx = get(ax,'xlim');
limy = get(ax,'ylim');
if ~isempty(handle)
%     position = [limx(1)+(limx(2)-limx(1))*0.005 limy(1)+(limy(2)-limy(1))*0.02 0];
%     set(handle,'string',sprintf('%s',filtername),'position',position);
% else
delete(handle);
end
filtername = strrep(filtername, '_', ' ');
    t=text(limx(2)-(limx(2)-limx(1))*0.22,limy(2)-(limy(2)-limy(1))*0.08,sprintf('%s',filtername),'Color','red','FontSize',10);
    set(t,'userdata','filtername','parent',ax,'interpreter','tex','clipping','on');
t2=title(ax,[upper([ud.estacion ud.componente]) ' ' datestr(limx(1),'yyyy/mm/dd')],'visible','on');
drawnow;
% pause();
end