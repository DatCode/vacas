function vrefresh_crosshair_line()
global h;
global colores;
cursorpoint = abstorelpoint(get(h.fig,'currentpoint'),h.selected_axes);
axpos = get(h.selected_axes,'position');
limx = get(h.selected_axes,'xlim');
limy = get(h.selected_axes,'ylim');
% disp(['cp = ' mat2str(cursorpoint)]);
plotpoint = cursorpoint./axpos(3:4).*[limx(2)-limx(1) limy(2)-limy(1)]+[limx(1) limy(1)];
% disp(['plotpoint = ' datestr(plotpoint(1)) ',' num2str(plotpoint(2))]);
ud = get(h.selected_axes,'userdata');
linked = ud.linked_ax;

for i = 1:length(linked)
    act_ud = get(linked(i),'userdata');
    handleline = act_ud.crosshairline;
    if ishandle(handleline) || isobject(handleline)
        set(handleline,'ydata',limy,'xdata',[plotpoint(1) plotpoint(1)],'color',colores.crosshairline,'visible','on');
    else
        handleline = line([plotpoint(1) plotpoint(1)],limy,'color',colores.crosshairline,'parent',linked(i));
    end
    act_ud.crosshairline = handleline;
    uistack(handleline,'top');
    set(linked(i),'userdata',act_ud);
    
end
refresh_display_data()
end

function refresh_display_data
global h;
ud = get(h.selected_axes,'userdata');
if valid_field(h,'selected_signal',0)
    try
        xdata = get(h.selected_signal,'xdata');
        slimx=[datestr(xdata(1),'HH:MM:SS') ' - ' datestr(xdata(end),'HH:MM:SS')];
    catch
        slimx = '00:00:00 - 00:00:00';
        h.selected_signal = [];
    end
else
    slimx = '00:00:00 - 00:00:00';
end
crosshairxpoint=get(ud.crosshairline,'xdata');
if isempty(crosshairxpoint)
    crosshairxpoint = 'undefined';
else
    crosshairxpoint = datestr(crosshairxpoint(1),'HH:MM:SS');
end
set(h.dynamic_event_data,'value',3,'string',{...
    html([strong('Rango sel: ') slimx]);...
    html([strong('Pos cursor: ') crosshairxpoint]);...
    '------------------------------'})
end