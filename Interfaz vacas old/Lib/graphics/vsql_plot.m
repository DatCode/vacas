function ax=vsql_plot(input)
global selected_filter;
global h;
try
    display_state('Buscando senial en el servidor...');
    datos = get_signal(input);
    if ~isstruct(datos)
        error('No hay datos');
    end
    %     datos=SQLdata(ti,tf,'conversion','um/s');%,'componente',output.componente,'estacion',output.estacion);
catch
    datos = false;
    display_state('No se ha encontrado senial...');
end
ax = [];
in_display_event_axes = false;
if isstruct(datos)
    
    %Grafica la senial
    %     senial = center_signal(datos.SQLdata_data);
    senial = datos.(input.coly);
    tiempo = datos.(input.colx{1});
    
    if valid_field(input,'mode','')
        if strcmpi(input.mode,'replace') && valid_field(h,'display_event_axes',0)
            ax = h.display_event_axes;
            in_display_event_axes=true;
        end
    end
    
    if ~isempty(ax)
        options.ax = ax;
        %         disp('plotea')
        ax=plot_signal(tiempo,senial,options);
    else
        %         disp('plotea')
        ax=plot_signal(tiempo,senial);
        if in_display_event_axes
            h.display_event_axes = ax;
        end
    end
    
    %Avisa que estacion se esta observando
    userdata = get(ax,'userdata');
    userdata.display = ['filtered_' input.coly];
    userdata.event_display = 'true';
    userdata.scf = '';
    userdata.zoom = 'off';
    userdata.filtername = selected_filter;
    userdata.crosshairline = -1;
    userdata.sensor = input.sensor;
    
    set(ax,'userdata',userdata);
    
    %Grafica los eventos
    %     refresh_events_in_graph(ax);
    
    %Refresca los contadores
    %     refresh_counters();
    %     refresh_filtername_text(ax);
    %     display_events_in_ax(ax,'refresh')
end

% display_state('Listo...');
end

