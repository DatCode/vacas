function content_axes = vplot_signal(tiempo,signal,options)
global h;
global axes_height;
% global original_signal_color;
global colores;
display_state('Graficando senial...');

if nargin<3
    %Crea axes
    fig_pos = get(h.fig,'position');
    fwidth=fig_pos(3);
    
    screensize = get(0,'screensize');
    fwidth = fwidth *screensize(3);
    
    vaddAxes(h.axes_panel,[fwidth-397-70 axes_height]);
    ax=h.ax(end);
    ud = get(ax,'userdata');
    p=plot(ax,tiempo,signal);
    set(ax,'userdata',ud);
    hold(ax,'on');
    p_filt = plot(ax,tiempo,filter_signal(signal));
else
    if valid_field(options,'ax',0)
        ax = options.ax;
        p_filt = get_signal_plot(ax,'filtered_signal');
        p = get_signal_plot(ax,'original_signal');
        if ~isempty(p_filt) && ~isempty(p)
            set(p_filt,'xdata',tiempo,'ydata',filter_signal(signal));
            set(p,'xdata',tiempo,'ydata',signal);
        else
            p=plot(ax,tiempo,signal);
            
            hold on;
            p_filt = plot(ax,tiempo,filter_signal(signal));
        end
    else
        %Crea axes
        fig_pos = get(h.fig,'position');
        fwidth=fig_pos(3);
        screensize = get(0,'screensize');
    fwidth = fwidth *screensize(3);
        vaddAxes(h.axes_panel,[fwidth-397-70 axes_height]);
        ax=h.ax(end);
        ud = get(ax,'userdata');
        p=plot(ax,tiempo,signal);
        set(ax,'userdata',ud);
        
        hold on;
        p_filt = plot(ax,tiempo,filter_signal(signal));
    end
    
    %Guarda el axes para mostrar eventos
    if valid_field(options,'display_event_axes',true)
        if  options.display_event_axes
            h.display_event_axes = ax;
        end
    end
end
userdata.name = 'original_signal';
set(p,'userdata',userdata);
set(p,'visible','off');

set(p,'color',colores.original_signal);
uistack(p_filt,'top');
userdata.name = 'filtered_signal';
set(p_filt,'userdata',userdata);
set(p_filt,'color',colores.filtered_signal);
remember_limits(p_filt);

pause(0.01);
content_axes = get(p_filt,'parent');
refresh_date_xtick();
% set(ax,'units','normalized');

if strcmpi(get(h.fig,'units'),'normalized')
set_global_units('normalized');
end
display_state('Listo...');
end