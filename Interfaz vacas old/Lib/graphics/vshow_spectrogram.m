function vshow_spectrogram(varargin)
% tic;
global h;
if isfield(h,'selected_axes')
    if ishandle(h.selected_axes) || isobject(h.selected_axes)
        try
            delete(h.selected_signal);
        catch
        end
        
        if isnumeric(h.selected_axes)
            if h.selected_axes==0
                return;
            end
        end
        set(get_signal_plot(h.selected_axes,'selected_signal'),'visible','off');
        %         button = h.fast_button(5);
        %         refresh_fast_button(button);
        try
            set(h.selected_signal,'visible','off');
        catch
        end
        p=get_signal_plot(h.selected_axes,'current');    
        if isempty(p)
           p = get_signal_plot(h.selected_axes,'filtered_signal'); 
        end
        clear_axes();
        
        s = get_spectrogram();
        userdata = get(h.selected_axes,'userdata');
        %         display_anterior = userdata.display;
        userdata.display = 'spectrogram';
        if ~isempty(s)
            delete(s);
            p = get_signal_plot(h.selected_axes,'event_zoom');
            if isempty(p)
                p = get_signal_plot(h.selected_axes,'filtered_signal');
            end
            p_userdata = get(p,'userdata');
            limx = get(h.selected_axes,'xlim');
            show_in_graph({p_userdata.name});
            set(h.selected_axes,'xlim',limx);
            display_state('Listo...');
            userdata.display = 'filtered_signal';
            set(h.selected_axes,'userdata',userdata);
            refresh_date_xtick();
            refresh_fast_buttons_color();
            refresh_filtername_text;
            try
            set(h.selected_signal,'visible','on');
            catch
            end
            refresh_marks();
            return;
        end
        set(h.selected_axes,'userdata',userdata);
        v_spectrogram(p);
    end
    refresh_fast_buttons_color();
    refresh_marks();
end
display_state('Listo...');
% toc;
end