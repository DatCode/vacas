function refreshLabelLimits()
global video_handles;
st = getSignalAxesStems();
xdatas = zeros(1,length(st));
for i = 1: length(st)
    xdata =get(st(i),'xdata');
    xdatas(i) = xdata(1);
end
mintime = min(xdatas);
maxtime = max(xdatas);
fmt = 'HH:MM:SS';
set(video_handles.signal_first_limit_field,'string',datestr(mintime,fmt))
set(video_handles.signal_last_limit_field,'string',datestr(maxtime,fmt))


st = getVideoAxesStems();
xdatas = zeros(1,length(st));
for i = 1: length(st)
    xdata =get(st(i),'xdata');
    xdatas(i) = xdata(1);
end
mintime = min(xdatas);
maxtime = max(xdatas);
fmt = 'HH:MM:SS';
set(video_handles.video_first_limit_field,'string',datestr(mintime,fmt))
set(video_handles.video_last_limit_field,'string',datestr(maxtime,fmt))

end