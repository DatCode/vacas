function ct = getCurrentVideoTime()
global actx;
time = get(get(actx,'controls'),'currentposition');
ct = datenum(0,0,0,0,0,time);
end