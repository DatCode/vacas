function makeSignalAxesStem(t,n)
global video_handles;
udf = get(video_handles.uipanel1,'userdata');
limy = get(video_handles.signal_axes,'ylim');
st=getSignalAxesStems();
if nargin<2
    current_st=getClosestStem(st,t);
    cont = length(st);
    if cont<2
        hold(video_handles.signal_axes,'on');
        st = stem(video_handles.signal_axes,[t t],limy);
        ud.name = 'signal limit stem';
        set(st,'userdata',ud);
    end
    if isempty(current_st)
        current_st = st;
    end
    set(current_st,'xdata',[t t]);
    udf.current_stem = current_st;
    set(video_handles.uipanel1,'userdata',udf);
else
    result_st=[];
    switch(n)
        case 'first'
            result_st = st(1);
            xdata1 = get(st(1),'xdata');
            xdata2 = get(st(end),'xdata');
            if xdata1(1)>xdata2(1)
                result_st=st(end);
                if xdata1(1)<t
                    return
                end
            end
        case 'last'
            result_st = st(1);
            xdata1 = get(st(1),'xdata');
            xdata2 = get(st(end),'xdata');
            if xdata1(1)<xdata2(1)
                result_st=st(end);
                if xdata1(1)>t
                   return 
                end
            end
    end
    set(result_st,'xdata',[t t],'ydata',limy);
end
end