function time = getDatenumFromTitle()
global video_handles;
title=get(video_handles.video_panel,'Title');
time = 0;
try
time = datenum(title,'yyyymmdd_HH-MM');
end
end