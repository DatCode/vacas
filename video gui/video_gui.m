function varargout = video_gui(varargin)
% VIDEO_GUI MATLAB code for video_gui.fig
%      VIDEO_GUI, by itself, creates a new VIDEO_GUI or raises the existing
%      singleton*.
%
%      H = VIDEO_GUI returns the handle to a new VIDEO_GUI or the handle to
%      the existing singleton*.
%
%      VIDEO_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VIDEO_GUI.M with the given input arguments.
%
%      VIDEO_GUI('Property','Value',...) creates a new VIDEO_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before video_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to video_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help video_gui

% Last Modified by GUIDE v2.5 20-Sep-2016 19:48:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @video_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @video_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before video_gui is made visible.
function video_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to video_gui (see VARARGIN)
% global peer;
warning('off','all');
%% Crea variables globales
global video_handles;
global video;
video_handles=handles;
video_handles.duration_readed = false;
video_handles.draging = false;
global actx;
global trans_plots; % Variable para los plot a transferir
set(handles.figure1,'renderer','painter');

%A�ade request de se�al a la gui
if ~isempty(varargin)
    set(handles.uipanel1,'userdata',varargin{1});
end

% set(get(hObject,'children'),'parent',handles.figure1);
% btngrouppos = get(handles.sel_eje,'position');
% delete(handles.sel_eje);
% handles.sel_eje = uibuttongroup(handles.figure1,...
%                   'Title','Seleccionar eje',...
%                   'Position', btngrouppos,...
%                   'SelectionChangedFcn',@(bg,event) eje_changed(bg,event));

%% Aniade paths
addpath(genpath([pwd '\Lib\']));
addpath([pwd '\..\Source\SQL\']);
addpath([pwd '\..\Source\SQL\sql_vacas\']);
addpath([pwd '\..\Source\SQL\sql functions\']);
addpath([pwd '\..\Source\SQL\etiquetas\']);
addpath([pwd '\..\Source\SQL\time functions\']);
addpath([pwd '\..\rest\']);

handles.output = hObject;
%% Configuracion inicia|l de windows media player
global h;
% hud = get(h.selected_axes,'userdata');
% set(handles.signal_axes,'userdata',hud);
if isempty(trans_plots)
% url='http://146.83.206.92/Videos/videos2/fixed/20160115_21-00-01-001.mkv';
%     set_url(url);
    signal_struct = sel_signal();
    ud = get(handles.signal_axes,'userdata');
    signal_struct.eje_x=signal_struct.eje_x-mean(signal_struct.eje_x);
    signal_struct.eje_y=signal_struct.eje_y-mean(signal_struct.eje_y);
    signal_struct.eje_z=signal_struct.eje_z-mean(signal_struct.eje_z);
    ud.signal = signal_struct;      
else
    transud = get(trans_plots.ax,'userdata');
    signal_struct = transud.datos;
    idx = signal_struct.it>=trans_plots.ti & signal_struct.it<=trans_plots.tf;
    signal_struct.it=signal_struct.it(idx);signal_struct.eje_x=signal_struct.eje_x(idx);
    signal_struct.eje_y=signal_struct.eje_y(idx);signal_struct.eje_z=signal_struct.eje_z(idx);
    
    signal_struct.eje_x=signal_struct.eje_x-mean(signal_struct.eje_x);
    signal_struct.eje_y=signal_struct.eje_y-mean(signal_struct.eje_y);
    signal_struct.eje_z=signal_struct.eje_z-mean(signal_struct.eje_z);
    
    url = getVideoUrl(trans_plots.ti,trans_plots.tf);    
    ud = get(handles.signal_axes,'userdata');
    ud.signal = signal_struct;
end
set(handles.signal_axes,'userdata',ud);

pud.name = 'signal';
p=plot(handles.signal_axes,signal_struct.it,signal_struct.eje_x,'userdata',pud);
set(handles.signal_axes,'ylim',[min(signal_struct.eje_x)*1.1 max(signal_struct.eje_x)*1.1]);
set(handles.signal_axes,'xlim',[min(signal_struct.it) max(signal_struct.it)]);
datetick(handles.signal_axes,'x',13,'keeplimits');
% draw_spectrogram(p);
clases = getClases();
set(handles.class_popup,'string',clases.clase,'value',1,'userdata',clases);
refreshSubclases();

% Update handles structure
guidata(hObject, handles);
% UIWAIT makes video_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%% Llama a windows media player
pan_pos = vabsolute_position(handles.video_embed);
h2 = actxcontrol('WMPlayer.ocx.7','parent',handles.figure1,'position',pan_pos,...
    'callback',{'CurrentItemChange' 'video_change'; 'PositionChange' 'refresh_time'; 'EndOfStream' 'video_end'},...
    'isRemote',1);
actx = h2;
set(actx,'enablecontextmenu',0);
if isempty(trans_plots)
    url='http://146.83.206.92/Videos/videos2/fixed/20160115_21-00-01-001.mkv';
end
set_url(url);
play();
pause(2);
stop();
titulo = get(get(actx,'currentmedia'),'name');
width =  get(get(actx,'currentmedia'),'imagesourcewidth');
height = get(get(actx,'currentmedia'),'imagesourceheight');
network = get(get(actx,'network'));
framerate = network.encodedFrameRate;
duracion = get(get(actx,'currentmedia'),'durationstring');
mode('none');
actx.controls.stop;
set(handles.signal_axes,'userdata',ud);



%% Guarda datos del video en una variable global
video.title=titulo;
video.width = width;
video.height=height;
video.network=network;
video.framerate=framerate;
video.url = url; 

%% Inicializa datos del video mostrados
set(handles.duracion,'string',duracion);
set(handles.video_panel,'title',titulo);
video_handles.cursor = add_cursor(handles.signal_axes);
mode('full'); 

%% Crea timer para actualizar el tiempo
global mytimer2;
mytimer2 = timer('TimerFcn', {@refresh_time},...
    'StartDelay',2,'period',0.5,...
    'ExecutionMode', 'FixedSpacing',...
    'BusyMode','drop');
start(mytimer2)
warning('on','all');

% --- Outputs from this function are returned to the command line.
function varargout = video_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in play_btn.
function play_btn_Callback(hObject, eventdata, handles)
% hObject    handle to play_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)   
play();

% --- Executes on button press in stop_btn.
function stop_btn_Callback(hObject, eventdata, handles)
% hObject    handle to stop_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stop();

% --- Executes on button press in back_btn.
function back_btn_Callback(hObject, eventdata, handles)
% hObject    handle to back_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
step('back');

% --- Executes on button press in pause_btn.
function pause_btn_Callback(hObject, eventdata, handles)
% hObject    handle to pause_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pause_video();

% --- Executes on button press in fullscreen_btn.
function fullscreen_btn_Callback(hObject, eventdata, handles)
% hObject    handle to fullscreen_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fullscreen();

% --- Executes on button press in forward_btn.
function forward_btn_Callback(hObject, eventdata, handles)
% hObject    handle to forward_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
step('forward');

% --- Executes on button press in ejex_box.
function ejex_box_Callback(hObject, eventdata, handles)
% hObject    handle to ejex_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eje_changed(hObject,handles);
% Hint: get(hObject,'Value') returns toggle state of ejex_box

% --- Executes on button press in ejey_box.
function ejey_box_Callback(hObject, eventdata, handles)
% hObject    handle to ejey_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eje_changed(hObject,handles);
% Hint: get(hObject,'Value') returns toggle state of ejey_box


% --- Executes on button press in ejez_box.
function ejez_box_Callback(hObject, eventdata, handles)
% hObject    handle to ejez_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eje_changed(hObject,handles);
% Hint: get(hObject,'Value') returns toggle state of ejez_box


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over back_btn.
function back_btn_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to back_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function signal_axes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to signal_axes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% global video_handles;
% handles = video_handles;
% % Hint: place code in OpeningFcn to populate signal_axes
%     signal_struct = sel_signal();
%     p=plot(signal_struct.it',signal_struct.eje_x,'parent',handles.signal_axes);
%     set(handles.signal_axes,'ylim',[min(signal_struct.eje_x)*1.1 max(signal_struct.eje_x)*1.1]);
%     set(handles.signal_axes,'xlim',[min(signal_struct.it) max(signal_struct.it)]);
%     datetick(handles.signal_axes,'x',13,'keeplimits');
%     p2 =plot(handles.signal_axes,signal_struct.it,signal_struct.eje_x);
%     set(handles.signal_axes,'ylim',[min(signal_struct.eje_x)*1.1 max(signal_struct.eje_x)*1.1]);
%     set(handles.signal_axes,'xlim',[min(signal_struct.it) max(signal_struct.it)]);
%     datetick(handles.signal_axes,'x',13,'keeplimits');
%     draw_spectrogram(p2);


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(hObject);
global mytimer2;
try
delete(mytimer2);
catch
end


% --------------------------------------------------------------------
function sel_eje_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to sel_eje (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function sel_eje_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sel_eje (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function eje_changed(src,handles)
col=get(src,'userdata');
ud = get(handles.signal_axes,'userdata');
signal = ud.signal;
plots = get(handles.signal_axes,'children');
ud = get(plots,'userdata');
id = cell2mat(cellfun(@(x) ~isempty(x),ud,'uniformoutput',false));
ud = ud(id);
if length(ud)>1
    if iscell(ud)
   idx = cellfun(@(x) strcmpi(x.name,'signal'),ud,'uniformoutput',false);
    else
        idx = arrayfun(@(x) strcmpi(x.name,'signal'),ud,'uniformoutput',false);
    end
    idx =cell2mat(idx);
   plots = plots(id);
   sel_plot = plots(idx);
else
    sel_plot = plots(id);
end
set(sel_plot,'ydata',signal.(col));

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over ejex_box.
function ejex_box_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to ejex_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eje_changed(hObject,handles);


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over ejey_box.
function ejey_box_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to ejey_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eje_changed(hObject,handles);


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over ejez_box.
function ejez_box_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to ejez_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eje_changed(hObject,handles);


% --- Executes on button press in addevent_btn.
function addevent_btn_Callback(hObject, eventdata, handles)
% hObject    handle to addevent_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1,'visible','off');
etiquetar();
delete(handles.figure1);

% --- Executes on button press in cancelar_btn.
function cancelar_btn_Callback(hObject, eventdata, handles)
% hObject    handle to cancelar_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);


% --- Executes on selection change in subclass_popup.
function subclass_popup_Callback(hObject, eventdata, handles)
% hObject    handle to subclass_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns subclass_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from subclass_popup


% --- Executes during object creation, after setting all properties.
function subclass_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to subclass_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in class_popup.
function class_popup_Callback(hObject, eventdata, handles)
% hObject    handle to class_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns class_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from class_popup
refreshSubclases();

% --- Executes during object creation, after setting all properties.
function class_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to class_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function video_last_limit_field_Callback(hObject, eventdata, handles)
str = get(hObject,'string');
time = datenum([datestr(getDatenumFromTitle(),'yyyy-mm-dd ') str],'yyyy-mm-dd HH:MM:SS');
%Crea el stem
makeVideoTimeLineStem(time,'last');

% --- Executes during object creation, after setting all properties.
function video_last_limit_field_CreateFcn(hObject, eventdata, handles)
% hObject    handle to video_last_limit_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function video_first_limit_field_Callback(hObject, eventdata, handles)
str = get(hObject,'string');
time = datenum([datestr(getDatenumFromTitle(),'yyyy-mm-dd ') str],'yyyy-mm-dd HH:MM:SS');
%Crea el stem
makeVideoTimeLineStem(time,'first');

% --- Executes during object creation, after setting all properties.
function video_first_limit_field_CreateFcn(hObject, eventdata, handles)
% hObject    handle to video_first_limit_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function signal_last_limit_field_Callback(hObject, eventdata, handles)
str = get(hObject,'string');
global video_handles;
ud = get(video_handles.signal_axes,'userdata');
time = datenum([datestr(min(ud.signal.it),'yyyy-mm-dd ') str],'yyyy-mm-dd HH:MM:SS');
%Crea el stem
makeSignalAxesStem(time,'last');

% --- Executes during object creation, after setting all properties.
function signal_last_limit_field_CreateFcn(hObject, eventdata, handles)
% hObject    handle to signal_last_limit_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function signal_first_limit_field_Callback(hObject, eventdata, handles)
str = get(hObject,'string');
global video_handles;
ud = get(video_handles.signal_axes,'userdata');
time = datenum([datestr(min(ud.signal.it),'yyyy-mm-dd ') str],'yyyy-mm-dd HH:MM:SS');
%Crea el stem
makeSignalAxesStem(time,'first');


% --- Executes during object creation, after setting all properties.
function signal_first_limit_field_CreateFcn(hObject, eventdata, handles)
% hObject    handle to signal_first_limit_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% video_handles.signal_axes
% video_handles.videotimelin.....................e_axes
current_object = gco(handles.figure1);
% udf = get(handles.uipanel1,'userdata');
parent = get(current_object,'parent');
global video_handles;
video_handles.draging = true;
if ~isempty(parent)
    if current_object==handles.signal_axes || parent==handles.signal_axes
        px = get(handles.signal_axes,'currentpoint');
        makeSignalAxesStem(px(1));
    elseif current_object == handles.videotimeline_axes || parent==handles.videotimeline_axes
        px = get(handles.videotimeline_axes,'currentpoint');
        makeVideoTimeLineStem(px(1));
    end
    
end

% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
current_object = gco(handles.figure1);
udf = get(handles.uipanel1,'userdata');
parent = get(current_object,'parent');
global video_handles;
%   isfield(udf,'current_stem')
if ~isempty(parent) && video_handles.draging
    if current_object==handles.signal_axes || parent==handles.signal_axes
        if isfield(udf,'current_stem')
            px = get(handles.signal_axes,'currentpoint');
            makeSignalAxesStem(px(1));
        end
    elseif current_object == handles.videotimeline_axes || parent==handles.videotimeline_axes
        if isfield(udf,'current_video_stem')
            px = get(handles.videotimeline_axes,'currentpoint');
            makeVideoTimeLineStem(px(1));
        end
    end    
    refreshLabelLimits();
end


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonUpFcn(hObject, eventdata, handles)
global video_handles;
video_handles.draging = false;
udf = get(handles.uipanel1,'userdata');
if isfield(udf,'current_stem')
    udf = rmfield(udf,'current_stem');
%     'eliminado'
end
set(handles.figure1,'userdata',udf);
refreshLabelLimits();



% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over addevent_btn.
function addevent_btn_ButtonDownFcn(hObject, eventdata, handles)


% --- Executes on selection change in celo_popup.
function celo_popup_Callback(hObject, eventdata, handles)
% hObject    handle to celo_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns celo_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from celo_popup


% --- Executes during object creation, after setting all properties.
function celo_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to celo_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
