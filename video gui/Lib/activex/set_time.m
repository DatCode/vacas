function set_time(time,objetivo)
global actx;
global video_handles;
set(get(actx,'controls'),'currentposition',time);
if ~ischar(time)
str =datestr(datenum(0,0,0,0,0,time),'HH:MM');
end
switch(objetivo)
    case 'duracion'
        set(video_handles.duracion,'string',str);
    case 'tiempo'
        set(video_handles.tiempo_actual,'string',str);
end
end

