function refreshDuration()
global actx;
global video_handles;
duracion = get(get(actx,'currentmedia'),'durationString');
set(video_handles.duracion,'string',duracion);
if ~strcmpi(duration,'00:00')
    video_handles.duration_readed = true;
    inicio = getDatenumFromTitle();
%     fin = inicio+datenum(['0000_01_00 00:' duracion],'yyyy_mm_dd HH:MM:SS');
    fin = inicio + datenum(0,0,0,1,00,00);
    if fin-inicio>0
        %% Inicializa variables
        ud = get(video_handles.videotimeline_axes,'userdata');
        havetoplotzeroline = true;
        havetostemcurrenttime = true;        
        
        %% Grafica linea en zero
        if isfield(ud,'zero_line')
            if ishandle(ud.zero_line)
               set(ud.zero_line,'xdata',[inicio fin],'ydata',[0 0]);
               havetoplotzeroline=false;
            end
        end
        
        if havetoplotzeroline
            p = plot(video_handles.videotimeline_axes,[inicio fin],[0 0],'color',[0 0 1]);
            ud.zero_line = p;
            uistack(p,'top');
        end
        
        %% Grafica stem que se mueve durante el transcurso del video
        if isfield(ud,'currenttime_stem')
           if ishandle(ud.currenttime_stem)
              set(ud.currenttime_stem,'xdata',repmat(getCurrentVideoTime()+inicio,1,2)); 
              havetostemcurrenttime = false;
           end
        end
        if havetostemcurrenttime
            hold(video_handles.videotimeline_axes,'on');
           s = stem(video_handles.videotimeline_axes, repmat(getCurrentVideoTime()+inicio,1,2), [-.9 .9],'marker','none');
           ud.currenttime_stem = s;
        end
        
        %% Actualiza ud del axes del tiempo de video
        set(video_handles.videotimeline_axes,'userdata',ud);
        
        %% Actualiza limites del axes del tiempo de video
        set(video_handles.videotimeline_axes,'xlim',[inicio fin],'ylim',[-1 1],'ytick',[]);
        datetick(video_handles.videotimeline_axes,'x',13,'keeplimits');
        box(video_handles.videotimeline_axes,'on');
    end
end
end

