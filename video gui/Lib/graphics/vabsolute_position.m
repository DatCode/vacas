function pos = vabsolute_position(el)
parents = get_parents(el);

%Se elimina el handle de la figura
%En cambio se coloca el handle del elemento
if length(parents)>1
%     parents = parents(1:end-1);
    parents(end) = el;
    set(parents,'units','pixels');
    positions = get(parents(1:end),'position');
    if iscell(positions)
        positions = cell2mat(positions);
    end
    x = sum(positions(:,1));
    y = sum(positions(:,2));
    
    el_pos = get(el,'position');
    pos = [x y el_pos(3) el_pos(4)];
else
    pos = get(el,'position');
end
end