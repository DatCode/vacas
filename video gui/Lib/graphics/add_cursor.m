function cursor=add_cursor(ax)
limx = get(ax,'xlim');
limy=get(ax,'ylim');
pud.name = 'cursor';
hold(ax,'on');
cursor=plot(ax,[limx(1) limx(1)],limy*0.95,'r','userdata',pud);
end

