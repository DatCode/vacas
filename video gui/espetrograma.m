clear all
clc

load('datos_esp.mat')
kconver=0.001986667;

[spect_S,spect_F,~,~]=spectrogram(senal,...
                                  hamming(300),... %window
                                  295,... %n overlap 295
                                  1024,... %nfft
                                  100,... %fs (fs fija)
                                  'yaxis');
   DEP_Sdb=10*log10(spect_S.*conj(spect_S));
   imagesc(t,spect_F,DEP_Sdb);axis xy;
   rango_caxis = [50 100];
   caxis(rango_caxis+20.*log10(kconver));%Color axis rango 50-100 db
   ylim([0 20]);% Limita 0 - 20Hz
   ylabel('Frec. [Hz]');
    
