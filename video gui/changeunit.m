function changeunit(file)
% Change unit of all children of a FIG file to normalized

fig = hgload(file);
recurschange(get(fig,'children'));
newfig_file = 'newfoo.fig';
fprintf('New figure saved in %s\n', newfig_file);
hgsave(fig, newfig_file);
close(fig);

end % changeunit

% This function should be located in the changeunit mfile 
function recurschange(h)

set(h, 'Unit', 'normalized')
c = get(h,'children');
for k=1:length(c)
    ck = c(k);
    if ~isempty(ck)
        recurschange(ck);
    end
end

end % recurschange