function st = getVideoAxesStems()
global video_handles;
ch = get(video_handles.videotimeline_axes,'children');
types = get(ch,'type');
idx = strcmpi(types,'stem');
ud = get(ch(idx),'userdata');
cont = 0;
idx2=logical(zeros(1,length(ud)));
if ~isempty(ud)
    for i = 1: length(ud)
        if iscell(ud)
            udi = ud{i};
        else
            udi = ud(i);
        end
        if isfield(udi,'name')
            if strcmpi(udi.name,'video limit stem')
                cont = cont+1;
                idx2(i)=true;
            end
        end
    end
end
st = ch(idx);
st = st(idx2);
end