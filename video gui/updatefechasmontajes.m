set_cow_connector('jose','59575351');
global cow_connector;

sel = 'select id_montaje from vacas_db.montaje';
id = cow_connector.query(sel);

if iscell(id)
    id = cell2mat(id);
end

for i = 1: length(id)
   sel = ['select min(it), max(it) from vacas_db.acc where id_montaje = ' num2str(id(i))];
   datos = cow_connector.query(sel);
   min = datos{1};
   max = datos{2};
   upd = ['update vacas_db.montaje set f_inst = ''' min ''', f_ret=''' max ''' where id_montaje = ' num2str(id(i))];
   cow_connector.query(upd);
end