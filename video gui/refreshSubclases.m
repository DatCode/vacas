function refreshSubclases()
global video_handles;
clases = getClases();
val = get(video_handles.class_popup,'value');

    subclases = getSubclasesByClase(clases.id{val});
    set(video_handles.subclass_popup,'string',subclases.subclase,'value',1);
    
end