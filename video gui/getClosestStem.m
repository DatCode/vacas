function st = getClosestStem(st,point)
difs = zeros(1,length(st));
for i = 1:length(st)
    if iscell(st)
       sti = st{i};
    else
        sti=st(i);
    end
    xdata = get(sti,'xdata');
    difs(i) = abs(xdata(1)-point(1));
end
[~,idx]=min(difs);
st = st(idx);
end