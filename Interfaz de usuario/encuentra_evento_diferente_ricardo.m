global cow_connector;
t1 = datenum(2015,01,1);
t2 = datenum(2017,01,01);
etiquetas = get_etiquetas(t1,t2);

%subclase
%1: monta aceptada
idx1 = and(strcmpi(etiquetas.clase,'mn'),strcmpi(etiquetas.subclase,'montada'));
idxc = cellfun(@(x) ~isempty(x),eventos.subclase);
idx2 = cell2mat(eventos.subclase)==1;
idxc(idxc==1)=idx2;


idx1 = find(idx1);
idxc = find(idxc);
idxf = zeros(1,21);
idxfinal = [];
for i = 1:length(idxc)
   evento = get_events_by_idx(eventos,idxc(i));
   for j = 1:length(idx1)
       evento2 = get_events_by_idx(etiquetas,idx1(j));
       comentarios = strcmpi(evento2.comentarios,evento.comentario);
       inicio = abs(evento2.inicio_senial{1}-evento.inicio_senal{1})<datenum(0,0,0,0,0,1);
       fin = abs(evento2.fin_senial{1}-evento.fin_senal{1})<datenum(0,0,0,0,0,1);
       celo = evento2.celo{1}==evento.celo{1};
       if (comentarios && inicio && fin && celo)
           idxfinal = [idxfinal i];
       end
   end
end