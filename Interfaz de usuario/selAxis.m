function selAxis(src,eventData,ax,campo)
global selected_filter;
ud = get(ax,'userdata');
p = get_signal_plot(ax,'filtered_signal');

%Se�al seleccionada
signal=ud.datos.(campo);
signal=center_signal(signal);
signal=filter_signal(signal,selected_filter);

%Actualiza data propia handles
set(p,'ydata',signal);
ud.eje = campo;
set(ax,'userdata',ud);

%Refresca checked uimenu
set(get(get(src,'parent'),'children'),'checked','off');
set(src,'checked','on');

%Refresca titulo
refresh_filtername_text(ax)
end