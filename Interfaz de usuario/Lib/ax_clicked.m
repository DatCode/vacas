%% Accion al seleccionar un axes
function ax_clicked(varargin)
global h;
global flag;
src=varargin{1};
if flag.choosing_axes || flag.selecting_signal    
    if h.selected_axes == src
        %Clickea el mismo axes        
        selected_event=is_hover_an_event();
        if ~isempty(selected_event)
            %Se ha clickeado una etiqueta
            if ~isempty(h.selected_event) && valid_field(h,'selected_event',0)
                %Desselecciona el evento anterior y selecciona el actual
                if ~isequal(selected_event{1},h.selected_event{1})
                    toggle_event_edgecolor();
%                     h.selected_event{1}.unselect();
                    h.selected_event=selected_event;
%                     selected_event{1}.select();
                    toggle_event_edgecolor();
                    write_dynamic_event();
                end
            else
                %Selecciona el evento
                h.selected_event=selected_event;
%                 selected_event{1}.select();
                write_dynamic_event();
                toggle_event_edgecolor();
            end
        else
            %Desselecciona el evento que se haya seleccionado anteriormente
            if valid_field(h,'selected_event',0)
                if ~isempty(h.selected_event)
                    toggle_event_edgecolor();
                end
            end
            
            %Desselecciona el axes y limpia
            h.selected_axes = 0;
            h.current_signal = 0;
            h.selected_event = 0;
            clear_selection();
            toggle_sel_axes(src);
            clear_dynamic_event();
        end
    else
        %Refresca
%         delete_marks;
        try
            ud = get(h.selected_axes,'userdata');
            delete(ud.crosshairline);
            ud.crosshairline = -1;
            set(h.selected_axes,'userdata',ud);
        catch
        end
        h.selected_axes = src;
        %     disp(num2str(h.selected_axes));
        h.current_signal = get_signal_plot(src,'filtered_signal');
        refresh_counters();
        clear_selection();
        clear_dynamic_event();
        if valid_field(h,'selected_event')
            if ~isempty(h.selected_event)
                toggle_event_edgecolor
            end
            
        end
        h.selected_event=is_hover_an_event();
        toggle_sel_axes(src); %Selecciona axes
        
        %Selecciona evento
        if ~isempty(h.selected_event)
            write_dynamic_event();
            toggle_event_edgecolor();
        else
            writeAxesInfo();            
        end
        toggle_marks;
        toggle_marks;
    end
    refresh_counters();
    refresh_fast_buttons_color('mode','refresh');
    refresh_filtername_text;
else
    if flag.choosing_sync_axes
        options.mode = 'sync';
        toggle_sel_axes(src,options);
        
    end
end
end