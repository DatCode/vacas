function filter_current_axes( varargin )
global h;
global selected_filter;
if valid_field(h,'selected_axes',0)
    if ~isempty(selected_filter)
       ud = get(h.selected_axes,'userdata');
       ud.filtername = selected_filter;
       set(h.selected_axes,'userdata',ud);
       p = get_signal_plot(h.selected_axes,'original_signal');
       ydata = get(p,'ydata');
       xdata = get(p,'xdata');
       ydata = filter_signal(ydata,selected_filter);
       p = get_signal_plot(h.selected_axes,'filtered_signal');
       set(p,'xdata',xdata','ydata',ydata);
       try
           delete(h.event_zoom);
       catch
       end
       show_in_graph({'filtered_signal'},h.selected_axes);
       refresh_filtername_text
    end
end
end

