function centered_signal = center_signal(signal)
    centered_signal = signal-mean(signal); %Resta el valor medio
end