function filtered_signal = filter_signal(signal,filter_name)
% global selected_filter;
% if nargin<2
%     filter_name = selected_filter;
% end
% default = 'Filtro_pasabanda_2_12_orden10';
% if ~isempty(filter_name)
%     filter_mat = [filter_name '.mat'];
% else
%     filter_mat = [default '.mat'];
% end
% variables = whos('-file',filter_mat);
% var_name = variables(1).name;
% load(filter_mat);
% filtered_signal=eval(['filter(' var_name ',signal)']);
filtered_signal = signal;
end