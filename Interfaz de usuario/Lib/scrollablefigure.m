%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function h = scrollablefigure(varargin)
%% Crea la figura
global h;
if isempty(varargin)
%     f_width = 1040;
%     f_height = 720;
%     sz = [f_width f_height]; % figure size
%     screensize = get(0,'ScreenSize');
%     xpos = ceil(screensize(3)/2-sz(1)/2); % center the figure on the
%     ypos = ceil(screensize(4)/2-sz(2)/2); % center the figure on the
%     h.fig = figure('units','pixels','position',[xpos, ypos, sz(1), sz(2)],...
%         'name','Ingreso usuario','color',[0.4 0.35 0.25],...
%         'closerequestfcn',@close_fig,'numbertitle','off','menu','none');
%     
%     %% Crea el panel para el slider
%     pan_width = f_width-60;
%     pan_height = f_height-40-70;
%     h.axes_panel = uipanel('parent',h.fig,'units','pixels','position',[20 20 pan_width pan_height],...
%         'backgroundcolor',[.7 .7 .7]);
end

panpos = get(h.axes_panel,'position');
pan_width = panpos(3);
pan_height=panpos(4);
% %% Crea boton para a�adir plot
% boton_height = 70;
% boton_width = pan_width + 15;
% h.boton = uicontrol('style','pushbutton','parent',h.axes_panel,'string','A�adir plot',...
%     'units','pixels','position',[20 f_height-80 boton_width boton_height],'fontweight','bold',...
%     'callback',@push_button);
end

%% Accion al cerrar la figura
function close_fig(src,callbackdata)
delete(src);
clearvars -global h;
end

%% Accion al mover el scroll
function slider_event(src,callbackdata,varargin)
global h;
handles = h;
slider_val = get(src,'value');
callback = get(src,'callback');
if length(callback)==3
    valor_anterior = callback{3};
else
    valor_anterior = varargin{1};
end
callback{3} = slider_val;
set(src,'callback',callback);

for i=1:length(handles.ax)
    h = handles.ax(i);
    pos_actual = get(h,'position');
    pos_nueva = [0 (valor_anterior-slider_val) 0 0] + pos_actual;
    set(h,'position',pos_nueva);
end
end

%% Accion al seleccionar un axes
function ax_clicked(varargin)
src=varargin{1};
pan = get(src,'parent');
ax_anterior = get(pan,'userdata');
set(pan,'userdata',src);
sel_color = 'blue';
nonsel_color = 'black';

if ~isempty(ax_anterior)
    if ax_anterior==src
        isbox = get(src,'box');
        if strcmpi(isbox,'on')
            box(src,'off');
            set(src,'xcolor',nonsel_color,'ycolor',nonsel_color);
        else
            box(src);
            set(src,'xcolor',sel_color,'ycolor',sel_color);;
        end
    else
        box(ax_anterior,'off');
        set(ax_anterior,'xcolor',nonsel_color,'ycolor',nonsel_color);
        box(src);
        set(src,'ycolor',sel_color,'xcolor',sel_color);
    end
else
    box(src);
    set(src,'ycolor',sel_color,'xcolor',sel_color);
end
end

%% Accion al presionar el boton para a�adir evento
function push_button(src,eventdata)
addAxes();
randomPlot();
end

%% Funcion para crear un axes
function addAxes(varargin)
global h;

%% Maneja entradas
%Crea el objeto capaz de manejar varargin
p = inputParser;

%Se definen los valores por default
defaultparent = h.axes_panel;
pan_position = get(h.axes_panel,'position');
defaultsize = [pan_position(3)-40 100];
possiblepushdir = {'up','down'};
defaultpushdir = 'down';
defaultreference = 0;

if isfield(h,'ax')
    defaultreference = h.ax(end);
end

%A�ade al esquema los argumentos chequeandolos
addOptional(p,'parent',defaultparent,@(x) length(x) == 1 && isnumeric(x));
addOptional(p,'size',defaultsize,@(x) length(x)==2 && isnumeric(x));
addOptional(p,'pushdir',defaultpushdir,@(x) any(validatestring(x,possiblepushdir)));
addOptional(p,'reference',defaultreference,@(x) length(x) == 1 && isnumeric(x));

%Realiza el parse
parse(p,varargin{:});

%Muestra resultados;
resultado = p.Results;

%% Obtiene la posicion de referencia
%Obtiene la posicion de la referencia
if resultado.reference == 0
    refpos = [0 0 pan_position(3:4)];
else
    refpos = get(resultado.reference,'position');
end

%% Define constantes
%Define el espacio entre cada elemento
distance = 50;
%Define margen top
ax_top = 20;

%% A�ade el axes y realiza el push a los demas axes
%Caso en que sea el primer grafico
if refpos(2)==0
    if isfield(h,'ax')
        idx = length(h.ax)+1;
    else
        idx = 1;
    end
    %Calcula punto y
    axy = pan_position(4)-ax_top-resultado.size(2);
    axx = 25;
    h.ax(idx)=axes('parent',resultado.parent,'units','pixels','position',...
        [axx axy resultado.size(1) resultado.size(2)],'buttondownfcn',{@ax_clicked});
else
    %Reemplaza en posicion al axes de referencia
    h.ax(end+1)=axes('parent',resultado.parent,'units','pixels','position',...
        refpos,'buttondownfcn',{@ax_clicked});
    
    %caso en que se desee el siguiente grafico desplace a la referencia
    if strcmpi(resultado.pushdir,'up')
        pushdown(resultado.reference,'this');
        %caso en que se desee el siguiente grafico abajo de la referencia
    else
        pushdown(resultado.reference,'next');
    end
end
end

%% Funcion que permite pushear bot
function pushdown(varargin)
global h;

%Crea el objeto capaz de manejar varargin
p = inputParser;

%Se definen los valores por default
defaultstart = 'this';
possiblestart = {'this','next'};

%A�ade al esquema los argumentos chequeandolos
addOptional(p,'start',defaultstart,@(x) any(validatestring(x,possiblestart)));
addRequired(p,'object',@isnumeric);

parse(p,varargin{:});

resultados = p.Results;

%Pushea los elementos hacia abajo
elementos = get(h.axes_panel,'children');

%filtra los elementos
if strcmpi(resultados.start,'next')
    k = find(elementos~=resultados.object);
    elementos = elementos(k);
end

posiciones =get(elementos,'position');
if iscell(posiciones)
    posiciones = cell2mat(posiciones);
end
posref = get(resultados.object,'position');

if ~isempty(posiciones)
    %Obtiene los elementos que estan por debajo de la referencia
    idx = find(posiciones(posiciones(:,2)<=posref(2)));
    
    %Recorre elementos
    for i=1:length(idx)
        set(elementos(idx(i)),'position',posiciones(idx(i),:)-[0 150 0 0]);
    end
    
    %Checkea bounds
    checknewaxesbounds();
end
end

%% Chequea los componentes se salen del recuadro visible del padre
% De ser necesario crea un scroll para el componente
function checkChildBounds(parent)

end

%% Chequea si un elemento sobrepasa los limites del recuadro del padre
function checknewaxesbounds()
global h;

%Obtiene posiciones
pos = get(gca,'position');
panpos = get(h.axes_panel,'position');

%Define parametros slider
pan_width = panpos(3);
pan_height = panpos(4);
max_slide = abs(pos(2))+50;
slider_height_rate = pan_height/max_slide;
if max_slide>=150
    slider_step = 150/max_slide;
else
    slider_step = 0.5;
end

%Crea el slider en caso de ser necesario
if pos(2)<0
    %Crea el slider
    try
        delete(h.slider);
    catch
        disp('Cant delete slider');
    end
    h.slider = uicontrol('style','slider','parent',h.fig,'position',[pan_width+20 20 15 pan_height],...
        'backgroundcolor',[.8 .8 .8], 'Foregroundcolor',[.1 .1 .1], 'callback',{ @slider_event, max_slide},...
        'max',max_slide,'value',max_slide,'sliderstep',[slider_step slider_height_rate]);
end
end

function randomPlot(varargin)

%Maneja entrada
if nargin<1
    h = gca;
else
    h = varargin{1};
end

%Define vector tiempo
t  = -10:0.1:10;

%Define tipos de plots
a = rand(1,length(t))*10 - rand(1,length(t))*10;

%Plotea
bdf = get(h,'buttondownfcn');
plot(h,t,a,'color',[rand(1) rand(1) rand(1)],'buttondownfcn',bdf);
set(h,'buttondownfcn',bdf);
box(h,'off');
end

%% Evento rueda raton en la figura
function scroll_wheel(varargin)

end