%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function el = findfirstchild(varargin)
parent = varargin{1};
childs = get(parent,'children');
flag = 1;
i = 1;
while(flag)
    prop_name = varargin{2};
    prop_val = get (childs(i),prop_name);
    prop_search = varargin{3};
    try
    if prop_val == prop_search
        flag = 0;
        el = childs(i);
    end
    catch
        try
            if strcmpi(prop_val,prop_search)
                flag = 0;
                el = childs(i);
            end
        catch
            
        end
    end
    i = i+1;
end
end