%% Accion al mover el scroll
function slider_event(src,callbackdata,varargin)
global h;
handles = h;
slider_val = get(src,'value');
callback = get(src,'callback');
if length(callback)==3
    valor_anterior = callback{3};
else
    valor_anterior = varargin{1};
end
callback{3} = slider_val;
set(src,'callback',callback);

try
for i=1:length(handles.ax)
    ax = handles.ax(i);
    pos_actual = get(ax,'position');
    pos_nueva = [0 (valor_anterior-slider_val) 0 0] + pos_actual;
    set(ax,'position',pos_nueva);
end
catch
    
end
end