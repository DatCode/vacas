%CopyRight (c), 2015 Jose Martinez Lavados
function [varargout] = getTxtProperty(file,property)

value = [];
Index = [];
if checkTextFile(file)
    fid = fopen(file,'r');
    texto = textscan(fid,'%s %s');
    Key   = property;
    
    try
        Index = strcmpi(texto{:,1},[Key ':']);
        value = texto{2}{find(Index),:};
    catch 
        warning('No se pudo leer la propiedad');
    end
    fclose(fid);
end

varargout{1} = value;
if nargout>1
    varargout{2}=Index;
end
end