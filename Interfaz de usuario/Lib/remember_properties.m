function remember_properties(file,handle,element,property)
val = getTxtProperty(file,element);
if ~isempty(val)
    set(handle,property,val);
end
end