%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function testfigure(varargin)
global h;
global drag;
drag = struct('start', [], 'points',[],'end',[]);
h.fig = figure('windowstyle','docked','windowbuttonmotionfcn',@mouse_move,...
    'windowbuttondownfcn',@buttondown,'windowbuttonupfcn',@buttonup,...
    'units','pixels');

h.p(1) = random_plot();
h.rect=[];
h.ax = get(h.p(1),'parent');
remember_limits(h.p(1));
end

function mouse_move(src,evt)
global isdragging;
global clickup;
global drag;
global h;

p = get_plots(gca);

%Cambia el cursor
if ishover(h.fig,h.ax)
    set(h.fig,'pointer','crosshair');
else
    set(h.fig,'pointer','arrow');
end

%Cuando esta draggeando
if(isdragging)
    isdragging = false;
    %Obtiene el punto donde se encuentra el puntero
    cp = get(h.fig,'currentpoint');
    
    %Chequea si esta sobre el axes
    if ishover(src,h.ax)
        
        %Selecciona el trozo de se�al momento a momento
        [h.rect, sel_time_range] = select_signal(h.ax,drag.start,h.rect);
        try
            if ~isempty(p(2))
                if ishandle(p(2))
                    %Pinta el trozo de se�al haciendo el slide
                    brand_selected_signal(h.p(1),sel_time_range,'red',h.p(2));
                else
                end
            end
        catch
            %             disp('Error: no existe el sliced plot');
        end
    end
    
    if ~clickup %Chequea si ya se ha soltado el click
        isdragging = true;
    end
end
end

%Al soltar el click del mouse
function buttonup(src,evt)
global isdragging;
global clickup;
global drag;
global h;

clickup= true;
isdragging = 0;

drag.end = get(src,'currentpoint');

if(sum(drag.end == drag.start)~=2)    
    %Elimina el rectangulo al soltar el click
    if(~isempty(h.rect))
        if ishandle(h.rect)
            delete(h.rect);
            h.rect = [];
        end
    end
end
end

%Al apretar el click del mouse
function buttondown(src,evt)
global isdragging;
global clickup;
global drag;
global h;
isdragging = 1;
clickup = false;
drag.start = get(src,'currentpoint');
cp = hover(src,h.ax);

if ishover(gcf,gca) %Chequea que el cursor este sobre el axes
    [h.rect, sel_time_range] = select_signal(h.ax,drag.start);%Obtiene el rango de tiempos seleccionado y crea el rect
    if length(h.p)>1 %Chequea si existe el sliced plot
        if ~isempty(h.p(2))
            if ishandle(h.p(2))%Chequea si aun es un handles
                joinPlots(h.p(1),h.p(2));%Une los plots
            end
        end
    end
    try
        h.p(2)=brand_selected_signal(h.p(1),sel_time_range,'red');%Crea el sliced plot
    catch
        disp('buttondown(): Error: attempt to brand_select_signal');
    end
end
end