function filter_listbox_events(filter)
global h;
global event_filter;
event_filter = [];
can_dir = true;
%Obtiene los eventos
userdata = get(h.event_listbox,'userdata');
events = userdata.events;

display_state('Filtrando lista de eventos...');

%Aniade parametros tipo, creador y revisor al filtrado
if valid_field(filter,'tipo','')
    event_filter.tipo = filter.tipo;
end
if valid_field(filter,'creador','')
    event_filter.creador = filter.creador;
end
if valid_field(filter,'revisor','')
    event_filter.modificador = filter.revisor;
end

%Realiza el primer filtro
filtered_events = master_filter(events);

%Verifica ordenado por variable
if valid_field(filter,'variable','') && valid_field(filter,'orden','')
    event_filter.variable = filter.variable;
    event_filter.orden = filter.orden;
    variable= filter.variable;
    orden=filter.orden;
    variables = {'Inicio';'Duracion';'Fecha creacion';'Fecha modificacion';'Creador';'Revisor'};
    eq_var_field = {'inicio';'';'f_creacion';'f_modificacion';'creador';'modificador'};
    
    ordenes = {'ascendente','descendente'};
    if ~isempty(variable) && ~isempty(orden)
        idx_sel_var = find(strcmpi(variables,variable));
        idx_sel_ord = find(strcmpi(ordenes,orden));
    end
    
    if idx_sel_var ==2
        ti=cell2mat(filtered_events.inicio);tf=cell2mat(filtered_events.fin);
        duracion = tf-ti;
        val_var=duracion;
    else
        val_var=filtered_events.(eq_var_field{idx_sel_var});
        
        if ~ischar(val_var{1})
            val_var=cell2mat(val_var);
            can_dir = false;
        else
            if idx_sel_var==3 || idx_sel_var==4
                val_var = char(val_var);
                val_var = val_var(:,1:end-2);
                val_var=cellstr(val_var);
                val_var = datenum(val_var,'yyyy-mm-dd HH:MM:SS');
            end
        end
    end
    if can_dir
        if idx_sel_ord==1
            [b,idx]=sort(val_var,'ascend');
            
        else
            [b,idx]=sort(val_var,'descend');
        end
    else
        [b,idx]=sort(val_var);
    end
    %Ordena
    filtered_events = get_events_by_idx(filtered_events,idx);
end

%Verifica seccionado de eventos
if valid_field(filter,'seccion','')
    event_filter.seccion = filter.seccion;
    if ~isempty(filter.seccion)
        seccion = filter.seccion;
        secciones = {'Tipo';'Creador';'Revisor'};
        idx_sel_sec = strcmpi(secciones,seccion);
        eq_seccion = {'tipo';'creador';'modificador'};
        field = char(eq_seccion(idx_sel_sec));
        unicos = unique(filtered_events.(field));
        indices = [];
        if ~strcmpi(field,'tipo')
            for i = 1: length(unicos)
                
                indices = [indices;find(strcmpi(filtered_events.(field),unicos(i)))];
            end
        else
            normal_labels = {'lp';'vt';'tc';'tr'};
            ot_finished = false;
            for i = 1: length(unicos)
                if sum(strcmpi(normal_labels,unicos(i)))==1
                    indices = [indices;find(strcmpi(filtered_events.(field),unicos(i)))];
                else
                    if ~ot_finished
                        indices = [indices;get_ot_indexes(filtered_events,'find')];
                        ot_finished = true;
                    end
                end
            end
        end
        %Secciona los eventos
        filtered_events = get_events_by_idx(filtered_events,indices);
    end
end

%Escribe los eventos en la listbox
write_event_listbox(filtered_events);
userdata.filtered_events = filtered_events;
set(h.event_listbox,'userdata',userdata);

refresh_counters();

display_state('Listo...');
end