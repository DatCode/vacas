function events = events_to_struct(T,E,N,G,C,F,CE,CT)
events.tiempos =T;
events.tipo = E;
events.nombre = N;
events.grupo = G;
events.comentario = C;
events.frecuencia = F;
estaciones = {};
for i = 1:length(CE)
    try
        estaciones{i}=CE{i}(1:end-1);
        componentes{i}=CE{i}(end);
    catch 
        estaciones{i}='LAV';
        componentes{i}='Z';
    end
end
%     events.estacion = arrayfun(@(i) CE{i}(1:end-1),1:length(CE),'UniformOutput',false);
%     events.componente = arrayfun(@(j) CE{j}(1,end),1:length(CE),'UniformOutput',false);
events.estacion = estaciones';
events.componente=componentes';
events.timestamp = CT;


end