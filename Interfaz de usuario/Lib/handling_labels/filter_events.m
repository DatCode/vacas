function filtered_events = filter_events(events,field,value)
filtered_events = [];
idx = [];
if ismatrix(value) && ~ischar(value) || ischar(value) && ~isrow(value)
%     filtered_events = filter_events(events,field,value{1});
%     for i=2:length(value)
%        filtered_events = join_events(filtered_events,filter_events(events,field,value{i}));
%     end
a.(field)=value;
filtered_events = union_events_by_field(events,a,field,'union');
    return;
else
    if iscell(value)
        value = {1};
    end
    
    if valid_field(events,field)
        if ischar(events.(field){1})
            idx = find(strcmpi(events.(field),value));            
        else
            array = cell2mat(events.(field));
            idx = find(array==value);
        end
    else
        filtered_events = events;
    end
end
if ~isempty(idx)
%     filtered_events.tiempos=events.tiempos(idx,:);
%     filtered_events.tipo = events.tipo(idx);
%     filtered_events.grupo = events.grupo(idx);
%     filtered_events.comentario = events.comentario(idx);
%     filtered_events.frecuencia = events.frecuencia(idx);
%     filtered_events.estacion = events.estacion(idx);
%     filtered_events.componente = events.componente(idx);
%     filtered_events.nombre = events.nombre(idx);
%     filtered_events.timestamp = events.timestamp(idx);
    fields = fieldnames(events);
    for i=1:length(fields)
        filtered_events.(fields{i})=events.(fields{i})(idx,:);
    end
end
end