%Deselecciona todas las etiquetas del axes
function unselectAllEventsInAxes(ax)
global h;
ud = get(ax,'userdata');
if isfield(ud,'events')
    etiquetas = ud.events;
    if ~isempty(etiquetas)
        cellfun(@(x) x.unselect(),etiquetas,'uniformoutput',false);        
    end
end
h.selected_event=0;
end