function events = get_events_by_idx(events,idx)
if isstruct(events)
    fields = fieldnames(events);
    if isrow(idx)
        idx=idx';
    end
    for i=1:length(fields)
        %         if isboolean(idx)
        %             events.(fields{i})=events.(fields{i})(idx);
        %         else
        events.(fields{i})=events.(fields{i})(idx,:);
        %         end
    end
end
end