function sel_event_from_list(varargin)
global h;
value = get(h.event_listbox,'value');
userdata = get(h.event_listbox,'userdata');
if valid_field(userdata,'events')
   evento = get_events_by_idx(userdata.filtered_events,value);
   display_event_data(evento);
end
end