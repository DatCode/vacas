function write_event_listbox(events)
global colores;
global h;
str={};
    largo = length(events.inicio_senial);
    for i=1:largo
        clase = lower(events.clase{i});
        if valid_field(colores,clase,[-1 -1 -1])
            try
            hex_color = rgb_to_hex(colores.(clase));
            catch
                
                a=1;
            end
            str = [str;{['<html><p style ="color:#' hex_color '">' events.clase{i} '|' ...
            events.nombre_experto{i} '|' datestr(events.inicio_senial(i),'yyyy-mm-dd HH:MM:SS') '</p></html>' ]}];
        else
            clase = 'unknow';
            hex_color = [dec2hex(colores.(clase)(1)*255) dec2hex(colores.(clase)(2)*255)  dec2hex(colores.(clase)(3)*255)] ;
            if length(hex_color)<6
                for j=1:6-length(hex_color)
                    hex_color = [hex_color '0'];
                end
            end
            str = [str;{['<html><p style ="color:#' hex_color '">' events.clase{i} '|' ...
            events.nombre_experto{i} '|' datestr(events.inicio_senial{i},'yyyy-mm-dd HH:MM:SS') '</p></html>']}];
        end
    end   
    set(h.event_listbox,'value',1);
    set(h.event_listbox,'string',str);
%     refresh_counters();
end