function filtered_events=master_filter(events)
global event_filter;
if isstruct(event_filter)
%     fields = {'nombre','tiempos','tipo','grupo','comentario','frecuencia','estacion','componente'};
    fields = fieldnames(events);
    filtered_events =[];
    for i=1:length(fields)
        if valid_field(event_filter,fields{i})
            
            
            events = filter_events(events,fields{i},event_filter.(fields{i}));
            
            if ~isempty(filtered_events)
                filtered_events=union_events_by_field(filtered_events,events,'id_evento','inner');
            end
            
            if isempty(filtered_events) && ~isempty(events)
               filtered_events = events; 
            end        
        end
    end
%     filtered_events = events;
else
    filtered_events = events;
end
end