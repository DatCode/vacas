function force_listbox(events)
global h;
filtered_events = master_filter(events);

userdata.events = events;
userdata.name = 'Lista eventos';
userdata.filtered_events = filtered_events;
set(h.event_listbox,'userdata',userdata);

%Ordena por tiempo de inicio
if ~isempty(filtered_events)    
    
    %Escribe los eventos
    write_event_listbox(filtered_events);
else
    clear_event_listbox();
end

refresh_counters();

%%ASI ERA PARA LOS VOLCANES
% global h;
% % if ~isempty(events)
% %     [b,idx] = sort(cell2mat(events.inicio),'ascend');
% %     events = get_events_by_idx(events,idx);
% % end
% filtered_events = master_filter(events);
% 
% userdata.events = events;
% userdata.name = 'Lista eventos';
% userdata.filtered_events = filtered_events;
% set(h.event_listbox,'userdata',userdata);
% 
% %Ordena por tiempo de inicio
% if ~isempty(filtered_events)    
%     
%     %Escribe los eventos
%     write_event_listbox(filtered_events);
% else
%     clear_event_listbox();
% end
% 
% refresh_counters();
end