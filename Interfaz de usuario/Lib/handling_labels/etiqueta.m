classdef etiqueta < handle
   properties
       event_struct;
       lines_handle;
       text_handle;
       axes_handle;
       selected;
       visible;
       tri1;
       tri2;
       color;
   end
   
   methods
       function value=get_start_time(obj)
           value = obj.event_struct.inicio_senial;
       end
       function value = get_end_time(obj)
          value = obj.event_struct.fin_senial; 
       end
       function value = get.event_struct(obj)
          value = obj.event_struct; 
       end
       function value = get_duration(obj)
          value = obj.get_end_time-obj.get_start_time;
       end
       function value = get_class(obj)
           value = obj.event_struct.clase{1};
       end
       function obj=paint(obj)
          [ obj.lines_handle ,~,obj.text_handle] = draw_events (obj.axes_handle,obj.event_struct); 
          obj.color = get(obj.lines_handle,'color');
          set(obj.lines_handle,'showbaseline','off','linewidth',1.5);
          set(obj.text_handle,'HorizontalAlignment','right');
       end
       function value = is_selected(obj)
          value = obj.selected;
       end
       function obj = set_start_time(obj,t)
           obj.event_struct.inicio{1}=t;
           obj.repaint();
       end
       function obj = set_end_time(obj,t)
           obj.event_struct.fin{1}=t;
           obj.repaint();
       end
       function obj = repaint(obj)
          visible_mode = 'on';
          YLim=get(obj.axes_handle,'YLim');
          multiplier = 0.8;
          linewidth = 1.5;
          fontweight = 'normal';
          edgecolor = 'none';
          if obj.is_selected()
             multiplier = 0.95; 
             linewidth = 2.5;
             uistack(obj.lines_handle,'top');
             uistack(obj.text_handle,'top');
             fontweight = 'bold';
             edgecolor = 'black';
          end
          margin=abs(diff(YLim))*0.2;
          ymin=min(YLim);
          ymax = max(YLim);
          ymin=ymin+margin;
          ymax=ymax-margin;
%           if max(YLim)>0
%             ymin=min(YLim)*multiplier;
%             ymax=max(YLim)*multiplier;
%           else
%             ymin = min(YLim)*multiplier;
%             ymax = max(YLim)*(2-multiplier);
%           end
          xmin=obj.get_start_time();
          xmax=obj.get_end_time();
          if(~obj.visible)
            visible_mode = 'off';
          end
          
          set(obj.text_handle,'visible',visible_mode,'position',[obj.get_start_time() ymax*.9 0],...
          'fontweight',fontweight,'edgecolor',edgecolor);
          set(obj.lines_handle,'visible',visible_mode,'ydata',[ymin ymax ymax ymin],...
              'linewidth',linewidth,'xdata',[xmin xmin xmax xmax]);          
          
       end
       function obj = unselect(obj)
          obj.selected = false; 
%           disp('deselecciona etiqueta');
%           delete(obj.tri1);
          obj.repaint();
       end
       function obj = toggle_select(obj)
          if(obj.selected)
              obj.unselect();
          else
              obj.select();
          end
%           disp('toggle');
       end
       function obj = toggle_visible(obj)
%           obj.unselect();
          obj.visible = ~obj.visible;
          obj.repaint();
          
       end
       function obj = select(obj)
          obj.selected=true; 
%           disp('selecciona etiqueta');
          obj.repaint();
       end
       function obj=erase(obj)
          delete(obj.text_handle);
          delete(obj.lines_handle);
          obj.text_handle = [];
          obj.lines_handle = [];
       end
       
       %CONSTRUCTOR
       function obj = etiqueta(ax,event)
          obj.event_struct = event;
          obj.lines_handle = [];
          obj.text_handle = [];
          obj.axes_handle = ax;
          obj.selected = false;
          obj.color = [];
       end
       
       function obj = set_visible(obj,option)
          set(obj.lines_handle,'visible',option);
          set(obj.text_handle,'visible',option);
          obj.repaint();
       end
       function obj = visible_on(obj)
          obj.visible = true;
          obj.repaint();
       end
       function obj = visible_off(obj)
          obj.visible = false;
          obj.repaint();
       end
       function value = ishover(obj,pointx)
          if obj.get_end_time>=pointx && obj.get_start_time<=pointx
%               disp([datestr(obj.get_end_time) '>=' datestr(pointx) ' && ' datestr( obj.get_start_time) '<=' datestr(pointx)]);
              value = true;
          else
              value = false;
          end
       end
   end
end
