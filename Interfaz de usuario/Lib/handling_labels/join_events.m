function [ output ] = join_events( event1,event2 )
%JOIN_EVENTS Summary of this function goes here
%   Detailed explanation goes here
if isempty(event1) && ~isempty(event2)
   output = event2;
   return;
else
    if isempty(event2) && ~isempty(event1)
        output = event1;
        return;
    else
        if isempty(event2) && isempty(event1)
            output = [];
           return; 
        end
    end
end
fields = fieldnames(event1);
output = event1;
for i = 1:length(fields)
    output.(fields{i}) = [output.(fields{i});event2.(fields{i})];
end
end