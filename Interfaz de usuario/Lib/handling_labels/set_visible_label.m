function set_visible_label(varargin)
global h;

if nargin<2
    ax = h.selected_axes;
    id = varargin{1};
else
    id = varargin{2};
    ax = varargin{1};
end

if iscell(id)
    id = id{1};
end

if iscell(ax)
    ax = ax{1};
end
ud = get(ax,'userdata');
ev = ud.etiquetas;
for i = 1:length(ev)
    if ev{i}.event_struct.id_etiqueta == id
        ev{i}.visible_on();
    else
        ev{i}.visible_off();
    end
end
% child=get(ax,'Children');
% ylim = get(ax,'ylim')*0.8;
% ymin=ylim(1);
% ymax = ylim(2);
% %     sel_child = h.selected_event;
% for n=1:length(child)
%     type=get(child(n),'Type');
%     if strcmp(type,'patch')
%         ud = get(child(n),'userdata');
%         if id~=ud.event.id_evento{1}
%             set(child(n),'Visible','off');
%         else
%             ydata = [ymin ymin ymax ymax];
%             set(child(n),'Visible','on','ydata',ydata);
%         end
%     end
% end

% end
end

