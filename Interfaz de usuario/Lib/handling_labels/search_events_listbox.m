function search_events_listbox(varargin)
waitfor(search_for_events_form());
global event_request;
global session;
global h;
global event_filter;
global colores;

ti = datenum(event_request.fecha,'yyyy-mm-dd');
tf = datenum(event_request.fecha,'yyyy-mm-dd') + str2num(event_request.dias);

if ~isempty(event_request.estacion)
    event_filter.estacion = event_request.estacion;
else
    event_filter = [];
end
%     event_filter.componente = event_request.componente;

%Busca eventos
% [T,E,N,G,C,F,CE,CT] = findEvent(ti,tf,session.user, session.pass);
% events = events_to_struct(T,E,N,G,C,F,CE,CT);
events = g_findEvent(ti,tf);

force_listbox(events);
end