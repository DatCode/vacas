function display_event_data(evento)
global h;
global colores;
html = '<html>';
ehtml = '</html>';
try
p = ['<p style = "color:#' num2str(rgb_to_hex(colores.(lower(evento.clase{1})))) '">'];
catch
   p= ['<p style = "color:#' num2str(rgb_to_hex(colores.unknow)) '">'];
end

str={[html p strong('Clase: ') evento.clase{1} '</p>' ehtml];...
    [html p strong('Subclase: ') evento.subclase{1} '</p>' ehtml];...
    [html strong('Experto: ') evento.nombre_experto{1} ehtml];...
    [html strong('Correo: ') evento.correo_experto{1} ehtml];...
%     [html strong('MAC: ') evento.mac{1}];...
    [html strong('Inicio: ') datestr(evento.inicio_senial(1), 'yyyy-mmm-dd HH:MM:SS','local') ehtml];...
    [html strong('Fin: ') datestr(evento.fin_senial(1), 'yyyy-mmm-dd HH:MM:SS','local') ehtml];...
    [html strong('Duracion: ') estimateDurationStr(evento.fin_senial(1),evento.inicio_senial(1)) ehtml];...
    [html strong('Fecha creacion: ') evento.fecha_creacion{1} ehtml];...
    [html strong('Comentarios: ') evento.comentarios{1} ehtml];...
    '------------------------------'};
set(h.dynamic_event_data,'string',str);
set(h.dynamic_event_data,'value',length(str));
set(h.dynamic_event_data,'enable','on');
end

function duration=estimateDurationStr(t1,t2)
d=abs(t2-t1);
if d>=1
   duration = [num2str(d) ' dias'];
else
    if d>=1/24
       duration = [num2str(d*24) ' horas']; 
    else
        if d>=1/24/60
           duration = [num2str(d*24*60) ' minutos']; 
        else
            duration = [num2str(d*24*60*60) ' segundos'];
        end
    end
end
end