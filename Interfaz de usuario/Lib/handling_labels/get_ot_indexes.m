function idx = get_ot_indexes(events,array_type)
ia = strcmpi(events.tipo,'lp');
ib = strcmpi(events.tipo,'vt');
ic = strcmpi(events.tipo,'tc');
id = strcmpi(events.tipo,'tr');
idx = or(ia,ib);
idx = or(idx,ic);
idx = or(idx,id);
idx = ~idx;

if nargin>1
   if strcmpi(array_type,'find')
      idx = find(idx); 
   end
end
end