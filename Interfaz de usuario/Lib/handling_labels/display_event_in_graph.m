function ax=display_event_in_graph(event,ax)
global h;
if nargin<2
    if valid_field(h,'selected_axes',0)
        ax = h.selected_axes;
    else
        ax = gca;
    end
    if nargin<1
        ax = display_event_in_graph(h.selected_event{1}.event_struct,ax);
        return;
    end
end

%Limites temporales del evento
tie = event.inicio_senial;
tfe = event.fin_senial;
anchot = tfe-tie; %Ancho total del evento
p_margen = 0.5; %Porcentaje a dejar como margen

%Limites finales deseados
ti=tie-anchot*p_margen/3;
tf=tfe+anchot*p_margen/3;

if valid_handle(ax)
    ud=get(ax,'userdata');
    %     if strcmpi(event.estacion{1},ud.estacion)
    
    %Tiempo de inicio y fin de la se�al
    p = get_signal_plot(ax,'filtered_signal');
    xdata = get(p,'xdata');
    ydata = get(p,'ydata');
    tip = xdata(1);
    tfp=xdata(end);
    
    if tip<=ti && tfp>=tf
        idx = find(xdata>=ti & xdata<=tf);
        zoom_p = plot(ax,xdata(idx),ydata(idx),'b');
        userdata.name='event_zoom';
        set(zoom_p,'userdata',userdata);
        set(p,'visible','off');
        remember_limits(zoom_p);
        set_visible_label(ax,event.id_etiqueta);
        refresh_filtername_text(ax);
        return;
    else
        
        setSignalLimits(ax,[ti tf],event);
        try
            refresh_events_in_graph(h.view_axes);
        end
        ax=display_event_in_graph(event,ax);
        refresh_filtername_text(ax);
        return;
    end
    %     else
%         try
%            delete(ax);
%         end
%         h.ax=validate_handles(h.ax);
%     end
end
% datos.componente = 'z';
% datos.estacion = event.estacion{1};
% datos.ti=ti;
% datos.tf=tf;
% datos.mode = 'replace';
% datos.display_event_axes = true;
% 
request.coly = 'eje_z';
request.colx = {'it'};
sensor = 'acelerometro';
request.tabla = get_sensor_table(sensor);
request.db = 'vacas_db';
request.range_x = [ti tf];
request.fuente = 'id montaje';
request.fuente_val = event.id_montaje;
request.sensor = sensor;
ax=sql_plot(request);

if ishandle(ax)
    display_event_in_graph(event,ax);
end
% pause(0.5);
% refresh_filtername_text(ax);
end