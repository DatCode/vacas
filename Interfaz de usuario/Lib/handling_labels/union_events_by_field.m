function [ events ] = union_events_by_field( event1,event2,field ,option)

if ~isempty(event1) && isempty(event2)
    events = event1;s
    return;
else
    if ~isempty(event2) && isempty(event1)
        events = event2;
        return;
    else
        if isempty(event2) && isempty(event1);
            events=[];
            return;
        end
    end
end

% largo = length(event2.(field));
% idx={};
% if ischar(event1.(field){1})
% index = cellfun(@strcmpi,event1.(field),event2.(field))
% events = get_events_by_idx(event1,index);
% else
%     r=intersect(event1.(field),event2.(field));

if ischar(event1.(field){1})
    event2.(field)=unique(event2.(field));
    
    %     r=intersect(event1.(field),event2.(field));
    if strcmpi(option,'inner')
        idx=ones(length(event1.(field)),1);
        for i=1:length(event2.(field))
            coincidencias = strcmpi(event1.(field),event2.(field)(i));
            idx = and(idx,coincidencias);
        end
    else
        idx=zeros(length(event1.(field)),1);
        for i=1:length(event2.(field))
            if strcmpi(field,'tipo') && strcmpi(event2.(field)(i),'ot')
                coincidencias = get_ot_indexes(event1);
            else
                coincidencias = strcmpi(event1.(field),event2.(field)(i));
            end
            idx = xor(idx,coincidencias);
        end
    end
    
else
    idx = [];
    a = cell2mat(event1.(field));
    b = unique(cell2mat(event2.(field)));
    %     str = [];
    %     cellfun(@(i) event1.(field)==event2.(field)(i),{1:length(event2.(field))},'UniformOutput',false);
    for i=1:length(event2.(field))
        idx=[idx;find(a==b(i))];
        %         str=[str '  event1.(field) ==  event2.(field)(' num2str(i) ',:) ' operator];
    end
    
    %     str = str(1:end-1);
    %     idx = eval(str);
end

events = get_events_by_idx(event1,idx);
% end
end

