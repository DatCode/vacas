function refresh_counters()
global h;
global filter_user;
total = 0;

if h.counter_object~=h.event_listbox
    h.counter_object = h.selected_axes;
end

if valid_field(h,'counter_object',0)
    user_data = get(h.counter_object,'userdata');
    if valid_field(user_data,'filtered_events','')
        user_data.events=user_data.filtered_events;
    else
%         str = get(h.counter_object,'string');
    end
    if valid_field(user_data,'events','')
        events = user_data.events;
        if filter_user
            
        else
            for i=1:length(h.event_count)
                try
                    label = get(h.event_count_title(i),'string');
                    label = lower(label);
                    
                    tipos = cellfun(@(e) e.get_type(),events,'uniformoutput',false);
                    tipos = tipos(strcmpi(tipos,label));
                    
                    set(h.event_count(i),'string',length(tipos));
                    
                    total = total+length(tipos);
                catch
                    
                end
            end
        end
        
        set(h.event_count(1),'string',total);
    else
        clear_counters();
    end
else
    clear_counters();
end
end