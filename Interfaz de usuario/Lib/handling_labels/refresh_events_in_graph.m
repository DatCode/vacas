function refresh_events_in_graph(varargin)
global h;
global session;
global filter_user;

%Obtiene tiempos de inicio y fin
display_state('Buscando eventos...');
if nargin==0
    if valid_field(h,'selected_axes',0)
        ax=h.selected_axes;
        p = get_signal_plot(ax,'filtered_signal');
        xdata = get(p,'xdata');
        ti = xdata(1);
        tf = xdata(end);
    else
        ax=[];
        return;
    end
else
    if nargin<2
        ax=varargin{1};
        p = get_signal_plot(ax,'filtered_signal');
        xdata = get(p,'xdata');
        ti = xdata(1);
        tf = xdata(end);
    else
        ti=varargin{1};
        tf=varargin{2};
    end
end
%Busca eventos
if filter_user
    % [T,E,N,G,C,F,CE,CT] = findEvent(ti,tf,session.user, session.pass,['eventos_' session.grupo '_' session.user]);
    events = get_etiquetas(ti,tf);
else
    %    [T,E,N,G,C,F,CE,CT] = findEvent(ti,tf,session.user, session.pass);
    events = get_etiquetas(ti,tf);
end

userdata = get(ax,'userdata');
if isfield(events,'id_predial')
events = get_events_by_idx(events,events.id_predial==str2num(userdata.predial));
etiquetas = arrayfun(@(x) etiqueta(ax,get_events_by_idx(events,x)),(1:length(events.id_etiqueta)),'uniformoutput',false);
cellfun(@(x) x.paint(),etiquetas,'uniformoutput',false);

userdata.events = events;
userdata.etiquetas = etiquetas;
set(ax,'userdata',userdata);
end
display_state('Listo...');
end