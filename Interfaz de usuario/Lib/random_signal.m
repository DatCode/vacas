function signal = random_signal(points, max_amp)
signal = rand(1,points)*max_amp;
end