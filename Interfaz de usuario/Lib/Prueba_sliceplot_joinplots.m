t1 = -5;
t2 = 10;
points = 10000;
t = linspace(t1,t2,points);
a = sin(20*t/pi);
b = sin(10*t/pi)*2;
c = sin(5*t/pi)*0.5;
x = a+b-c;

f=figure('windowstyle','docked');
% figure;
p1 = plot(t,x);
ax = get(p1,'parent');

p2 = slicePlot(p1,0,5,'r');

pause();
joinPlots(p1,p2);

%Crea el rectangulo transparente
a = annotation('rectangle', [0.5 0.5 0.1 0.1],'facecolor','blue');
set(a, 'FaceAlpha', 0.5);