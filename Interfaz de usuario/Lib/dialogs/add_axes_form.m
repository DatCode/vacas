function varargout = add_axes_form(varargin)
% ADD_AXES_FORM MATLAB code for add_axes_form.fig
%      ADD_AXES_FORM, by itself, creates a new ADD_AXES_FORM or raises the existing
%      singleton*.
%
%      H = ADD_AXES_FORM returns the handle to a new ADD_AXES_FORM or the handle to
%      the existing singleton*.
%
%      ADD_AXES_FORM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADD_AXES_FORM.M with the given input arguments.
%
%      ADD_AXES_FORM('Property','Value',...) creates a new ADD_AXES_FORM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before add_axes_form_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to add_axes_form_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help add_axes_form

% Last Modified by GUIDE v2.5 15-Oct-2015 16:32:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @add_axes_form_OpeningFcn, ...
    'gui_OutputFcn',  @add_axes_form_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before add_axes_form is made visible.
function add_axes_form_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to add_axes_form (see VARARGIN)

calendar_button = image_button(handles.uipanel4,[215 237 37 37],@()uicalendar('destinationui',...
    handles.fecha,'selectiontype',1,'windowstyle','modal','outputdateformat','yyyy-mm-dd'),'calendar.png');
set(get(calendar_button,'parent'),'position',[263 163 37 37])

% Choose default command line output for add_axes_form
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% display_state('Esperando accion del usuario...');
% UIWAIT makes add_axes_form wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%r1 = z
%r9=n
%r10=e
a=1;
filename = 'add_axes.txt';
if ~exist(filename)
    fid = fopen(filename,'w');
    fclose(fid);
end

val=getTxtProperty(filename,'eje');
if ~isempty(val)
    r={'1' '9' '10'};
    ejes = {'z','n','e'};
    r= r{strcmpi(ejes,val)};
    set(handles.Componente,'selectedobject',handles.(['radiobutton' r]));
end
val=getTxtProperty(filename,'estacion');
if ~isempty(val)
    set(handles.estacion,'value',find(strcmpi(get(handles.estacion,'string'),val)));
end

remember_properties(filename,handles.hora_inicio,'hora_inicio','string');
remember_properties(filename,handles.fecha,'fecha','string');
remember_properties(filename,handles.minutos,'minutos','string');


% --- Outputs from this function are returned to the command line.
function varargout = add_axes_form_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in estacion.
function estacion_Callback(hObject, eventdata, handles)
% hObject    handle to estacion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns estacion contents as cell array
%        contents{get(hObject,'Value')} returns selected item from estacion


% --- Executes during object creation, after setting all properties.
function estacion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to estacion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minutos_Callback(hObject, eventdata, handles)
% hObject    handle to minutos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minutos as text
%        str2double(get(hObject,'String')) returns contents of minutos as a double


% --- Executes during object creation, after setting all properties.
function minutos_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minutos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function hora_inicio_Callback(hObject, eventdata, handles)
% hObject    handle to hora_inicio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of hora_inicio as text
%        str2double(get(hObject,'String')) returns contents of hora_inicio as a double


% --- Executes during object creation, after setting all properties.
function hora_inicio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to hora_inicio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in aceptar.
function aceptar_Callback(hObject, eventdata, handles)
% hObject    handle to aceptar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global new_axes_form;
sel_object = get(handles.Componente,'selectedobject');
new_axes_form.componente = get(sel_object,'string');
new_axes_form.hora_inicio = get(handles.hora_inicio,'String');
new_axes_form.fecha = get(handles.fecha,'string');
new_axes_form.minutos = get(handles.minutos,'string');
lista = get(handles.estacion,'string');
idx = get(handles.estacion,'value');
new_axes_form.estacion = lista{idx};

%Guarda cambios en la interfaz
filename = 'add_axes.txt';
setTxtProperty(filename,'eje',new_axes_form.componente);
setTxtProperty(filename,'hora_inicio',new_axes_form.hora_inicio);
setTxtProperty(filename,'fecha',new_axes_form.fecha);
setTxtProperty(filename,'minutos',new_axes_form.minutos);
setTxtProperty(filename,'estacion',new_axes_form.estacion);
delete(handles.figure1);

% --- Executes on button press in cancelar.
function cancelar_Callback(hObject, eventdata, handles)
% hObject    handle to cancelar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global new_axes_form;
% str = set_instruction_for_handles(handles);
% cellfun(@(x) disp(x),str);
tic;
% guide_to_codegui(mfilename,handles,'nuevo_axesform');
toc;
new_axes_form = [];
clear global new_axes_form;
delete(handles.figure1);


function fecha_Callback(hObject, eventdata, handles)
% hObject    handle to fecha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fecha as text
%        str2double(get(hObject,'String')) returns contents of fecha as a double


% --- Executes during object creation, after setting all properties.
function fecha_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fecha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function aceptar_ButtonDownFcn(varargin)


