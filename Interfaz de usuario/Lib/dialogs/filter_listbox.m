function varargout = filter_listbox(varargin)
% FILTER_LISTBOX MATLAB code for filter_listbox.fig
%      FILTER_LISTBOX, by itself, creates a new FILTER_LISTBOX or raises the existing
%      singleton*.
%
%      H = FILTER_LISTBOX returns the handle to a new FILTER_LISTBOX or the handle to
%      the existing singleton*.
%
%      FILTER_LISTBOX('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FILTER_LISTBOX.M with the given input arguments.
%
%      FILTER_LISTBOX('Property','Value',...) creates a new FILTER_LISTBOX or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before filter_listbox_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to filter_listbox_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help filter_listbox

% Last Modified by GUIDE v2.5 17-Nov-2015 14:55:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @filter_listbox_OpeningFcn, ...
                   'gui_OutputFcn',  @filter_listbox_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before filter_listbox is made visible.
function filter_listbox_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to filter_listbox (see VARARGIN)
global h;
global event_filter;
userdata = get(h.event_listbox,'userdata');
eventos = userdata.events;
creadores = unique(eventos.creador);
set(handles.creadores,'string',[{'Seleccione creador'};creadores]);
modificadores = unique(eventos.modificador(~strcmpi(eventos.modificador,'null')));
set(handles.revisores,'string',[{'Seleccione revisor'};modificadores]);
variables = {'Inicio';'Duracion';'Fecha creacion';'Fecha modificacion';'Creador';'Revisor'};
set(handles.variable,'string',[{'Seleccione variable'};variables]);
secciones = {'Tipo';'Creador';'Revisor'};
set(handles.seccion,'string',[{'Seleccione seccion'};secciones]);
ordenes = {'Ascendente';'Descendente'};
% Coloca las selecciones anteriores en la interfaz
if isstruct(event_filter)
    %Recuerda tipo anterior seleccionado
    if valid_field(event_filter,'tipo','')
       event_filter.tipo=lower(event_filter.tipo);
       tipos = {'lp','tc','tr','ot','vt'};
       for i =1:length(tipos)
           if sum(strcmpi(event_filter.tipo,tipos{i}))==1
                set(handles.(tipos{i}),'value',1);
           else
               set(handles.(tipos{i}),'value',0);
           end
       end
    end
    
    %Recuerda creador anterior seleccionado
    if valid_field(event_filter,'creador','')
        idx = [];
        for i=1:length(event_filter.creador)
           idx = [idx;find(strcmpi(creadores,event_filter.creador{i}))]; 
        end
        if ~isempty(idx)
            idx=idx+1;
            set(handles.creadores,'value',idx);
        end
    end
    
    %Recuerda revisor anterior seleccionado
    if valid_field(event_filter,'modificador','')
        idx = [];
        for i=1:length(event_filter.modificador)
           idx = [idx;find(strcmpi(modificadores,event_filter.modificador{i}))]; 
        end
        if ~isempty(idx)
            idx=idx+1;
            set(handles.revisores,'value',idx);
        end
    end
    
    %Recuerda variable a ordenar
    if valid_field(event_filter,'variable','')
        idx = [];
        for i=1:length(event_filter.variable)
           idx = [idx;find(strcmpi(variables,event_filter.variable{i}))]; 
        end
        if ~isempty(idx)
            idx=idx+1;
            set(handles.variable,'value',idx);
        end
    end
    
    %Recuerda orden
    if valid_field(event_filter,'orden','')
        idx = [];
        for i=1:length(event_filter.orden)
           idx = [idx;find(strcmpi(ordenes,event_filter.orden{i}))]; 
        end
        if ~isempty(idx)
            idx=idx+1;
            set(handles.orden,'value',idx);
        end
    end
    
    %Recuerda seccionado
    if valid_field(event_filter,'seccion','')
        idx = [];
        for i=1:length(event_filter.seccion)
           idx = [idx;find(strcmpi(secciones,event_filter.seccion{i}))]; 
        end
        if ~isempty(idx)
            idx=idx+1;
            set(handles.seccion,'value',idx);
        end
    end
end

% Choose default command line output for filter_listbox
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
display_state('Esperando accion del usuario');
% UIWAIT makes filter_listbox wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = filter_listbox_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in creadores.
function creadores_Callback(hObject, eventdata, handles)
% hObject    handle to creadores (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns creadores contents as cell array
%        contents{get(hObject,'Value')} returns selected item from creadores


% --- Executes during object creation, after setting all properties.
function creadores_CreateFcn(hObject, eventdata, handles)
% hObject    handle to creadores (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in revisores.
function revisores_Callback(hObject, eventdata, handles)
% hObject    handle to revisores (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns revisores contents as cell array
%        contents{get(hObject,'Value')} returns selected item from revisores


% --- Executes during object creation, after setting all properties.
function revisores_CreateFcn(hObject, eventdata, handles)
% hObject    handle to revisores (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox22.
function checkbox22_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox22


% --- Executes on button press in checkbox23.
function checkbox23_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox23


% --- Executes on button press in checkbox24.
function checkbox24_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox24


% --- Executes on button press in lp.
function lp_Callback(hObject, eventdata, handles)
% hObject    handle to lp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of lp


% --- Executes on button press in vt.
function vt_Callback(hObject, eventdata, handles)
% hObject    handle to vt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of vt


% --- Executes on button press in tr.
function tr_Callback(hObject, eventdata, handles)
% hObject    handle to tr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tr


% --- Executes on button press in tc.
function tc_Callback(hObject, eventdata, handles)
% hObject    handle to tc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tc


% --- Executes on button press in ot.
function ot_Callback(hObject, eventdata, handles)
% hObject    handle to ot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ot


% --- Executes on button press in aceptar.
function aceptar_Callback(hObject, eventdata, handles)
% hObject    handle to aceptar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Revisa los tipos de evento
h_tipos = get(handles.tipos,'children');
filter.tipo=[];
str=get(h_tipos,'string');
val = get(h_tipos,'value');
filter.tipo = str(cell2mat(val)==1);

%Revisa creadores
values = get(handles.creadores,'value');
str = get(handles.creadores,'string');
filter.creador = str(values(values~=1));

%Revisa revisores
values = get(handles.revisores,'value');
str = get(handles.revisores,'string');
filter.revisor = str(values(values~=1));

%Obtiene la opcion seleccionada
ch = get(handles.options,'children');
values = get(ch,'value');
str = get(ch,'string');
filter.opcion = str(cell2mat(values)==1);

%Obtiene variable
value = get(handles.variable,'value');
str = get(handles.variable,'string');
filter.variable = str(value(value~=1));

%Obtiene orden
value = get(handles.orden,'value');
str = get(handles.orden,'string');
filter.orden = str(value(value~=1));

%Obtiene seccion
value = get(handles.seccion,'value');
str = get(handles.seccion,'string');
filter.seccion = str(value(value~=1));

%Filtra los eventos en la listbox
filter_listbox_events(filter);

close(handles.figure1);


% --- Executes on button press in cancelar.
function cancelar_Callback(hObject, eventdata, handles)
% hObject    handle to cancelar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);


% --- Executes on selection change in listbox3.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3


% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox4.
function listbox4_Callback(hObject, eventdata, handles)
% hObject    handle to listbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox4


% --- Executes during object creation, after setting all properties.
function listbox4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox37.
function checkbox37_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox37


% --- Executes on button press in checkbox38.
function checkbox38_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox38


% --- Executes on button press in checkbox39.
function checkbox39_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox39


% --- Executes on button press in checkbox40.
function checkbox40_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox40


% --- Executes on button press in checkbox41.
function checkbox41_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox41


% --- Executes on button press in checkbox42.
function checkbox42_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox42


% --- Executes on selection change in seccion.
function seccion_Callback(hObject, eventdata, handles)
% hObject    handle to seccion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns seccion contents as cell array
%        contents{get(hObject,'Value')} returns selected item from seccion


% --- Executes during object creation, after setting all properties.
function seccion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seccion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in variable.
function variable_Callback(hObject, eventdata, handles)
% hObject    handle to variable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns variable contents as cell array
%        contents{get(hObject,'Value')} returns selected item from variable


% --- Executes during object creation, after setting all properties.
function variable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to variable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in orden.
function orden_Callback(hObject, eventdata, handles)
% hObject    handle to orden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns orden contents as cell array
%        contents{get(hObject,'Value')} returns selected item from orden


% --- Executes during object creation, after setting all properties.
function orden_CreateFcn(hObject, eventdata, handles)
% hObject    handle to orden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over cancelar.
function cancelar_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to cancelar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
a=1;
