function varargout = search_for_events_form(varargin)
% SEARCH_FOR_EVENTS_FORM MATLAB code for search_for_events_form.fig
%      SEARCH_FOR_EVENTS_FORM, by itself, creates a new SEARCH_FOR_EVENTS_FORM or raises the existing
%      singleton*.
%
%      H = SEARCH_FOR_EVENTS_FORM returns the handle to a new SEARCH_FOR_EVENTS_FORM or the handle to
%      the existing singleton*.
%
%      SEARCH_FOR_EVENTS_FORM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEARCH_FOR_EVENTS_FORM.M with the given input arguments.
%
%      SEARCH_FOR_EVENTS_FORM('Property','Value',...) creates a new SEARCH_FOR_EVENTS_FORM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before search_for_events_form_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to search_for_events_form_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help search_for_events_form

% Last Modified by GUIDE v2.5 07-Nov-2015 20:10:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @search_for_events_form_OpeningFcn, ...
                   'gui_OutputFcn',  @search_for_events_form_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before search_for_events_form is made visible.
function search_for_events_form_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to search_for_events_form (see VARARGIN)

calendar_button = image_button(hObject,[283 196 38 38],@()uicalendar('destinationui',...
    handles.fecha,'selectiontype',1,'windowstyle','modal','outputdateformat','yyyy-mm-dd'),'calendar.png');

% Choose default command line output for search_for_events_form
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% display_state('Esperando accion del usuario');
% UIWAIT makes search_for_events_form wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = search_for_events_form_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in estacion.
function estacion_Callback(hObject, eventdata, handles)
% hObject    handle to estacion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns estacion contents as cell array
%        contents{get(hObject,'Value')} returns selected item from estacion


% --- Executes during object creation, after setting all properties.
function estacion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to estacion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function dias_Callback(hObject, eventdata, handles)
% hObject    handle to dias (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dias as text
%        str2double(get(hObject,'String')) returns contents of dias as a double


% --- Executes during object creation, after setting all properties.
function dias_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dias (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function hora_inicio_Callback(hObject, eventdata, handles)
% hObject    handle to hora_inicio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of hora_inicio as text
%        str2double(get(hObject,'String')) returns contents of hora_inicio as a double


% --- Executes during object creation, after setting all properties.
function hora_inicio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to hora_inicio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in aceptar.
function aceptar_Callback(hObject, eventdata, handles)
% hObject    handle to aceptar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global event_request;
% sel_object = get(handles.Componente,'selectedobject');
% event_request.componente = get(sel_object,'string');
event_request.fecha = get(handles.fecha,'string');
event_request.dias = get(handles.dias,'string');
lista = get(handles.estacion,'string');
idx = get(handles.estacion,'value');
if idx~=1
event_request.estacion = lista{idx};
else
    event_request.estacion = [];
end
delete(handles.figure1);

% --- Executes on button press in cancelar.
function cancelar_Callback(hObject, eventdata, handles)
% hObject    handle to cancelar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);


function fecha_Callback(hObject, eventdata, handles)
% hObject    handle to fecha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fecha as text
%        str2double(get(hObject,'String')) returns contents of fecha as a double


% --- Executes during object creation, after setting all properties.
function fecha_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fecha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
