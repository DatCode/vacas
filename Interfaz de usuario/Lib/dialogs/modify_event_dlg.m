function varargout = add_event_dlg(varargin)
% ADD_EVENT_DLG MATLAB code for add_event_dlg.fig
%      ADD_EVENT_DLG, by itself, creates a new ADD_EVENT_DLG or raises the existing
%      singleton*.
%
%      H = ADD_EVENT_DLG returns the handle to a new ADD_EVENT_DLG or the handle to
%      the existing singleton*.
%
%      ADD_EVENT_DLG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADD_EVENT_DLG.M with the given input arguments.
%
%      ADD_EVENT_DLG('Property','Value',...) creates a new ADD_EVENT_DLG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before add_event_dlg_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to add_event_dlg_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help add_event_dlg

% Last Modified by GUIDE v2.5 05-Dec-2015 16:39:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @add_event_dlg_OpeningFcn, ...
    'gui_OutputFcn',  @add_event_dlg_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before add_event_dlg is made visible.
function add_event_dlg_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to add_event_dlg (see VARARGIN)
global h;
if isfield(h,'selected_event');
    if iscell(h.selected_event)
%         tiempo = get(h.selected_signal,'xdata');
%         t1 = tiempo(1);
%         t2 = tiempo(end);
%         userdata = get(h.selected_event,'userdata');
        set(handles.figure1,'userdata',h.selected_event);
        event = h.selected_event{1}.event_struct;
        t1 = event.inicio{1};
        t2 = event.fin{1};
        
        set(handles.tiempo_inicio,'string',datestr(t1,'HH:MM:SS'),'userdata',t1);
        set(handles.tiempo_fin,'string',datestr(t2,'HH:MM:SS'),'userdata',t2);
        
        p = get_signal_plot(h.selected_axes,'filtered_signal');
        tiempos = get(p,'xdata');
%         val = get(hObject,'value');
        tf = get(handles.tiempo_fin,'userdata');
        ti = get(handles.tiempo_inicio,'userdata');
        min = ti;
        max = tiempos(end);
        new_val = (tf-min)/(max-min);
        set(handles.fin_slider,'value',new_val);
        
        min = tiempos(1);
        max = tf;
        new_val = (ti-min)/(max-min);
        set(handles.inicio_slider,'value',new_val);
    else
        delete(handles.figure1);
        return;
    end
else
    set(handles.tiempo_inicio,'string','00:00:00');
    set(handles.tiempo_fin,'string','00:00:00');
    delete(handles.figure1);
    return;
end
% Choose default command line output for add_event_dlg
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);
display_state('Esperando accion del usuario...');
% UIWAIT makes add_event_dlg wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = add_event_dlg_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if ~isempty(handles)
    varargout{1} = handles.output;
end


% --- Executes on button press in crear.
function crear_Callback(hObject, eventdata, handles)
% hObject    handle to crear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h;
if isfield(h,'selected_axes')
%     userdata = get(h.selected_axes,'userdata');
    e_handle = get(handles.figure1,'userdata');
% set(e_handle,'xdata',[ti tf tf ti]);
% e_handle{1}.set_start_time(ti);
% e_handle{1}.set_end_time(tf);
%     e_userdata = get(h_etiqueta,'userdata');
    event = e_handle{1}.event_struct;
    id_evento = event.id_evento{1};
%     estacion = userdata.estacion;
    %     t1 = datenum(get(handles.tiempo_inicio,'string'),'HH:MM:SS');
    %     t2 = datenum(get(handles.tiempo_fin,'string'),'HH:MM:SS');
    t1=get(handles.tiempo_inicio,'userdata');
    t2=get(handles.tiempo_fin,'userdata');
    if t1~=t2
        tipos = get(handles.tipo,'string');
        idx = get(handles.tipo,'value');
        tipo = tipos(idx,:);
        comentario = get(handles.comentario,'string');
        display_state('Modificando etiqueta...');
        g_modify_event(id_evento,t1,t2,tipo,comentario);
        delete(handles.figure1);
%         clear_selection()
        refresh_events_in_graph();
    end
    display_state('Listo...');
end

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
refresh_events_in_graph();
delete(handles.figure1);

% --- Executes on slider movement.
function inicio_slider_Callback(hObject, eventdata, handles)
% hObject    handle to inicio_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h;
% global sel_color;
p = get_signal_plot(h.selected_axes,'filtered_signal');
tiempos = get(p,'xdata');
val = get(hObject,'value');
tf = get(handles.tiempo_fin,'userdata');
ti = get(handles.tiempo_inicio,'userdata');
min = tiempos(1);
max = tf;

new_t=min+val*(max-min);
ti=new_t;
if tf<ti
    tf=ti;
    ti=new_t;
end

set(handles.tiempo_inicio,'string',datestr(ti,'HH:MM:SS'),'userdata',ti);

e_handle = get(handles.figure1,'userdata');
% set(e_handle,'xdata',[ti tf tf ti]);
e_handle{1}.set_start_time(ti);
e_handle{1}.set_end_time(tf);
% p = get_signal_plot(h.selected_axes,'filtered_signal');
% brand_selected_signal(p,[ti tf],sel_color);

min = ti;
max = tiempos(end);
new_val_ini = (tf-min)/(max-min);
set(handles.fin_slider,'value',new_val_ini);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function inicio_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inicio_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function fin_slider_Callback(hObject, eventdata, handles)
% hObject    handle to fin_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h;global sel_color;
p = get_signal_plot(h.selected_axes,'filtered_signal');
tiempos = get(p,'xdata');
val = get(hObject,'value');
tf = get(handles.tiempo_fin,'userdata');
ti = get(handles.tiempo_inicio,'userdata');
min = ti;
max = tiempos(end);

new_tf=min+val*(max-min);
tf=new_tf;
if tf<ti
    tf=ti;
    ti=new_tf;
end

min = tiempos(1);
max = tf;
new_val_ini = (ti-min)/(max-min);

set(handles.tiempo_fin,'string',datestr(tf,'HH:MM:SS'),'userdata',tf);
set(handles.inicio_slider,'value',new_val_ini);
% p = get_signal_plot(h.selected_axes,'filtered_signal');
% brand_selected_signal(p,[ti tf],sel_color);
e_handle = get(handles.figure1,'userdata');
% set(e_handle,'xdata',[ti tf tf ti]);
e_handle{1}.set_start_time(ti);
e_handle{1}.set_end_time(tf);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function fin_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fin_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in tipo.
function tipo_Callback(hObject, eventdata, handles)
% hObject    handle to tipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns tipo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from tipo


% --- Executes during object creation, after setting all properties.
function tipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function comentario_Callback(hObject, eventdata, handles)
% hObject    handle to comentario (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of comentario as text
%        str2double(get(hObject,'String')) returns contents of comentario as a double


% --- Executes during object creation, after setting all properties.
function comentario_CreateFcn(hObject, eventdata, handles)
% hObject    handle to comentario (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function text3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
