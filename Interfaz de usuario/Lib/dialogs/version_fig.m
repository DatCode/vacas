function varargout = version_fig(varargin)
% VERSION MATLAB code for version.fig
%      VERSION, by itself, creates a new VERSION or raises the existing
%      singleton*.
%
%      H = VERSION returns the handle to a new VERSION or the handle to
%      the existing singleton*.
%
%      VERSION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VERSION.M with the given input arguments.
%
%      VERSION('Property','Value',...) creates a new VERSION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before version_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to version_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help version

% Last Modified by GUIDE v2.5 21-Jan-2016 18:42:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @version_OpeningFcn, ...
                   'gui_OutputFcn',  @version_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before version is made visible.
function version_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to version (see VARARGIN)

% Choose default command line output for version
handles.output = hObject;

version = '1.0';
fecha='2016_02_10';
str = sprintf('%s%s\n%s','V',version,[fecha '.']);
set(handles.texto,'string',str);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes version wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = version_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in aceptar.
function aceptar_Callback(varargin)
% hObject    handle to aceptar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(gcf);
