function remember_figure_properties( filename,handles )
if exist(filename)==2
   matstruct=load(filename);
   fieldsmat = fieldnames(matstruct);
   fields = fieldnames(handles);
%    if sum(strcmpi(fields,'fini_field'))==1 && sum(strcmpi(fields,'ffin_field'))==1
   truefields = intersect(fieldsmat,fields);
   for j=1:length(truefields)
      fieldsi=fieldnames(matstruct.(truefields{j}));     
      for i=1:length(fieldsi)
         set(handles.(truefields{j}),fieldsi{i},matstruct.(truefields{j}).(fieldsi{i}));          
      end
%    end
   end
end
end

