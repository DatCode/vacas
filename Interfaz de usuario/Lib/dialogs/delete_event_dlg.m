function delete_event_dlg(varargin)
global h;
% [selectedButton dlgshown] = uigetpref(...
%     'eliminado',...                        % Group
%     'savefigurebeforeclosing',...           % Preference
%     'Eliminacion de etiqueta',...                    % Window title
%     {'Esta seguro de que desea eliminar para siempre la etiqueta?'
%      ''
%      'Luego de la operacion, usted no sera capaz de recuperar la etiqueta'},...
%     {'borrar';'Estoy seguro'},...        % Values and button strings
%      'ExtraOptions','Cancel',...             % Additional button
%      'DefaultButton','Cancel');           % Default choice
choice = questdlg('Esta seguro de que desea eliminar para siempre la etiqueta?', ...
	'Luego de la operacion, usted no sera capaz de recuperar la etiqueta', ...
	'Borrar','Cancelar','Cancelar');
switch choice
   case 'Borrar'  % Open a Save dialog (without testing if saved before)
       userdata=get(h.selected_event,'userdata');
       evento = userdata.event;
       id_evento = evento.id_evento;
       display_state('Intentando borrar la etiqueta...');
       s=g_delete_event(cell2mat(id_evento));
       if s==1
           display_state('Etiqueta eliminada exitosamente!');
       else
           display_state('Fallo el intento por eliminar la etiqueta');
       end
       pause(0.5);
       refresh_events_in_graph();
   case 'Cancelar'               % Do not close the figure
       return
 end