function varargout = search_events(varargin)
% SEARCH_EVENTS MATLAB code for search_events.fig
%      SEARCH_EVENTS, by itself, creates a new SEARCH_EVENTS or raises the existing
%      singleton*.
%
%      H = SEARCH_EVENTS returns the handle to a new SEARCH_EVENTS or the handle to
%      the existing singleton*.
%
%      SEARCH_EVENTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEARCH_EVENTS.M with the given input arguments.
%
%      SEARCH_EVENTS('Property','Value',...) creates a new SEARCH_EVENTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before search_events_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to search_events_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help search_events

% Last Modified by GUIDE v2.5 05-Aug-2016 12:45:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @search_events_OpeningFcn, ...
                   'gui_OutputFcn',  @search_events_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before search_events is made visible.
function search_events_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to search_events (see VARARGIN)

% Choose default command line output for search_events
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

calendar_button = image_button(hObject,[293 298 32 32],@()uicalendar('destinationui',...
    handles.date_field,'selectiontype',1,'windowstyle','modal','outputdateformat','yyyy-mm-dd'),'calendar.png');
set(get(calendar_button,'parent'),'position',[154 18 37 37])
set(get(calendar_button,'parent'),'parent',handles.date_panel);
uistack(get(calendar_button,'parent'),'top')

%Recuerda valores previos
filename = 'search_events.mat';
remember_figure_properties(filename,handles);


% --- Outputs from this function are returned to the command line.
function varargout = search_events_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function cancel_btn_Callback(hObject, eventdata, handles)
delete(handles.figure1);

function search_btn_Callback(hObject, eventdata, handles)
% global h;
%Obtiene inputs
date_field.string = get(handles.date_field,'string');
duration_field.string = get(handles.duration_field,'string');

%Almacena inputs para luego ser recordados
save('search_events.mat','date_field','duration_field');

%Obtiene etiquetas
fini = datenum(date_field.string,'yyyy-mm-dd');
ffin = datenum(0,0,str2double(duration_field.string))+fini;
etiquetas = get_etiquetas(fini,ffin);

%Actualiza listbox con etiquetas
force_listbox(etiquetas);

%Cierra la ventana
delete(handles.figure1);

function duration_field_Callback(hObject, eventdata, handles)
% hObject    handle to duration_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of duration_field as text
%        str2double(get(hObject,'String')) returns contents of duration_field as a double


% --- Executes during object creation, after setting all properties.
function duration_field_CreateFcn(hObject, eventdata, handles)
% hObject    handle to duration_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function duration_unit_label_CreateFcn(hObject, eventdata, handles)
% hObject    handle to duration_unit_label (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function date_field_Callback(hObject, eventdata, handles)
% hObject    handle to date_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of date_field as text
%        str2double(get(hObject,'String')) returns contents of date_field as a double


% --- Executes during object creation, after setting all properties.
function date_field_CreateFcn(hObject, eventdata, handles)
% hObject    handle to date_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
