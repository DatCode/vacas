%CopyRight (c), 2015 Jose Martinez Lavados
function handle=myGUI(varargin)
%% Maneja entrada
if nargin<3
    user = '';
    pass = '';
    grupo = 1;
else
    user = char(varargin{1});
    pass = char(varargin{2});
    grupo = char(varargin{3});
    
    if strcmpi(grupo,'ufro')
        grupo =1;
    else
        grupo = 2;
        
    end
end

%% Crea la figura
sz = [300 370]; % figure size
screensize = get(0,'ScreenSize');
xpos = ceil((screensize(3)-sz(2))/2); % center the figure on the
ypos = ceil((screensize(4)-sz(1))/2); % center the figure on the
h.f = figure('units','pixels','position',[xpos, ypos, sz(1), sz(2)],...
    'toolbar','none','menu','none','name','Ingreso usuario','color',[0.4 0.35 0.25],...
    'closerequestfcn',@cierra,'numbertitle','off');
handle = h.f;
%% Ingreso de usuario
%Crea los campo de usuario
h.t(1) = uicontrol('style','edit','units','pixels',...
    'position',[10, 300, 280, 30],'string',user);
%Crea label usuario
h.o(1) = uicontrol('style','text','units','pixels',...
    'position',[10, 330, 280,20],'string','Usuario','backgroundcolor',get(gcf,'color'),...
    'foregroundcolor',[1 1 1],'fontsize',12,'horizontalalignment','center');

%% Ingreso de password
%Crea campo de pass
h.t(2) = uicontrol('style','edit','units','pixels',...
    'position',[10, 230, 280, 30],'string',pass);
h.o(3) = uicontrol('style','text','units','pixels',...
    'position',[10, 260, 280,20],'string','Contraseņa','backgroundcolor',get(gcf,'color'),...
    'foregroundcolor',[1 1 1],'fontsize',12,'horizontalalignment','center');

%% Ingreso de grupo
%Crea label
h.o(2) = uicontrol('style','text','units','pixels',...
    'position',[10, 190, 280,20],'string','Grupo','backgroundcolor',get(gcf,'color'),...
    'foregroundcolor',[1 1 1],'fontsize',12,'horizontalalignment','center');
%Crea menu popup
h.pop(1) = uicontrol('style','popup',...
    'String',{'ufro', 'ovdas'},...
    'position',[10 150 280 40],'horizontalalignment','center','value',grupo);

% Create yes/no checkboxes
h.c(1) = uicontrol('style','checkbox','units','pixels',...
    'position',[10,140,280,15],'string','Recuerdame','backgroundcolor',get(gcf,'color'),...
    'foregroundcolor',[1 1 1]);
h.c(2) = uicontrol('style','checkbox','units','pixels',...
    'position',[10,120,280,15],'string','Ingresar siempre con estos datos',...
    'backgroundcolor',get(gcf,'color'),'foregroundcolor',[1 1 1]);

%% Creacion del boton de ingreso
h.p(2) = uicontrol('style','pushbutton','units','pixels',...
    'position',[155,50,140,50],'string','Ingresar',...
    'callback',@buton_callback,'backgroundcolor',[0.2 0.7 0.3],...
    'foregroundcolor',[1 1 1],'fontweight','bold','fontsize',12);

%% Creacion del boton de checkeo de conexion
h.p(1) = uicontrol('style','pushbutton','units','pixels',...
    'position',[10,50,145,50],'string','Checkear',...
    'callback',@buton_check_callback,'backgroundcolor',[0.2 0.3 0.7],...
    'foregroundcolor',[1 1 1],'fontweight','bold','fontsize',12);

%Autofocus a user
uicontrol(h.t(1));
% Pushbutton callback
    function buton_callback(varargin)
        refresh_data();
        delete(h.f);
    end

    function buton_check_callback(varargin)
        global datos_user;
        
        refresh_data();
        dbSource = 'cesar';
        server_connection(datos_user.user,datos_user.pass,h.f);
        
    end

    function refresh_data()
        %variables globales
        global datos_user;
        
        %resultados del ingreso
        editable_text_values = get(h.t,'string');
        checkbox_values = cell2mat(get(h.c,'Value'));
        popmenu_string = get(h.pop(1),'string');
        popmenu_values = get(h.pop(1),'value');
        selected_popmenu = popmenu_string(popmenu_values(1));
        
        %Guarda datos en la variable global
        user2 = char(editable_text_values(1));
        pass2 = char(editable_text_values(2));
        datos_user = struct('user',user2,'pass',pass2,'grupo',selected_popmenu,...
            'recordar',checkbox_values(1),'autoiniciar',checkbox_values(2),'cierra',0);
    end

    function cierra(varargin)
        global datos_user;
        datos_user.cierra = 1;
        delete (h.f);
    end
end