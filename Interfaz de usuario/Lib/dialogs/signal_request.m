function varargout = signal_request(varargin)
% SIGNAL_REQUEST MATLAB code for signal_request.fig
%      SIGNAL_REQUEST, by itself, creates a new SIGNAL_REQUEST or raises the existing
%      singleton*.
%
%      H = SIGNAL_REQUEST returns the handle to a new SIGNAL_REQUEST or the handle to
%      the existing singleton*.
%
%      SIGNAL_REQUEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIGNAL_REQUEST.M with the given input arguments.
%
%      SIGNAL_REQUEST('Property','Value',...) creates a new SIGNAL_REQUEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before signal_request_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to signal_request_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help signal_request

% Last Modified by GUIDE v2.5 06-Mar-2016 18:04:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @signal_request_OpeningFcn, ...
                   'gui_OutputFcn',  @signal_request_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before signal_request is made visible.
function signal_request_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to signal_request (see VARARGIN)

% Choose default command line output for signal_request
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes signal_request wait for user response (see UIRESUME)
% uiwait(handles.figure1);
calendar_button = image_button(hObject,[293 298 32 32],@()uicalendar('destinationui',...
    handles.inicio,'selectiontype',1,'windowstyle','modal','outputdateformat','yyyy-mm-dd'),'calendar.png');
set(get(calendar_button,'parent'),'position',[270 100 37 37])
set(get(calendar_button,'parent'),'parent',handles.uipanel3);
uistack(get(calendar_button,'parent'),'top')

%Recuerda valores previos
filename = 'signal_request.mat';
remember_figure_properties(filename,handles);

% --- Outputs from this function are returned to the command line.
function varargout = signal_request_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in cancelar.
function cancelar_Callback(hObject, eventdata, handles)
% hObject    handle to cancelar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);

% --- Executes on button press in cargar.
function cargar_Callback(hObject, eventdata, handles)
%Obtiene input
sensor = get_val_list(handles.sensor);
variable = get_val_list(handles.variable);
inicio = get(handles.inicio,'string');
hora = get(handles.hora,'string');
duracion = get(handles.duracion,'string');
unidad = get_val_list(handles.unidad_tiempo);
fuente = get_val_list(handles.fuente);
fuente_val = get(handles.fuente_val,'string');

%Genera request
% request.predial = '1113';
% 
request.coly = variable;
request.colx = {'it'};
request.sensor = sensor;
request.tabla = get_sensor_table(sensor);
request.db = 'vacas_db';
t1 = datenum([inicio ' ' hora],'yyyy-mm-dd HH:MM');

switch(unidad)
    case 'Segundos'
      factor = datenum(0,0,0,0,0,1);  
    case 'Minutos'
        factor = datenum(0,0,0,0,1,0);
    case 'Horas'
        factor = datenum(0,0,0,1,0,0);
    case 'Dias'
        factor = 1;
end

t2 = t1+str2num(duracion)*factor;

request.range_x = [t1 t2];
request.fuente = fuente;
request.fuente_val = fuente_val;
request.sensor = sensor;
request.sag = getSagFromRequest(request);
request.time_format = 'datenum';

%Agrega grafico
sql_plot(request);

%Guarda propiedades figura para recordar
warning('off');
fuente_val.string = get(handles.fuente_val,'string');
fuente.string = get(handles.fuente,'string');
fuente.val = get(handles.fuente,'val');
hora.string=get(handles.hora,'string');
unidad_tiempo.string = get(handles.unidad_tiempo,'string');
unidad_tiempo.value = get(handles.unidad_tiempo,'value');
duracion.string = get(handles.duracion,'string');
inicio.string = get(handles.inicio,'string');
variable.string = get(handles.variable,'string');
variable.value = get(handles.variable,'value');
sensor.string = get(handles.sensor,'string');
warning('on');
sensor.value = get(handles.sensor,'value');
save('signal_request.mat','fuente_val','fuente','hora',...
    'unidad_tiempo','duracion','inicio','variable','sensor');
delete(handles.figure1);

function value = get_val_list(handle)
str = get(handle,'string');
val = get(handle,'value');
value = str{val};

function inicio_Callback(hObject, eventdata, handles)
% hObject    handle to inicio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inicio as text
%        str2double(get(hObject,'String')) returns contents of inicio as a double
str = get(hObject,'string');
if strcmpi(str,'yyyy-mm-dd')
    set(hObject,'string','');
end

% --- Executes during object creation, after setting all properties.
function inicio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inicio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function duracion_Callback(hObject, eventdata, handles)
% hObject    handle to duracion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of duracion as text
%        str2double(get(hObject,'String')) returns contents of duracion as a double


% --- Executes during object creation, after setting all properties.
function duracion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to duracion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in unidad_tiempo.
function unidad_tiempo_Callback(hObject, eventdata, handles)
% hObject    handle to unidad_tiempo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns unidad_tiempo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from unidad_tiempo


% --- Executes during object creation, after setting all properties.
function unidad_tiempo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to unidad_tiempo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in sensor.
function sensor_Callback(hObject, eventdata, handles)
%Cambia las posibles variables a mostar
str = get(hObject,'string');
val = get(hObject,'value');
sensor = str{val};
variables = get_sensor_vars(sensor);
set(handles.variable,'value',1,'string',variables);

% --- Executes during object creation, after setting all properties.
function sensor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sensor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in variable.
function variable_Callback(hObject, eventdata, handles)
% hObject    handle to variable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns variable contents as cell array
%        contents{get(hObject,'Value')} returns selected item from variable


% --- Executes during object creation, after setting all properties.
function variable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to variable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function hora_Callback(hObject, eventdata, handles)
% hObject    handle to hora (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of hora as text
%        str2double(get(hObject,'String')) returns contents of hora as a double
str = get(hObject,'string');
if strcmpi(str,'HH:MM')
    set(hObject,'string','');
end

% --- Executes during object creation, after setting all properties.
function hora_CreateFcn(hObject, eventdata, handles)
% hObject    handle to hora (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in fuente.
function fuente_Callback(hObject, eventdata, handles)
% hObject    handle to fuente (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns fuente contents as cell array
%        contents{get(hObject,'Value')} returns selected item from fuente


% --- Executes during object creation, after setting all properties.
function fuente_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fuente (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fuente_val_Callback(hObject, eventdata, handles)
% hObject    handle to fuente_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fuente_val as text
%        str2double(get(hObject,'String')) returns contents of fuente_val as a double


% --- Executes during object creation, after setting all properties.
function fuente_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fuente_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
