function varargout = delete_event_dlg2(varargin)
% DELETE_EVENT_DLG2 MATLAB code for delete_event_dlg2.fig
%      DELETE_EVENT_DLG2, by itself, creates a new DELETE_EVENT_DLG2 or raises the existing
%      singleton*.
%
%      H = DELETE_EVENT_DLG2 returns the handle to a new DELETE_EVENT_DLG2 or the handle to
%      the existing singleton*.
%
%      DELETE_EVENT_DLG2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DELETE_EVENT_DLG2.M with the given input arguments.
%
%      DELETE_EVENT_DLG2('Property','Value',...) creates a new DELETE_EVENT_DLG2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before delete_event_dlg2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to delete_event_dlg2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help delete_event_dlg2

% Last Modified by GUIDE v2.5 03-Dec-2015 16:05:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @delete_event_dlg2_OpeningFcn, ...
    'gui_OutputFcn',  @delete_event_dlg2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before delete_event_dlg2 is made visible.
function delete_event_dlg2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to delete_event_dlg2 (see VARARGIN)
display_state('Esperando accion del usuario...');
% Choose default command line output for delete_event_dlg2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes delete_event_dlg2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = delete_event_dlg2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in borrar.
function borrar_Callback(hObject, eventdata, handles)
global h;
% hObject    handle to borrar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% userdata=get(h.selected_event,'userdata');
% evento = userdata.event;
evento = h.selected_event{1}.event_struct;
id_evento = evento.id_evento;
display_state('Intentando borrar la etiqueta...');
s=g_delete_event(cell2mat(id_evento));
if s==1
    display_state('Etiqueta eliminada exitosamente!');
else
    display_state('Fallo el intento por eliminar la etiqueta');
end
refresh_events_in_graph();
delete(handles.figure1);

% --- Executes on button press in cancelar.
function cancelar_Callback(hObject, eventdata, handles)
% hObject    handle to cancelar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over borrar.
function borrar_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to borrar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over cancelar.
function cancelar_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to cancelar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
