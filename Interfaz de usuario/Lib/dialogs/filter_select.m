function varargout = filter_select(varargin)
% FILTER_SELECT MATLAB code for filter_select.fig
%      FILTER_SELECT, by itself, creates a new FILTER_SELECT or raises the existing
%      singleton*.
%
%      H = FILTER_SELECT returns the handle to a new FILTER_SELECT or the handle to
%      the existing singleton*.
%
%      FILTER_SELECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FILTER_SELECT.M with the given input arguments.
%
%      FILTER_SELECT('Property','Value',...) creates a new FILTER_SELECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before filter_select_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to filter_select_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help filter_select

% Last Modified by GUIDE v2.5 20-Dec-2015 01:15:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @filter_select_OpeningFcn, ...
                   'gui_OutputFcn',  @filter_select_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before filter_select is made visible.
function filter_select_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to filter_select (see VARARGIN)

% Choose default command line output for filter_select
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

files = dir([pwd '\lib\filters\*.mat']);
filenames ={files(~[files.isdir]).name};
filenames=cellfun(@(x) x(1:end-4),filenames,'UniformOutput',false);
set(handles.filtros,'string',filenames);

% UIWAIT makes filter_select wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = filter_select_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in filtros.
function filtros_Callback(hObject, eventdata, handles)
% hObject    handle to filtros (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns filtros contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filtros


% --- Executes during object creation, after setting all properties.
function filtros_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filtros (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in aceptar.
function aceptar_Callback(hObject, eventdata, handles)
% hObject    handle to aceptar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
values = get(handles.filtros,'value');
string = get(handles.filtros,'string');
global selected_filter;
selected_filter = string{values};
delete(handles.figure1);