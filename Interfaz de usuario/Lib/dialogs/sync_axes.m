function varargout = sync_axes(varargin)
% SYNC_AXES MATLAB code for sync_axes.fig
%      SYNC_AXES, by itself, creates a new SYNC_AXES or raises the existing
%      singleton*.
%
%      H = SYNC_AXES returns the handle to a new SYNC_AXES or the handle to
%      the existing singleton*.
%
%      SYNC_AXES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SYNC_AXES.M with the given input arguments.
%
%      SYNC_AXES('Property','Value',...) creates a new SYNC_AXES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sync_axes_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sync_axes_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sync_axes

% Last Modified by GUIDE v2.5 10-Feb-2016 18:12:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sync_axes_OpeningFcn, ...
                   'gui_OutputFcn',  @sync_axes_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sync_axes is made visible.
function sync_axes_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sync_axes (see VARARGIN)

% Choose default command line output for sync_axes
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sync_axes wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = sync_axes_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in aceptar.
function aceptar_Callback(hObject, eventdata, handles)
% hObject    handle to aceptar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h;
global colores;
ch=get(h.axes_panel,'children');
set(ch,'xcolor','black','ycolor','black');
set(get(h.button_panel,'children'),'xcolor','black','ycolor','black')
close(handles.figure1);
%Hacer que los demas axes se adapten a h.selected_axes
mirror_ax(h.selected_axes);
%Perder rastro de h.selected_axes
h.selected_axes = 0;


% --- Executes on button press in cancelar.
function cancelar_Callback(hObject, eventdata, handles)
% hObject    handle to cancelar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h;
set(get(h.axes_panel,'children'),'xcolor','black','ycolor','black');
%Perder rastro de h.selected_axes
h.selected_axes= 0;
close(handles.figure1);