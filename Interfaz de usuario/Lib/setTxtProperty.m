function setTxtProperty(file,property,value)
[current_value,idx] = getTxtProperty(file,property);
if ~isempty(current_value)
   %Existe la propiedad. Se debe reemplazar el valor
   
   %Lee las propiedades y las actualiza
   fid = fopen(file,'r');
   texto = textscan(fid,'%s %s');
   [nrows,~] = size(texto{1});
   texto{2}{idx}=value;
   fclose(fid);
   
   %Vuelve a escribirlas en el archivo
   fid = fopen(file,'w');
   for i = 1:nrows
       fprintf(fid,'%s %s\n',texto{1}{i},texto{2}{i});
   end
   fclose(fid);
else
   %No existe la propiedad. Se debe crear.
   fid = fopen(file,'a');
   fprintf(fid,'%s: %s\n',property,value);
   fclose(fid);
end
end