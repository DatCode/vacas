function normal_zoom(varargin)
global h;
if valid_field(h,'selected_axes',0)
    ud = get(h.selected_axes,'userdata');
    if ~strcmpi(ud.display,'spectre') && ~strcmpi(ud.display,'spectrogram')
        p = get_signal_plot(h.selected_axes,'filtered_signal');
        
        zp = get_signal_plot(h.selected_axes,'event_zoom');
        try
            delete(zp);
        catch
        end
        show_in_graph({'filtered_signal'});
        remember_limits(p);
        % xdata = get(p,'xdata');
        % xlim(h.selected_axes,[xdata(1) xdata(end)]);
        set(get_signal_plot(h.selected_axes,'selected_signal'),'visible','on');
        
        ud.zoom = 'off';
        set(h.selected_axes,'userdata',ud);
        refresh_filtername_text
        refresh_marks;
        repaintEventsInGraph(h.selected_axes);
    end
end
end

