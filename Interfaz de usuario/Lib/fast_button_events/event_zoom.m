function event_zoom( varargin )
global h;
if valid_field(h,'selected_axes',0)
    s = get_spectrogram();
    ud = get(h.selected_axes,'userdata');
    if valid_field(h,'selected_event',0) && isempty(s) && ~strcmpi(ud.display,'spectre')
%         ud = get(h.selected_event,'userdata');
        ev = h.selected_event{1};
        event =ev.event_struct;
        try
          delete(get_signal_plot(h.selected_axes,'event_zoom'));
        catch
        end
        display_event_in_graph(event);
        refresh_filtername_text(h.selected_axes);
        set(get_signal_plot(h.selected_axes,'selected_signal'),'visible','on')
        refresh_marks;
        
        ud = get(h.selected_axes,'userdata');
        ud.zoom='on';
        set(h.selected_axes,'userdata',ud);
        
        repaintEventsInGraph(h.selected_axes);
    end
end
end

