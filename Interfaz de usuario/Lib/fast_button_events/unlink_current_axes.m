function unlink_current_axes()
global h;
if valid_field(h,'selected_axes',0)
   ud = get(h.selected_axes,'userdata');
   linked = ud.linked_ax;
   for i = 1:length(linked)
      act_ud = get(linked(i),'userdata');
      act_ud.linked_ax = act_ud.linked_ax(act_ud.linked_ax~=h.selected_axes);
      set(linked,'userdata',act_ud);
   end
   linked = h.selected_axes;
   ud.linked_ax =linked;
   set(h.selected_axes,'userdata',ud);
end
end

