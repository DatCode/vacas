%CopyRight (c), 2015 Jose Martinez Lavados
function [ connection ] = server_connection(varargin)
% server_conection: Funcion creada para manejar etiqueta de verificacion de
% la coneccion con el servidor, para la interf�z grafica.
% Input
%   la funcion no posee argumentos de entrada
%
% Output
% - connection:
global g_handles;

online  = '#0B610B';      % Verde
offline = '#FF0000';      % Rojo
dbSource = 'cesar';

if nargin<3
    connection=try_to_connect(dbSource,'tester','123456',5,3);     % abriendo una nueva conexi�n con el servidor
    
    % detectando si la conexi�n fue abierta.
    if isconnection(connection)
        % conectado al servidor
        %string={'<HTML><FONT color=' online '>ONLINE</Font></html>'};
        labelStr = ['<HTML><FONT color=' online '>&#8226;ONLINE</Font></html>'];
        jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
        set(g_handles.figure1,'units','pixels');
        dim_figure = get(g_handles.figure1,'position');
        dim_label = [dim_figure(3)-100 10 50 20];
        try
            delete(g_handles.server_connection);
        catch
        end
        [componente g_handles.server_connection] = javacomponent(jLabel,dim_label,g_handles.figure1);
        close(connection);
    else
        % conexion interrumpida, no hay conexi�n con el servidor
        %string={'<HTML><FONT color=' offline '>OFFLINE</Font></html>'};
        labelStr = ['<HTML><FONT color=' offline '>&#8226;OFFLINE</Font></html>'];
        jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
        set(g_handles.figure1,'units','pixels');
        dim_figure = get(g_handles.figure1,'position');
        dim_label = [dim_figure(3)-100 10 50 20];
        try
            delete(g_handles.server_connection);
        catch
        end
        [componente g_handles.server_connection] = javacomponent(jLabel,dim_label,g_handles.figure1);
    end
else
    if nargin == 3
        user = char(varargin{1});
        pass = char(varargin{2});
        handle = varargin{3};
        
        labelStr = ['<HTML><FONT color=' online '>Probando</Font></html>'];
        jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
        set(handle,'units','pixels');
        dim_figure = get(handle,'position');
        dim_label = [dim_figure(3)-100 10 50 20];
        try
            delete(g_handles.server_connection);
        catch
        end
        [componente g_handles.server_connection] = javacomponent(jLabel,dim_label,handle);
        set(g_handles.server_connection,'backgroundcolor',get(handle,'color'));
        
        pause(0.01);
        connection = try_to_connect(dbSource,user,pass,5,3);
        % detectando si la conexi�n fue abierta.
        if isconnection(connection)
            % conectado al servidor
            %string={'<HTML><FONT color=' online '>ONLINE</Font></html>'};
            labelStr = ['<HTML><FONT color=' online '>&#8226;ONLINE</Font></html>'];
            jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
            set(handle,'units','pixels');
            dim_figure = get(handle,'position');
            dim_label = [dim_figure(3)-100 10 50 20];
            try
                delete(g_handles.server_connection);
            catch
            end
            [componente g_handles.server_connection] = javacomponent(jLabel,dim_label,handle);
            set(g_handles.server_connection,'backgroundcolor',get(handle,'color'));
            close(connection);
        else
            % conexion interrumpida, no hay conexi�n con el servidor
            %string={'<HTML><FONT color=' offline '>OFFLINE</Font></html>'};
            labelStr = ['<HTML><FONT color=' offline '>&#8226;OFFLINE</Font></html>'];
            jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
            set(handle,'units','pixels');
            dim_figure = get(handle,'position');
            dim_label = [dim_figure(3)-100 10 50 20];
            try
                delete(g_handles.server_connection);
            catch
            end
            [componente g_handles.server_connection] = javacomponent(jLabel,dim_label,handle);
            set(g_handles.server_connection,'backgroundcolor',get(handle,'color'));
        end
    end
end


close(connection);
%set(g_handles.server_connection,'String',string);

end