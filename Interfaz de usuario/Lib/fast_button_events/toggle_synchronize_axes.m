function toggle_synchronize_axes(varargin)
global flag;
global colores;
    global h;
    if valid_field(h,'selected_axes',0) || flag.choosing_sync_axes
        button = h.fast_button(9);
        toggle_fast_button(button);
        if flag.choosing_sync_axes
            flag.choosing_sync_axes = false;
            
            waitfor(sync_axes);
            
%             pause(0.3);
            flag.choosing_axes = true;
            button = h.fast_button(1);
            toggle_fast_button(button);
        else
            flag.selecting_signal=false;
            flag.choosing_axes = false;
            flag.choosing_sync_axes = true;
            set(get(h.axes_panel,'children'),'xcolor','black','ycolor','black');
            set(h.selected_axes,'xcolor',colores.sync_axes,'ycolor',colores.sync_axes);
            if valid_field(h,'selected_axes',0)
                ch = get(h.selected_axes,'children');
                types = get(ch,'type');
                idx = strcmpi(types,'patch');
                if ~isempty(idx)
                    fc=get(ch(idx),'facecolor');
                    if iscell(fc)
                        for i = 1:length(fc)
                            set(ch(idx(i)),'edgecolor',fc{i});
                        end
                    else
                    set(ch(idx),'edgecolor',fc);
                    end
                end
            end
            %         clear_axes();
%             h.selected_axes = 0;
            if valid_field(h,'selected_event',0)
               h.selected_event{1}.unselect(); 
            end
            h.selected_event = 0;
            clear_dynamic_event();
            clear_selection();
            clear_counters();
        end
    end
end