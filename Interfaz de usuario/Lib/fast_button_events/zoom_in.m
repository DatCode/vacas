function zoom_in(varargin)
global h;
% if valid_field(h,'selected_signal',0)
if valid_field(h,'selected_axes',0)
    ud = get(h.selected_axes,'userdata');
    selected_signal = get_signal_plot(h.selected_axes,'selected_signal');
    if ishandle(selected_signal)
        xdata = get(selected_signal,'xdata');
        xlim(h.selected_axes,[xdata(1) xdata(end)]);
        ydata = get(selected_signal,'ydata');
        try
            set(h.selected_axes,'ylim',[min(ydata) max(ydata)]*1.1);
        catch
        end
        try
            delete(selected_signal);
        catch
        end
        
        h.selected_signal = [];
        refresh_date_xtick();
        
        ud.zoom = 'on';
        set(h.selected_axes,'userdata',ud);
        refresh_filtername_text
        %        refresh_events_in_graph
        repaintEventsInGraph(h.selected_axes);
        display_events_in_ax(h.selected_axes,'refresh');
        refresh_marks;
        
        
    end
end
end
% end