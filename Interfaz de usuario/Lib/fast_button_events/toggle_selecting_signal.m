function toggle_selecting_signal()
    global flag;
    global h;
    button = h.fast_button(2);
        toggle_fast_button(button);
    if flag.selecting_signal
        flag.selecting_signal=false;
                ud = get(h.selected_axes,'userdata');
        try
            delete(ud.crosshairline)
        catch
        end
        ud.crosshairline=-1;
        set(h.selected_axes,'userdata',ud);
        clear_selection;
        flag.choosing_axes = true;
            button = h.fast_button(1);
            toggle_fast_button(button);
    else
        if valid_field(h,'selected_axes',0)
            unselectAllEventsInAxes(h.selected_axes);        
        end
        flag.selecting_signal = true;
        flag.choosing_axes = false;
        flag.choosing_sync_axes = false;    
    end
end