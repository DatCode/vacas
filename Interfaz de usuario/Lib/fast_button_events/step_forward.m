function step_forward(ax,varargin)
global h;
if nargin<3
    ax = h.selected_axes;
end
takeAStep(ax,'forward');
end