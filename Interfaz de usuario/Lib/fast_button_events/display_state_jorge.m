%CopyRight (c), 2015 Jose Martinez Lavados
function display_state_jorge(state)
global g_handles;

if nargin<1
   state = 'Listo...'; 
end

labelStr = ['<HTML><FONT>' state '</Font></html>'];
jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
dim_label = [10 5 150 20];
try
delete(g_handles.state_label);
catch
end
g_handles.state = labelStr;
g_handles.state_label = javacomponent(jLabel,dim_label,g_handles.figure1);
pause(0.001);
end