function step_back(varargin)
global h;
if nargin<3
    ax = h.selected_axes;
end

takeAStep(ax,'back');
end