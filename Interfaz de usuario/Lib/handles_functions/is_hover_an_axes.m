function flag=is_hover_an_axes(ax)
global h;
flag = false;
if isfield(h,'ax') && ishover(h.fig,h.axes_panel)
    for i = 1: length(h.ax)
        if nargin==3
            if ishover(h.fig,h.ax(i))
                flag = h.ax(i);
                break;
            end
        else
            if ishover(h.fig,h.ax(i))
                flag = h.ax(i);
                break;
            end
        end
    end
end

if ishandle(flag) && nargin>0
   if flag~=ax 
      flag = false; 
   end
end
end