function p = get_signal_plot(ax,name)
if nargin==0
    ax = gca;
    name = 'filtered_signal';
end
p = [];
dibujos = get_plots(ax);
if ~isempty(dibujos)
    if ~strcmpi(name,'current')
        
        for i=1:length(dibujos)
            dibujo_actual = dibujos(i);
            userdata=get(dibujo_actual,'userdata');
            if valid_field(userdata,'name','')
                if strcmpi(userdata.name,name)
                    p=dibujo_actual;
                    return;
                end
            end
        end
%         p = get_spectrogram();
    else
        udax = get(ax,'userdata');
        for i=1:length(dibujos)
            dibujo_actual = dibujos(i);
            visible=get(dibujo_actual,'visible');            
            if strcmpi(visible,'on') && udax.crosshairline~=dibujo_actual
                uddib = get(dibujo_actual,'userdata');
                if sum(strcmpi({'markline','selected_signal'},uddib.name))==0 && ~strcmpi(uddib.name,'espectro')
                    p=[p;dibujo_actual];                
                end
            end            
        end
        
        if length(p)==1
            ud = get(p,'userdata');
            if sum(strcmpi({'espectro','espectrograma'},ud.name))>0
                p = get_signal_plot(ax,ud.refer_plot);
            end
        end
        
%         if isempty(p)
%             p = get_spectrogram();
%         end
    end
end
end