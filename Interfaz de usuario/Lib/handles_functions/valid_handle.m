function flag=valid_handle(handle)
if ~isempty(handle)
   if ishandle(handle) || isobject(handle)
       flag = true;
   else
       flag = false;
   end
else
    flag = false;
end
end

