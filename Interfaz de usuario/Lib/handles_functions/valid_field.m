function flag = valid_field(struct,field,bad_value)

flag = false;
if isfield(struct,field)
    if ~iscell(struct.(field))
        if ~ischar(struct.(field))
            if nargin<3
                bad_value  = 0;
            end
            if ~isstruct(struct.(field)) && ~ismatrix(struct.(field))
                if struct.(field)~=bad_value
                    flag = true;
                end
            else
                if length(struct.(field)) == 1 && ~isstruct(struct.(field))
                    if struct.(field)~=bad_value
                        flag = true;
                    end
                else
                    if ~isempty(struct.(field))
                        flag = true;
                    end
                end
            end
        else
            if nargin<3
                bad_value  = '';
            end
            if ~strcmpi(struct.(field),bad_value)
                flag = true;
            end
        end
    else
        if ~isempty(struct.(field))
            flag = true;
        end
    end
end