%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function point = hover(fig_handle, el_handle, el_pivote)
point = [];

%Obtiene los puntos del elemento
try
set(el_handle,'units','pixels');
catch
end
pos =absolute_position(el_handle);
xi = pos(1); yi=pos(2);
xf = xi+pos(3); yf = yi + pos(4);

%Obtiene el punto donde se encuentra el puntero
cp = get(fig_handle,'currentpoint');

% cp = abstorelpoint(cp,el_handle);
% if nargin==3
%    cp = abstorelpoint(cp,el_pivote)
% else
%     parents = get_parents(el_handle);
%     n = length(parents);
%     if n>1
%         parents = parents(1:end-1);
%         n = length(parents);
%         if n>1
%             for i = 1 : n
%                 cp = abstorelpoint(cp,parents(end-i))
%             end
%         else
%             cp = abstorelpoint(cp,parents)
%         end
%     end
% end
x = cp(1); y = cp(2);

%Verifica que el puntero este sobre el elemento
if isbetween(x,xi,xf) && isbetween(y,yi,yf)
    point = cp;
end
end