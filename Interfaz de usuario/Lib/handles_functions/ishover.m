function flag = ishover(parent, el)
    if(isempty(hover(parent,el)))
        flag = 0;
    else
        flag = 1;
    end
end