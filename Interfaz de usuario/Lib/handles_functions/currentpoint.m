function varargout = currentpoint(el)
    parents = get_parents(el);
    fig = parents(end);
    if ishover(fig,el)
        cpf = get(fig,'currentpoint');
       cpa= abstorelpoint(cpf,el);

        if nargout <2
            cp = cpa;
            varargout = {cp};
        else
            varargout ={cpa(1);cpa(2)};
        end
    else
        if nargout<2
            varargout = cell(1,2);
        else
            varargout = cell(1,2);
        end
    end        
end