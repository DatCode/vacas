function handle = get_text_handle(ax,varargin)
% global h;
% if valid_field(h,'selected_axes',0)
   ch = get(ax,'children');
   types = get(ch,'type');
   idx = strcmpi(types,'text');
   handle = ch(idx);
   
   if nargin>1
      name = varargin{1}; 
      ud = get(handle,'userdata');
      idx2 = strcmpi(ud,name);
      handle = handle(idx2);
   end
% end
end