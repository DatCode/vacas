function labels = get_event_handles(ax)
global h;
if nargin<1
    if valid_field(h,'selected_axes',0)
        ax = h.selected_axes;
    else
        ax=[];
    end
end

ud = get(ax,'userdata');
if valid_field(ud,'etiquetas')
   labels = ud.etiquetas;
else
    labels = [];
end
% function [handles_rectangles, handles_text_labels] = get_event_handles(ax)
% 

% ch=get(ax,'children');
% userdata = get(ch,'userdata');
% idx = [];
% idx2=[];
% for i=1:length(userdata)
%     if valid_field(userdata{i}, 'name','')
%         if strcmpi(userdata{i}.name,'event')
%             idx = [idx i];
%         end
%         if strcmpi(userdata{i}.name,'text_label')
%            idx2 = [idx2 i]; 
%         end
%     end
% end
% handles_rectangles = ch(idx);
% handles_text_labels = ch(idx2);
end