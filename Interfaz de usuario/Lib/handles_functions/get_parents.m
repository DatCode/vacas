%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function parents = get_parents(el)
    parents = [];
    flag = 1;
    while(flag)
        dady = get(el,'parent');
        if dady ~= 0
            if isempty(parents)
                parents = dady;
            else
                parents = [parents;dady];
            end
            el = dady;
        else
            flag = 0;
        end
    end
end