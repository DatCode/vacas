function set_inst = set_instruction_for_handles( handles,seteablefields )
fields = fieldnames(handles);
if nargin<2
    seteablefields = {'FontUnits';'Units';'FontWeight';'FontName';'FontAngle';'FontSize';'Foreground';'Color';'BackgroundColor';...
        'BorderType';'BorderWidth';'Visible';'Title';'TitlePosition';'Tag';'ForegroundColor';'HighlightColor';...
        'ButtonDownFcn';'windowbuttonmotionfcn';'windowbuttondownfcn';'numbertitle';'toolbar';'menu';'name';'closerequestfcn';...
        'resize';'windowscrollwheelfcn';'renderer';'label';'String';'Value';'selectionchangefcn';'Callback';'KeyPressFcn';'KeyReleaseFcn';...
        'Min';'Max';'SliderStep';'ListboxTop';'MenuBar'};
end
quote = '''';
set_inst = cell(1,length(fields));
parent = 0;
contadores = ones(1,12);
enames = cell(length(fields),1);
estilo_n = 0;
for i=1:length(fields)
    set_struct = get(handles.(fields{i}));
    fieldstoset = fieldnames(set_struct);
    try
        estilo = set_struct.Style;
    catch
        try
            estilo = set_struct.Type;
        catch
            
            estilo = [];
        end
    end
    if ~isempty(estilo)
        idx = [];
        try
        idx=find(strcmpi(fields,get(set_struct.Parent,'tag')));
        parent = enames{idx};
        catch
            parent = 'figure1';
        end
        try 
            tag = set_struct.Tag;
        catch
            tag = estilo;
        end
        switch(estilo)
            case 'figure'
                estilo_n = 1;
                if contadores(estilo_n)>1
                    continue;
                end                
                str = [tag '=' 'figure('];
            case 'edit'
                estilo_n = 2;
                str = [tag '='  'uicontrol(' parent ',' quote 'style' quote ',' quote 'edit' quote ','];
            case 'text'
                estilo_n = 3;
                str = [tag '='  'uicontrol(' parent ',' quote 'style' quote ',' quote 'text' quote ','];
            case 'popupmenu'
                estilo_n = 4;
                str = [tag '='  'uicontrol(' parent ',' quote 'style' quote ',' quote 'popupmenu' quote ','];
            case 'radiobutton'
                estilo_n=5;
                str = [tag '=' 'uicontrol(' parent ',' quote 'style' quote ',' quote 'radiobutton' quote ','];
            case 'pushbutton'
                estilo_n=6;
                str = [tag '=' 'uicontrol(' parent ',' quote 'style' quote ',' quote 'pushbutton' quote ','];
            case 'uipanel'
                estilo_n=7;
                str = [tag '='  'uipanel(' parent ','];
            case 'togglebutton'
                estilo_n=8;
                str = [tag '=' 'uicontrol(' parent ',' quote 'style' quote ',' quote 'togglebutton' quote ','];
            case 'checkbox'
                estilo_n=9;
                str = [tag '=' 'uicontrol(' parent ',' quote 'style' quote ',' quote 'checkbox' quote ','];
            case 'slider'
                estilo_n=10;
                str = [tag '=' 'uicontrol(' parent ',' quote 'style' quote ',' quote 'slider' quote ','];
            case 'listbox'
                estilo_n=11;
                str = [tag '=' 'uicontrol(' parent ',' quote 'style' quote ',' quote 'listbox' quote ','];
            case 'uibuttongroup'
                estilo_n=12;
                str = [tag '=' 'uibuttongroup(' parent ','];
                
            otherwise
                a=1;
        end
        %         str = ['set(handles.' fields{i}];
        for j = 1:length(fieldstoset)
            if sum(strcmpi(seteablefields,fieldstoset{j}))>0
                var = set_struct.(fieldstoset{j});
                if ~isempty(var)
                    try
                        type = find([ischar(var) ismatrix(var) isnumeric(var) or(ishandle(var(1)),isobject(var(1)))]);
                    catch
                        type = find([ischar(var) ismatrix(var) isnumeric(var) ishandle(var)]);
                    end
                    if ~isempty(type)
                        type = type(1);
                        if strcmpi(fieldstoset{j},'callback')
                            a=1;
                        end
                        switch(type)
                            %Char
                            case 1
                                str = [str quote fieldstoset{j} quote ',' quote var quote ','];
                                %Matriz
                            case 2
                                if strcmpi(class(var),'function_handle')
                                    func_name = fix_func2str(var);
                                    str = [str quote fieldstoset{j} quote ',' func_name ','];
                                else
                                    if iscell(var)
%                                         var=cellfun(@(x) fix_func2str(x),var,'UniformOutput',false);
                                        isbtngroup = false;
                                        try
                                            if strcmpi(char(var{1}),'manageButtons')
                                                isbtngroup=true;
                                            end
                                        catch
                                        end
                                        if ~isbtngroup
                                            str = [str quote fieldstoset{j} quote ',{'];
                                            celltype = find([ischar(var{1}) isnumeric(var{1})]);
                                            try
                                                switch(celltype)
                                                    case 1
                                                        for k=1:length(var)
                                                            str = [str quote var{k} quote ','];
                                                        end
                                                    case 2
                                                        for k=1:length(var)
                                                            str = [str var{k} ','];
                                                        end
                                                end
                                                str = [str(1:end-1) '},'];
                                            catch
                                            end
                                        end
                                    else
                                        str = [str quote fieldstoset{j} quote ',' mat2str(var) ','];
                                    end
                                end
                                %Numero
                            case 3
                                str = [str quote fieldstoset{j} quote ',' num2str(var) ','];
                                
                            case 4
                                str = [str quote fieldstoset{j} quote ',' num2str(var) ','];
                            otherwise
                                
                        end
                    end
                end
            end
        end
        str =[str(1:end-1) ');'];
        try
            str=[str 'set(' tag ',''position'',' mat2str(set_struct.Position) ');handles.' tag '=' tag ';'];
        catch
            
        end
        %     disp(str);
        set_inst{i}=str;
        enames{i}=[tag];
        contadores(estilo_n)=contadores(estilo_n)+1;
    end
end

end

function fixed = fix_func2str(func)
fixed = func;
% if isobject(func)
    if strcmpi(class(func),'function_handle')
        func = char(func);
%         fixed = regexp(func,'\)\w+\(','match');
        fixed =regexp(func,'''\w+''','match');
        fixed = char(fixed);
%         fixed = fixed(2:end-1);
        fixed = char(fixed);
        fixed = ['@' fixed(2:end-1)];
    end
% end
end