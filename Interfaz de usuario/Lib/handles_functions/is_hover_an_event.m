function handle = is_hover_an_event()
global h;
handle = [];
if valid_field(h,'selected_axes',0)
    ax=h.selected_axes;
    events = get_event_handles();
    %     xdatas = get(events,'xdata');
    
    if ~isempty(events)
        cp = currentpoint(ax);
        pos = get(ax,'position');
        limx = get(ax,'xlim');
        time_cp = cp(1)/pos(3)*(limx(2)-limx(1))+limx(1);
        idx=[];
        %         if iscell(xdatas)
        %             largo = length(xdatas);
        %         else
        %             largo = 1;
        %         end
        minduration = [];
        for i=1:length(events)
            %             try
            %                 t1 = xdatas{i}(1);
            %                 t2 = xdatas{i}(2);
            %             catch
            %                t1 = xdatas(1);
            %                t2 = xdatas(2);
            %             end
            %             datestr(t1)
            %             datestr(t2)
            %             if is_between(time_cp,t1,t2)
            %                 if isempty(idx)
            %                     idx = i;
            %                 else
            %                     t1a = xdatas{idx}(1);
            %                     t2a = xdatas{idx}(2);
            %                     t1b = t1;
            %                     t2b = t2;
            %
            %                     if abs(t1a-t2a) >abs(t1b-t2b)
            %                         idx = i;
            %                     else
            %                         idx = idx;
            %                     end
            %                 end
            %             end
            if events{i}.ishover(time_cp)
                if isempty(minduration)
                    minduration = events{i}.get_duration;
                    idx=i;
                else
                    if minduration>events{i}.get_duration
                        minduration = events{i}.get_duration;
                        idx=i;
                    end
                end
            end
            
        end
        handle = events(idx);
    end
end
end
% 
% function flag = is_between(val,n1,n2)
% if (val>=n1 && val<=n2) || (val>=n2 && val<=n1)
%     flag=true;
% else
%     flag=false;
% end
% end