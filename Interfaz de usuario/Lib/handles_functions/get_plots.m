function plots=get_plots(ax)
    ch = get(ax,'children');
    plots = [];
    for i = 1: length(ch)
        type = get(ch(i),'type');
        if strcmpi(type,'line')
            if isempty(plots)
                plots = ch(i);
            else
                plots = [plots ch(i)];
            end
        end
    end
end