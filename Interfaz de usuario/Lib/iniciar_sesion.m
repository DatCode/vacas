function [user,pass,grupo] = iniciar_sesion()
%% Trata de conectarse
filename = 'conf.txt';
global datos_user;
conectado=false;
user = [];
pass = [];
grupo = [];
while (~conectado)
    if ~checkTextFile('conf.txt')
        my_gui = myGUI;
        waitfor(my_gui);
        pause(0.001);
        
        if datos_user.cierra == 1
            delete(hObject);
            clear ('datos_user');
            return;
        end
        
        nombre = datos_user.user;
        pass = datos_user.pass;
        grupo = datos_user.grupo;
        
        if datos_user.recordar
            %% Guarda la contraseņa para la proxima conexion
            fid = fopen(filename,'w');
            fwrite(fid,sprintf('user: %s\npass: %s\ngrupo: %s',char(nombre),char(pass),char(grupo)));
            fclose(fid);
        end
        
        if datos_user.autoiniciar
            %% Guarda la contraseņa para la proxima conexion
            fid = fopen(filename,'w');
            fwrite(fid,sprintf('user: %s\npass: %s\ngrupo: %s\nautoiniciar: %d',char(nombre),char(pass),char(grupo),1));
            fclose(fid);
        end
        
    else
        if getTxtProperty(filename,'autoiniciar')
            nombre = getTxtProperty(filename,'user');
            grupo = getTxtProperty(filename,'grupo');
            pass = getTxtProperty(filename,'pass');
        else
            my_gui = myGUI(getTxtProperty(filename,'user'),getTxtProperty(filename,'pass'),getTxtProperty(filename,'grupo'));
            waitfor(my_gui);
            
            if datos_user.cierra == 1
                delete(hObject);
                clear ('datos_user');
                return;
            end
            nombre = datos_user.user;
            pass = datos_user.pass;
            grupo = datos_user.grupo;
            
            if datos_user.recordar
                %% Guarda la contraseņa para la proxima conexion
                fid = fopen(filename,'w');
                fwrite(fid,sprintf('user: %s\npass: %s\ngrupo: %s',char(nombre),char(pass),char(grupo)));
                fclose(fid);
            end
            
            if datos_user.autoiniciar
                %% Guarda la contraseņa para la proxima conexion
                fid = fopen(filename,'w');
                fwrite(fid,sprintf('user: %s\npass: %s\ngrupo: %s\nautoiniciar: %d',char(nombre),char(pass),char(grupo),1));
                fclose(fid);
            end
        end
        
    end
%     dbSource = 'cesar';
% dbSource = getTxtProperty('conector_conf.txt','dbsource_etiquetas');
   if ~set_cow_connector(nombre,pass)
       nombre=[];
       pass = [];
       grupo = [];
   else
       conectado = true;
   end
end
user = nombre;
% close(conn);
clear global datos_user;
end