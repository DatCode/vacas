function data = get_limited_data(p)
try
    ax = get(p,'parent');
    
    %Verifica que no sea diferente ud.display que pud.name
    ud = get(ax,'userdata');
    pud = get(p,'userdata');
    
    limx = get(ax,'xlim');
    xdata = get(p,'xdata');
    ydata = get(p,'ydata');
    
    if strcmpi(ud.display,pud.name)
        idx = xdata>=limx(1) & xdata<=limx(2);
        xdata = xdata(idx);
        ydata = ydata(idx);
    end
    
    data.xdata = xdata;
    data.ydata = ydata;
catch
    data.xdata =[];
    data.ydata = [];
end
end

