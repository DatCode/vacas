function pos = get_pos(ax,type)
unit = get(ax,'units');
if ~strcmpi(unit,type)
    set(ax,'units',type);
end
pos = get(ax,'position');
set(ax,'units',unit);
end

