function refresh_date_xtick(ax,nlabels,format)

%% Obtiene xdata del plot
if nargin<1
    global h;
    global tick_per_label;
    if valid_field(h,'selected_axes',0)
        ax = h.selected_axes;
    else
        ax = gca;
    end
    %     p = get_signal_plot(ax,'current');
    %     xdata = get(p,'xdata');
else
    if nargin<2
        global tick_per_label
    else
        tick_per_label = nlabels;
        
        
    end
end
if nargin<3
    format = 'HH:MM:SS';
end
pos = get_pos(ax,'pixels');
limx = get(ax,'xlim');
if tick_per_label==0
tick_per_label=floor(pos(3)/440*5);
end

%% Formatea el eje x
datetick(ax,'x',13,'keeplimits');

% if length(xdata)>2
%     p = get_signal_plot(ax,'current');
%     if isempty(p)
%         p = get_signal_plot(ax,'filtered_signal');
%     else
%         p=p(end);
%     end
    %     xdata = get(p,'xdata');
%     data = get_limited_data(p);
%     xdata = data.xdata;
    xtick_data = linspace(limx(1),limx(2),tick_per_label);
    set(ax,'xticklabel',datestr(xtick_data,format));
    set(ax,'xtick',xtick_data);
% end
end