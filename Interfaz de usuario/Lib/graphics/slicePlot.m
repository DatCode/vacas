%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function slicePlot(plotin,t1,t2,color,varargin)
global h;
x=get(plotin,'xdata');
y=get(plotin,'ydata');
ax= get(plotin,'parent');
id = x>=t1 & x<=t2;
x2=x(id);
y2=y(id);
selected_signal = get_signal_plot(ax,'selected_signal');
if ~isempty(selected_signal) && ishandle(selected_signal)
    
    set(selected_signal,'xdata',x2,'color',color,'ydata',y2);
else
    selected_signal = plot(ax,x2,y2,'color',color);
    userdata.name = 'selected_signal';
    set(selected_signal,'userdata',userdata);
end
%
% if(t1>t2)
%    aux = t2;
%    t2 =t1;
%    t1 = aux;
% end
%
% if nargin<5
%     idx = find(x>=t1 & x<=t2);
%     new_x = x(idx);
%     new_y = y(idx);
%
%     if(length(idx)>3)
%         x(idx(2:end-1))=NaN;
%         y(idx(2:end-1))=NaN;
%     end
%     hold on;
%
%     set(plotin,'xdata',x);
%     set(plotin,'ydata',y);
%     p = plot(new_x,new_y,'color',color);
% %     disp('crea plot');
% else
% %     disp('existe p(2)');
%     x2 = get(plotin2,'xdata');
%     y2 = get(plotin2,'ydata');
%
%     if(length(x2)>3)
%         x(isnan(x))=x2(2:end-1);
%         y(isnan(y))=y2(2:end-1);
%     end
%
%     idx = find(x>=t1 & x<=t2);
%     new_x = x(idx);
%     new_y = y(idx);
%
%     if(length(idx)>3)
%         x(idx(2:end-1))=NaN;
%         y(idx(2:end-1))=NaN;
%     end
%
%     hold on;
%
%     set(plotin,'xdata',x,'ydata',y);
%     set(plotin2,'xdata',new_x,'ydata',new_y);
%     p=plotin2;
% end
% h.selected_signal = p;
end