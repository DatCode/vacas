function remember_limits(plot1,plot2)
xdata = get(plot1,'xdata');
ydata = get(plot1,'ydata');
ax = get(plot1,'parent');
if nargin<2
    plot2 = plot1;
end
xdata2 = get(plot2,'xdata');
ydata2 =get(plot2,'ydata');

if(min(xdata)<min(xdata2))
    lim1 = min(xdata);
else
    lim1 = min(xdata2);
end
if(max(xdata)>max(xdata2))
    lim2 = max(xdata);
else
    lim2 = max(xdata2);
end

if(min(ydata)<min(ydata2))
    ylim1 = min(ydata);
else
    ylim1 = min(ydata2);
end
if(max(ydata)>max(ydata2))
    ylim2 = max(ydata);
else
    ylim2 = max(ydata2);
end
if(lim1>lim2)
    aux=lim2;
    lim2 =lim1;
    lim1=aux;
end

if(ylim1>ylim2)
    aux=ylim2;
    ylim2=ylim1;
    ylim1=aux;
end

% if lim1~=lim2 && ylim1~=ylim2
if lim1~=lim2
    xlim(ax,[lim1 lim2]);
end
if ylim1==0 && ylim2==0
    ylim1 = -0.5;
    ylim2 = 0.5;
end
if ylim1~=ylim2
    ylim(ax,[ylim1*1.1 ylim2*1.1]);
end
% end
% disp(['Lim superior: ' num2str(ylim2*1.1) ' inferior: ' num2str(ylim1*0.9)]);
ylabel(ax,'');

userdata = get(plot1,'userdata');
if valid_field(userdata,'name','')
    if strcmpi(userdata.name,'espectro')
        %        xdata = get(p,'xdata');
        try
            set(ax,'xticklabel',xdata(floor(linspace(1,length(xdata),10))));
            set(ax,'xtick',xdata(floor(linspace(1,length(xdata),10))));
        catch
            set(ax,'xticklabel',xdata,'xtick',xdata);
        end
    else
        refresh_date_xtick(ax,0);
    end
end
end
