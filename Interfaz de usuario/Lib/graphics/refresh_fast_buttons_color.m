function refresh_fast_buttons_color(varargin)

p = inputParser;
defaultMode = 'toggle';
modes = {'refresh','toggle'};
addParameter(p,'mode',defaultMode,@(x) any(validatestring(x,modes)));
p.parse(varargin{:});
mode = p.Results.mode;

global h;
global colores;
non_sel = colores.nonsel_fast_btn;

if valid_field(h,'selected_axes')
    if isobject(h.selected_axes) || ishandle(h.selected_axes)
        userdata = get(h.selected_axes,'userdata');
        switch(userdata.display)
            case 'spectre'
                idx = 4;
                mode_exec(mode,idx);
                return;
            case 'spectrogram'
                idx = 5;
                mode_exec(mode,idx);
                return;
            case 'filtered_signal'
                bounds = get(h.fast_button(4:5),'parent');
                cellfun(@(x) set(x,'ycolor',non_sel,'xcolor',non_sel,'box','off'),bounds);
            otherwise
                %                 disp('Error inesperado. Function refresh_fast_buttons_colors()');
                bounds = get(h.fast_button(4:5),'parent');
                cellfun(@(x) set(x,'ycolor',non_sel,'xcolor',non_sel,'box','off'),bounds);
        end
    end
else
    bounds = get(h.fast_button(4:5),'parent');
    cellfun(@(x) set(x,'ycolor',non_sel,'xcolor',non_sel,'box','off'),bounds);
end
end

function mode_exec(mode,idx)
global h;
global colores;
sel_color = colores.sel_fast_btn;
nonsel_color = colores.nonsel_fast_btn;
switch(mode)
    case 'refresh'
        %Obtiene grupos
        groups = cell2mat(cellfun(@(x) x.group,get(h.fast_button,'userdata'),'uniformoutput',false));
        btn_group = groups(idx);
        same_group_btn = find(groups==btn_group);
        arrayfun(@(x) set(h.fast_btn_ax(x),'xcolor',nonsel_color,'ycolor',nonsel_color),...
            same_group_btn);
        set(h.fast_btn_ax(idx),'xcolor',sel_color,'ycolor',sel_color);
    case 'toggle'
        toggle_fast_button(h.fast_button(idx));
end
end