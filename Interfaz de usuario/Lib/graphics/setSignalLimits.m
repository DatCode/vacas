function setSignalLimits(varargin)
global h;
global flag;
flag.extending_plot= true;

%Maneja entrada
if nargin<3 && valid_field(h,'selected_axes',0)
    ax = h.selected_axes;
    lim = varargin{1};
    event = varargin{2};
else
    if nargin<2
        ax = gca;
        lim = varargin{1};
        event = varargin{2};
    else
        ax = varargin{1};
        lim = varargin{2};
        event = varargin{3};
    end
end
seg = datenum(0,0,0,0,0,1);
lim = lim + [-seg +seg];
%Aplica limites a todos los axes enlazados, incluyendose a si mismo
ud = get(ax,'userdata');
linked = ud.linked_ax;
for i = 1:length(linked)
    set_limits(linked(i),lim,event);
end
flag.extending_plot= false;
end

function set_limits(ax,lim,event)
%Datos del ax
limx = get(ax,'xlim');
ud = get(ax,'userdata');
distx = limx(2)-limx(1);
maxdisttol = datenum(0,0,0,1,0,0);
if strcmpi(ud.zoom,'off')
    %Obtiene plots
    p = get_signal_plot(ax,'original_signal');
    p_filt = get_signal_plot(ax,'filtered_signal');
    p_ev_zoom=get_signal_plot(ax,'event_zoom');
    
    %Obtiene datos temporales de los graficos
    xdata = get(p,'xdata'); ydata = get(p,'ydata');
    xdata2 = get(p_filt,'xdata');ydata2=get(p_filt,'ydata');
    display_state('Buscando senial en el servidor...');
    
    %Ajusta la se�al al tramo pedido
    idx=xdata>=lim(1) & xdata<=lim(2);
    xdata = xdata(idx);
    ydata = ydata(idx);
    idx2 = xdata2>=lim(1) & xdata2<=lim(2);
    xdata2=xdata2(idx2);
    ydata2=ydata2(idx2);
    
    if distx<maxdisttol && (lim(1)>=limx(1) && lim(2)<=limx(2))
        if lim(1)>=limx(1) && lim(2)<=limx(2)
            %El tramo pedido esta dentro del rango de se�al actual, por lo
            %que no es necesario volver a pedirla

            set(p,'xdata',xdata,'ydata',ydata);
            set(p_filt,'xdata',xdata2,'ydata',ydata2);
        else
            %El tramo pedido necesita parte de la se�al y parte ya se tiene
            if lim(1)<limx(1)
                t1 = lim(1);t2=limx(1);
                %                 datos = SQLdata(t1,t2,'conversion','um/s','componente',ud.componente,'estacion',ud.estacion);
                request = ud.request;
                request.range_x = [t1 t2];
                datos = get_signal(request);
                senial = center_signal(datos.(input.coly));
                tiempo = datos.(input.colx{1});
                
                newy1 = [senial' ydata];
                newx1 = [tiempo' xdata];
                
                newx2 = [tiempo' xdata2 ];
                newy2 = [filter_signal(senial',ud.filtername) ydata2];
            else
                t1 = limx(2); t2 = lim(2);
                %                 datos = SQLdata(t1,t2,'conversion','um/s','componente',ud.componente,'estacion',ud.estacion);
                request = ud.request;
                request.range_x = [t1 t2];
                
                datos = get_signal(request);
                senial = center_signal(datos.(input.coly));
                tiempo = datos.(input.colx{1});
                newy1 = [ydata senial'];
                newx1 = [xdata tiempo'];
                
                newx2 = [xdata2 tiempo'];
                newy2 = [ydata2 filter_signal(senial',ud.filtername)];
            end
            
            %Reemplaza los valores en el grafico
            set(p,'xdata',newx1,'ydata',newy1);
            try
                set(p_ev_zoom,'xdata',newx2,'ydata',newy2);
            end
            set(p_filt,'xdata',newx2,'ydata',newy2);
            remember_limits(p_filt);
            
            
        end
    else
        %Busca la se�al completa y la reemplaza
        %         datos=SQLdata(lim(1),lim(2),'conversion','um/s','componente',ud.componente,'estacion',ud.estacion);
        request = ud.request;
%         request.range_x = [lim(1),lim(2)];
%         request.fuente = 'predial';
%         request.fuente_val = num2str(event.id_predial);
%         datos = get_signal(request);
        datos=sel_signal(lim(1),lim(2),num2str(event.id_predial),'acelerometro','datenum');
        senial = center_signal(datos.(request.coly));
        tiempo = datos.('it');
        
        set(p,'xdata',tiempo,'ydata',senial);
        set(p_filt,'xdata',tiempo,'ydata',filter_signal(datos.(request.coly)',ud.filtername));
        try
                set(p_ev_zoom,'xdata',tiempo,'ydata',filter_signal(signal',ud.filtername));
        end
        remember_limits(p_filt);
    end
    
    refresh_filtername_text(ax);
    refresh_current_ax_events(ax);
    display_events_in_ax(ax,'refresh');
end
end