% test draw_spect

close all
clear all
clc
x=0:0.01:10;
y=sin(20*2*pi*x);
f=figure();
h=plot(x,y);

draw_spect(h);