function toggle_original_signal( )
global h;
if valid_field(h,'selected_axes',0)
% ud = get(h.selected_axes,'userdata');


op = get_signal_plot(h.selected_axes,'original_signal');
vop = get(op,'visible');

if strcmpi(vop,'on')
    show_in_graph({'filtered_signal'},h.selected_axes);
else
    show_in_graph({'original_signal','filtered_signal'},h.selected_axes);
end
end
end

