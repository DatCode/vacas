%% Funcion que permite pushear bot
function pushdown(varargin)
global h;

distance = varargin{3};

%Crea el objeto capaz de manejar varargin
p = inputParser;

%Se definen los valores por default
defaultstart = 'this';
possiblestart = {'this','next'};

%A�ade al esquema los argumentos chequeandolos
addOptional(p,'start',defaultstart,@(x) any(validatestring(x,possiblestart)));
addRequired(p,'object',@(x) isobject(x)||ishandle(x));

parse(p,varargin{1:end-1});

resultados = p.Results;

%Pushea los elementos hacia abajo
elementos = get(h.axes_panel,'children');

%filtra los elementos
if strcmpi(resultados.start,'next')
    k = find(elementos~=resultados.object);
    elementos = elementos(k);
end

posiciones =get(elementos,'position');
if iscell(posiciones)
    posiciones = cell2mat(posiciones);
end
posref = get(resultados.object,'position');

height = posref(4);
margin = distance;

if ~isempty(posiciones)
    %Obtiene los elementos que estan por debajo de la referencia
    idx = find(posiciones(posiciones(:,2)<=posref(2)));
    
    %Recorre elementos
    for i=1:length(idx)
        set(elementos(idx(i)),'position',posiciones(idx(i),:)-[0 height+margin 0 0]);
    end
    
    %Checkea bounds
    checknewaxesbounds();
end
end