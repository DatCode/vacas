%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function [button,ax,callbacks] = image_button(varargin)
%% Input:
%                padre                : Elemento padre
%                Posicion            : Vector posicion para el boton
%                Ruta imagen    : Ruta al archivo imagen

ax = -1;
img = -1;

%A�ade el path con las imagenes
% addpath('../images/');

%% Maneja entradas
if nargin>=3
    %Obtiene al padre
    padre = varargin{1};
    fig = padre;
    if ~strcmpi(get(fig,'type'),'figure')
        parents = get_parents(padre);
        fig = parents(end);
    end
    
    
    %Crea el axes contenedor
    pos_ax = varargin{2};
    ax = axes('parent',padre,'units','pixels','position',pos_ax);
    
    %Lee la imagen y la carga al axes
    if nargin==4
        path_img = varargin{4};
        if ~exist(path_img,'file')
            path_img = 'not_found.jpeg';
        end
    else
        path_img = 'not_found.jpeg';
    end
    if(strcmpi(path_img(end-2:end),'png'))
        %         bg = get(fig,'color');
        bg = [1 1 1];
        try
            im =imread(path_img,'BackgroundColor',bg);
        catch
            im = imread(path_img);
        end
    else
        im = imread(path_img);
        %         im = imread(path_img)*0.5;     OSCURECE LA IMAGEN
    end
    im = imresize(im,pos_ax(3:4));
    img = image(im,'parent',ax,'buttondownfcn',{@onclick,varargin{3}});
    
    %Establece valores iniciales
    set(ax,'ytick',[],'yticklabel',[],'xtick',[],'xticklabel',[]);
    box(ax,'off');
    color = [0.5 0.5 0.5];
    set(ax,'xcolor',color,'ycolor',color);
    set(ax,'userdata',0);
    set(img,'userdata',0);
end

button = img;
callbacks = {img, @animate_sel_button;...
    img,@mouse_motion_handler};
end

function delete_button(src,evt)
delete(src);
% clear global clicked;
end

%Funcion evento a click
function onclick(src,evt,evt_func)
animate_sel_button(src,evt_func);
end

%Funcion que anima al boton al ser seleccionado
function animate_sel_button(img,evt_func)
%Pregunta si ya se esta animando el boton o no
src = get(img,'parent');
clicked = get(src,'userdata');

%En caso de que no se este animando actualmente
if clicked == 0
    %Avisa que ahora si se encuentra animandose
    clicked = 1;
    set(src,'userdata',clicked);
    
    %Define colores de seleccion y desseleccion
    nonsel_color = [0.5 0.5 0.5];
    sel_color = 'black';
    
    %Implementa color de seleccion
    %     box(src);
    set(src,'xcolor',sel_color,'ycolor',sel_color);
    
    %Define valores para la animacion
    pos_ini = get_position(src); %Posicion inicial del elemento
    pixels = 1; %Rango de pixeles a agrandar y achicar
    nmovs = 5;%Cantidad de frames
    time = 0.025;%Tiempo para llevarlo a cabo
    ts = time/nmovs;%Tiempo por frame
    
    pos = pos_ini;%Inicializa la posicion
    
    %Realiza la animacion de enpeque�ecimiento
    for i=1:nmovs
        pix = pixels/nmovs*i;
        pos = pos - [-pix/2 -pix/2 pix pix]; %Achica el boton
        set(src,'position',pos);
        pause(ts);
    end
    
    %Realiza la animacion de agrandamiento
    for i = 1:nmovs
        pix = pixels/nmovs*i;
        pos = pos + [-pix/2 -pix/2 pix pix];%Agranda el boton
        set(src,'position',pos);
        pause(ts);
    end
    
    %Implementa color de deseleccion
    %     box(src,'off');
    set(src,'xcolor',nonsel_color,'ycolor',nonsel_color);
    
    %Avisa que se deja de ocupar la animacion
    clicked = 0;
    set(src,'userdata',clicked);
    evt_func();
end
end

%Funcion para cambiar cursor, en caso de estar el mouse por sobre el boton
function mouse_motion_handler(src,varargin)
pos = get_position(src);
cp = get(gcf,'currentpoint');
xi = pos(1);
xf = xi+pos(3);
yi = pos(2);
yf = yi+pos(4);
before_entered = get(src,'userdata');
if before_entered
    if isbetween(cp(1),xi,xf) && isbetween(cp(2),yi,yf)
        set(gcf,'pointer','hand');
    end
else
    if ~isbetween(cp(1),xi,xf) && isbetween(cp(2),yi,yf)
        set(gcf,'pointer','normal');
    end
end
end

%% Funciones adicionales
function height=get_height(handle)
pos = get_position(handle);
height = pos(4);
end

function width = get_width(handle)
pos = get_position(handle);
width = pos(3);
end

function position = get_position(handle)
position = get(handle,'position');
end