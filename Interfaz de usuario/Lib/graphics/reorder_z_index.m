function reorder_z_index(ax)
% global h;
% if nargin==0
%     if valid_field(h,'selected_axes',0)
%         ax = h.selected_axes;
%     else
%         ax = gca;
%     end
% end
% childs  = get(ax,'children');
% order = {'event','selected_signal','filtered_signal','original_signal'};
% for i=1:length(childs)
%     for j=1:length(childs)
%         userdatai = get(childs(i),'userdata');
%         userdataj = get(childs(j),'userdata');
%         if valid_field(userdatai,'name','') && valid_field(userdataj,'name','')
%             idxi = find(strcmpi(order,userdatai.name));
%             idxj = find(strcmpi(order,userdataj.name));
%             
%             if ~isempty(idxi) && ~isempty(idxj)
%                 if idxj>idxi
%                     aux = childs(j);
%                     childs(j) = childs(i);
%                     childs(i)=aux;
%                 end
%             end
%         end
%     end
% end
% set(ax,'children',childs);
% uistack(h.button_panel,'top');
end