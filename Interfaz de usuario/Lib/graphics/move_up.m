function move_up(varargin)
global h;
if valid_field(h,'selected_axes',0)
   axpos = get(h.selected_axes,'position');
   positions = get(h.ax,'position');
   mindist=0;
   minax = 0;
   for i = 1:length(positions)
      if axpos(2)-positions{i}(2)<mindist && axpos(2)-positions{i}(2)~=0
          mindist = axpos(2)-positions{i}(2);
          minax = h.ax(i);
      end
   end
   if isnumeric(minax)
       if minax==0
          return ;
       end
   end
   interpos = get(minax,'position');
   set(h.selected_axes,'position',interpos);
   set(minax,'position',axpos);
end
end