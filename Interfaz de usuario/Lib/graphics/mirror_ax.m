function mirror_ax(ax)
global h;
if nargin<1
    ax = h.selected_axes;
end

ud = get(ax,'userdata');
linked = ud.linked_ax;
linked = unique(linked(linked~=ax & linked~=h.selected_axes));
%Define variables a repicar
limx = get(ax,'xlim');
%ud.display = 'zoom'

for i = 1:length(linked)
    replicate_times(linked(i),limx,ud);
    
end
end

function replicate_times(ax,limx,ud)
if strcmpi(ud.zoom,'off')
    display_state('Buscando senial faltante...');
    set_limx_data(get_signal_plot(ax,'filtered_signal'),limx);
    set_limx_data(get_signal_plot(ax,'original_signal'),limx);
    set_limx_data(get_signal_plot(ax,'selected_signal'),limx);
    if strcmpi(ud.event_display,'true')
        refresh_events_in_graph(ax);
    end
    
    if strcmpi(ud.display,'spectrogram')
        refresh_spectrogram(ax)
    else
        remember_limits(get_signal_plot(ax,'filtered_signal'));
        display_events_in_ax(ax,'refresh')
    end
    refresh_filtername_text(ax)
    refresh_date_xtick(ax)
    display_state('Listo...');
%     remember_limits(get_signal_plot(ax,'filtered_signal'));
else
    
end
end
function set_limx_data(p,limx)
if ~isempty(p)
%Datos del ax
ax=get(p,'parent');
limx = get(ax,'xlim');
lim = limx;
ud = get(ax,'userdata');
distx = limx(2)-limx(1);
maxdisttol = datenum(0,0,0,1,0,0);
if strcmpi(ud.zoom,'off')
    %Obtiene plots
    p = get_signal_plot(ax,'original_signal');
    p_filt = get_signal_plot(ax,'filtered_signal');
    p_ev_zoom=get_signal_plot(ax,'event_zoom');
    
    %Obtiene datos temporales de los graficos
    xdata = get(p,'xdata'); ydata = get(p,'ydata');
    xdata2 = get(p_filt,'xdata');ydata2=get(p_filt,'ydata');
    display_state('Buscando senial en el servidor...');
    
    %Ajusta la se�al al tramo pedido
    idx=xdata>=lim(1) & xdata<=lim(2);
    xdata = xdata(idx);
    ydata = ydata(idx);
    idx2 = xdata2>=lim(1) & xdata2<=lim(2);
    xdata2=xdata2(idx2);
    ydata2=ydata2(idx2);
    
    if distx<maxdisttol && (lim(1)>=limx(1) && lim(2)<=limx(2))
        if lim(1)>=limx(1) && lim(2)<=limx(2)
            %El tramo pedido esta dentro del rango de se�al actual, por lo
            %que no es necesario volver a pedirla

            set(p,'xdata',xdata,'ydata',ydata);
            set(p_filt,'xdata',xdata2,'ydata',ydata2);
        else
            %El tramo pedido necesita parte de la se�al y parte ya se tiene
            if lim(1)<limx(1)
                t1 = lim(1);t2=limx(1);
                %                 datos = SQLdata(t1,t2,'conversion','um/s','componente',ud.componente,'estacion',ud.estacion);
                request = ud.request;
                request.range_x = [t1 t2];
                datos = get_signal(request);
                senial = center_signal(datos.(input.coly));
                tiempo = datos.(input.colx{1});
                
                newy1 = [senial' ydata];
                newx1 = [tiempo' xdata];
                
                newx2 = [tiempo' xdata2 ];
                newy2 = [filter_signal(senial',ud.filtername) ydata2];
            else
                t1 = limx(2); t2 = lim(2);
                %                 datos = SQLdata(t1,t2,'conversion','um/s','componente',ud.componente,'estacion',ud.estacion);
                request = ud.request;
                request.range_x = [t1 t2];
                
                datos = get_signal(request);
                senial = center_signal(datos.(input.coly));
                tiempo = datos.(input.colx{1});
                newy1 = [ydata senial'];
                newx1 = [xdata tiempo'];
                
                newx2 = [xdata2 tiempo'];
                newy2 = [ydata2 filter_signal(senial',ud.filtername)];
            end
            
            %Reemplaza los valores en el grafico
            set(p,'xdata',newx1,'ydata',newy1);
            try
                set(p_ev_zoom,'xdata',newx2,'ydata',newy2);
            end
            set(p_filt,'xdata',newx2,'ydata',newy2);
            remember_limits(p_filt);
            
            
        end
    else
        %Busca la se�al completa y la reemplaza
        %         datos=SQLdata(lim(1),lim(2),'conversion','um/s','componente',ud.componente,'estacion',ud.estacion);
        request = ud.request;
%         request.range_x = [lim(1),lim(2)];
%         request.fuente = 'predial';
%         request.fuente_val = num2str(event.id_predial);
%         datos = get_signal(request);
        datos=sel_signal(lim(1),lim(2),num2str(ud.predial),'acelerometro','datenum');
        senial = center_signal(datos.(request.coly));
        tiempo = datos.('it');
        
        set(p,'xdata',tiempo,'ydata',senial);
        set(p_filt,'xdata',tiempo,'ydata',filter_signal(datos.(request.coly)',ud.filtername));
        try
                set(p_ev_zoom,'xdata',tiempo,'ydata',filter_signal(signal',ud.filtername));
        end
        remember_limits(p_filt);
    end
    
    refresh_filtername_text(ax);
    refresh_current_ax_events(ax);
    display_events_in_ax(ax,'refresh');
end
end
end

% function set_limx_data(p,limx)
% if ~isempty(p)
%     ax = get(p,'parent');
%     ud = get(ax,'userdata');
%     pud = get(p,'userdata');
%     xdata = get(p,'xdata');
%     ydata = get(p,'ydata');
%     idx = xdata>=limx(1) & xdata<=limx(2);
%     xdata = xdata(idx);
%     ydata = ydata(idx);
%     if isempty(xdata)
%         newx1 = limx;
%         newx2 = [];
%     else
%         newx1 = [limx(1) xdata(1)];
%         newx2 = [xdata(end) limx(2)];
%     end
%     
%     %Busca faltante izquierdo
%     datos1 = SQLdata(newx1(1),newx1(2),'conversion','um/s','componente',ud.componente,'estacion',ud.estacion);
%     senial = center_signal(datos1.SQLdata_data);
%     if strcmpi(pud.name,'filtered_signal')
%         filt_senial = filter_signal(senial,ud.filtername);
%     else
%         filt_senial = senial;
%     end
%     tiempo = datos1.SQLdata_time;
%     
%     xdata = [tiempo' xdata(2:end)];
%     ydata = [filt_senial' ydata(2:end)];
%     
%     %Busca faltante derecho
%     if ~isempty(newx2)
%         datos1 = SQLdata(newx2(1),newx2(2),'conversion','um/s','componente',ud.componente,'estacion',ud.estacion);
%         senial = center_signal(datos1.SQLdata_data);
%         if strcmpi(pud.name,'filtered_signal')
%             filt_senial = filter_signal(senial,ud.filtername);
%         else
%             filt_senial = senial;
%         end
%         tiempo = datos1.SQLdata_time;
%         
%         xdata = [xdata(2:end) tiempo'];
%         ydata = [ydata(2:end) filt_senial'];
%     end
%     set(p,'xdata',xdata,'ydata',ydata);
% end
% end

function refresh_spectrogram(ax)
s = get_spectrogram(ax);
sud = get(s,'userdata');
% global h;
if ~isempty(s)
    p = get_signal_plot(ax,sud.refer_plot);
    senal = get(p,'ydata');
    t = get(p,'xdata');
    
    %Calcula el espectrograma
    userdata = get(ax,'userdata');
kconver = userdata.kconver;
    
    %     idx = senal~=0;
    %     t=t(idx);
    %     senal = senal(idx);%*kconver;
    overlap = 295;
    nfft = 512;
    fs = get_fsamp(t(1));
    window = 300;
    [spect_S,spect_F,~,~]=spectrogram(senal,...
        hamming(window),... %window 300
        overlap,... %n overlap 295
        nfft,... %nfft 1024
        fs,... %fs (fs fija) 100
        'yaxis');
    DEP_Sdb=10*log10(spect_S.*conj(spect_S));
    set(s,'xdata',t,'ydata',spect_F,'cdata',DEP_Sdb);
    
    rango_caxis = [50 100];
    caxis(ax,rango_caxis+20.*log10(kconver));%Color axis rango 50-100 db
    ylim(ax,[0 20]);% Limita 0 - 20Hz
    xlim(ax,[t(1) t(end)]);
end
end