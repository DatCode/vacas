function p = random_plot(varargin)
if nargin==0
    ax = gca;
else
    ax = varargin{1};
end
puntos = floor(rand(1,1)*1000);
max_amp = rand(1,1)*10000;
t = puntos:2*puntos-1;
p = plot(t,random_signal(puntos,max_amp),'parent',ax,'color',[rand() rand() rand()]);
end