function repaint_select()
global h;
global drag;
global clickup;
global flag;
global colores;
if ishandle(h.selected_axes) && ~flag.extending_signal && isfield(h,'rect')
    ud = get(h.selected_axes,'userdata');
    if ishover(h.fig,h.selected_axes) && ~strcmpi(ud.display,'spectrogram')
        p = get_signal_plot(h.selected_axes,'filtered_signal');
        %Selecciona el trozo de se�al momento a momento
        [h.rect, sel_time_range] = select_signal(h.selected_axes,drag.start,h.rect);
        try
%             if ~isempty(p(2))
%                 if ishandle(p(2))
                    %Pinta el trozo de se�al haciendo el slide
                    brand_selected_signal(p,sel_time_range,colores.selected_signal);
                    limx = get(h.selected_axes,'xlim');
%                     datestr(sel_time_range)
                    mindist = (limx(2)-limx(1))*0.01;
                    
                    % Verifica si se esta seleccionado en el limite del
                    % axis. Trata de conseguir extender la se�al.
                    if ~clickup && (sel_time_range(1)-limx(1)<mindist || ...
                            limx(2)-sel_time_range(2)<mindist)
%                         disp(['ax pos: ' mat2str(get(h.selected_axes,'position'))]);
%                         disp(['mouse pos: ' mat2str(get(h.fig,'currentpoint'))]);
                        
                        if sel_time_range(1)-limx(1)<mindist
                           newlimx = limx-[datenum(0,0,0,0,5,0) 0];
                           setSignalLimits(newlimx);
                           ud.scf = '';
                           set(h.selected_axes,'userdata',ud);
                        else
                            if limx(2)-sel_time_range(2)<mindist
                                newlimx = limx+[0 datenum(0,0,0,0,5,0)];
                                setSignalLimits(newlimx);
                                ud.scf = '';
                           set(h.selected_axes,'userdata',ud);
                            end
                        end
                        
                    end
%                 else
%                 end
%             end
        catch
            %                         disp('Error: no existe el sliced plot');
        end
    end
end
display_state('Listo...');
end