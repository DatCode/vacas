%% Funcion para crear un axes
function addAxes(varargin)
global h;

%% Maneja entradas
%Crea el objeto capaz de manejar varargin
p = inputParser;

%Se definen los valores por default
defaultparent = h.axes_panel;
fig_position = get(h.fig,'position');
pan_position = get(h.axes_panel,'position');

if(strcmpi(get(h.axes_panel,'units'),'normalized'))
    screensize = get(0,'screensize');
    fig_position=fig_position.*([screensize(3:4) screensize(3:4)]);
    pan_position = pan_position.*([fig_position(3:4) fig_position(3:4)]);
end

defaultsize = [pan_position(3)-40 100];
possiblepushdir = {'up','down'};
defaultpushdir = 'down';
defaultreference = 0;

if isfield(h,'ax')
    if ~isempty(h.ax)
        defaultreference = h.ax(end);
    else
        defaultreference = 0;
    end
end

%A�ade al esquema los argumentos chequeandolos
addOptional(p,'parent',defaultparent,@(x) length(x) == 1 && ishandle(x));
addOptional(p,'size',defaultsize,@(x) length(x)==2 && isnumeric(x));
addOptional(p,'pushdir',defaultpushdir,@(x) any(validatestring(x,possiblepushdir)));
addOptional(p,'reference',defaultreference,@(x) length(x) == 1 && ishandle(x));

%Realiza el parse
parse(p,varargin{:});

%Muestra resultados;
resultado = p.Results;

%% Obtiene la posicion de referencia
%Obtiene la posicion de la referencia
if resultado.reference == 0 || ~ishandle(resultado.reference)
    refpos = [0 0 pan_position(3:4)];
    resultado.reference = 0;
else
    refpos = get(resultado.reference,'position');
end

%% Define constantes
%Define el espacio entre cada elemento
distance = 50;
%Define margen top
ax_top = distance;

%% A�ade el axes y realiza el push a los demas axes
% set(ax,'userdata',userdata);
%Caso en que sea el primer grafico
if refpos(2)==0
    if isfield(h,'ax')
        idx = length(h.ax)+1;
    else
        idx = 1;
    end
    %Calcula punto y
    axy = pan_position(4)-ax_top-resultado.size(2);
    axx = 50;
    
    if(2*axx+resultado.size(1)>pan_position(3))
        resultado.size(1) = pan_position(3)-2*axx;
    end
    
    h.ax(idx)=axes('parent',resultado.parent,'units','pixels','position',...
        [axx axy resultado.size(1) resultado.size(2)],'buttondownfcn',{@ax_clicked});
else
    %Reemplaza en posicion al axes de referencia
    h.ax(end+1)=axes('parent',resultado.parent,'units','pixels','position',...
        refpos,'buttondownfcn',{@ax_clicked});
    
    %caso en que se desee el siguiente grafico desplace a la referencia
    if strcmpi(resultado.pushdir,'up')
        pushdown(resultado.reference,'this',distance);
        %caso en que se desee el siguiente grafico abajo de la referencia
    else
        pushdown(resultado.reference,'next',distance);
    end
end

userdata.estacion = '';
userdata.display = 'filtered_signal';
userdata.event_display = 'true';
userdata.scf = '';
userdata.zoom = 'off';
userdata.filtername = '';
userdata.estacion = '';
userdata.componente = '';
userdata.crosshairline = -1;
userdata.markvisibility = 'on';
userdata.originalsignalvisibility = 'off';
userdata.marklines = [];
userdata.linked_ax = h.ax(end);
userdata.kconver = 1;
userdata.events={};
userdata.request=[];
userdata.id_montaje = [];
userdata.id_predial=[];
userdata.data=[];
userdata.coly=[];
set(h.ax(end),'userdata',userdata);
end