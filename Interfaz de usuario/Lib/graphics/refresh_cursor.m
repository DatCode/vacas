function ax=refresh_cursor()
global h;
global flag;
global timers;
if isfield(h,'ax')
    ax = is_hover_an_axes(h.selected_axes);
    if ishandle(ax)
%         disp('sobre un axes seleccionado');
        if flag.choosing_axes || flag.choosing_sync_axes
            set(h.fig,'pointer','hand');
        else
            if flag.selecting_signal
                set(h.fig,'pointer','crosshair');
                refresh_crosshair_line();
            else
                set(h.fig,'pointer','arrow');
            end
        end
    else
        button = is_hover_a_button();
        if isobject(button) || ishandle(button)
            set(h.fig,'pointer','hand');
%             if isempty(timers.tooltip)
%                 timers.tooltip = timer('TimerFcn', {@update_tooltip},...
%                     'StartDelay',1.5,...
%                     'ExecutionMode', 'singleShot',...
%                     'BusyMode','queue','userdata',button);
%                 start(timers.tooltip)
%                 disp('crea timer');
%             end

        else
            set(h.fig,'pointer','arrow');
        end
        
        ax = is_hover_an_axes();
        if ishandle(ax)
            if flag.choosing_axes || flag.choosing_sync_axes
                set(h.fig,'pointer','hand');
            else
                set(h.fig,'pointer','arrow');
            end
        end
    end
else
    ax = false;
    button = is_hover_a_button();
    if isobject(button) || ishandle(button)
        set(h.fig,'pointer','hand');
    else
        set(h.fig,'pointer','arrow');
    end
end
end

% function update_tooltip(varargin)
% global timers;
% global h;
% boton=timers.tooltip.userdata;
% cbtn = is_hover_a_button();
% if cbtn == boton
%     ud = get(boton,'userdata');
%     h.tooltip = uicontrol('style','text','string',ud.tooltip,'units','pixels','position',...
%         [get(h.fig,'currentpoint')-[0 10] 150 50]);
%     uistack(h.tooltip,'top');
% end
% end