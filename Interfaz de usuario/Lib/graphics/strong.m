function str = strong(str)
if ischar(str)
    str = ['<strong>' str '</strong>'];
end
end