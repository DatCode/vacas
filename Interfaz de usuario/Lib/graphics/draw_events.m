function [ handles_rectangles , eventos,handles_label_text] = draw_events (axes_handle,eventos,varargin)
%draw_events: Funcion creada para mostrar rectangulos donde exista un evento
%sismico, los eventos sismicos pueden ser 'VT', 'LP' o 'TR'.
%   SINTAXIS:
%     Tiempos: Ingresar una matr�z de tiempos de la forma [t1 t2; t3 t4;... ; t5 t6]
%     Etiqueta: Ingresar matr�z de etiquetas esta debe coincidir con el
%     largo vertical de la matr�z tiempo y debe ser del tipo str. ejemplo
%     ['VT' 'LP' ... 'TR']
%     yminimo: corresponde al valor minimo de la se�al completa.
%     alto: Corresponde al la distancia m�xima entre el minimo y el m�ximo
%     de la se�al completa.
%     handle_axes: manejador del axes donde se graficar�n los rectangulos.
%     user: lista de los usuarios que etiquetaron los eventos (debe ser del tipo cell).
%     user_show: lista de usuarios de los cuales se mostrar� las etiquetas (debe ser del tipo cell).
p = inputParser;
addParameter(p,'mode','general');
p.parse(varargin{:});
if ~(strcmpi(p.Results.mode,'general'))
    display_state('Dibujando eventos...');
end
copy_eventos = eventos;
eventos.tiempos = [eventos.inicio_senial eventos.fin_senial];
eventos.tipo = char(eventos.clase);
%% Configuraci�n principal
% colores para cada evento
cr =     [255 255 0]./255;   
co =     [255 0 0]./255;   
go =     [0 255 255]./255;   
mn =     [0.412, 0.412, 0.412];  
ot =     [.2 .2 .6];   
de =     [0.4941    0.1843    0.5569];
% transparency =  0.7;                  % nivel de transparencia
colores={cr co go mn ot de};

%% ObTCniendo informacion
YLim=get(axes_handle,'YLim');
ymin=min(YLim)*0.8;
ymax=max(YLim)*0.8;
T=eventos.tiempos;
E=eventos.tipo;

tipos = {'cr','co','go','mn','ot','de'};
try
    handles_rectangles =[];
    handles_label_text=[];
    eventos = [];
    for n=1:length(T(:,1))
        
        subplot(axes_handle);
        idx=find(strcmpi(tipos,E(n,:)));
%         handles_rect=patch('XData',[T(n,1) T(n,2) T(n,2) T(n,1)]...
%                 ,'YData',[ymin ymin ymax ymax]...
%                 ...%                 ,'FaceColor',VT_color,'EdgeColor',VT_color);
%                 ,'FaceColor',colores{idx},'EdgeColor',colores{idx}...
%                 ,'EdgeAlpha',transparency,'FaceAlpha',transparency,'parent',axes_handle); % Creando rectangulo
        handles_rect=stem(axes_handle,[T(n,1) T(n,1) T(n,2) T(n,2)],[ymin ymax ymax ymin],'color',colores{idx},'LineWidth',1.5);
        handle_label_text=text(T(n,1),ymax*.9,tipos{idx},...
            'HorizontalAlignment','right',...
            'BackgroundColor','g','parent',axes_handle);
        event=tipos{idx};
        
        handles_rectangles=[handles_rectangles;handles_rect];
        eventos=[eventos;event];
        handles_label_text = [handles_label_text handle_label_text];
        data = get_events_by_idx(copy_eventos,n);
        
        userdata=[];
        userdata.name = 'event';
        userdata.event = data;
        set(handles_rect,'userdata',userdata);
        
        ud = [];
        ud.name = 'text_label';
        set(handle_label_text,'userdata',ud);
    end
catch
end
if ~exist('handles_rectangles')
    estado=0;    % no se han deTCctado eventos dentro del rango de tiempo eseficicado.
    handles_rectangles=0;
    eventos=0;
else
    estado=1;    % se han deTCctado eventos dentro del rango de tiempo eseficicado.
end

% reorder_z_index(axes_handle);
end