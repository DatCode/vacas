function toggle_rect_events (axes_handle,option)
ud = get(axes_handle,'userdata');
if isfield(ud,'etiquetas')
evts = ud.etiquetas;
if nargin<2
   cellfun(@(e) e.toggle_visible(),evts,'uniformoutput',false);
else
    switch(option)
        case 'showall'
            cellfun(@(e) e.visible_on(),evts,'uniformoutput',false);
        case 'none'
            cellfun(@(e) e.visible_off(),evts,'uniformoutput',false);
    end
end
end
end