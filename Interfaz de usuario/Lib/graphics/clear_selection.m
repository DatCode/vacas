function clear_selection()
global h;
if valid_field(h,'selected_axes',0)
    ud = get(h.selected_axes,'userdata');
    linked = ud.linked_ax;
    for i = 1:length(linked)
        p=get_signal_plot(linked(i),'selected_signal');
        try
            delete(p);
        catch
        end
    end
    try 
        delete(h.rect); 
    catch
    end
    h.rect=[];
    h.selected_signal = 0;
end
end