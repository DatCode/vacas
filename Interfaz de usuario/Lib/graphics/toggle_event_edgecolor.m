function toggle_event_edgecolor()
global h;
% global colores;
global flag;

if valid_field(h,'selected_axes',0) && flag.choosing_axes
    %     try
    %     actual_color = get(h.selected_event,'edgecolor');
    %     if isequal(actual_color,colores.select_event_edge)
    %         set(h.selected_event,'edgecolor',get(h.selected_event,'facecolor'));
    %         set(h.selected_event,'linewidth',0.1);
    %         %     uistack(h.selected_event,'bottom');
    %         h.selected_event = [];
    %     else
    %         set(h.selected_event,'edgecolor',colores.select_event_edge);
    %         set(h.selected_event,'linewidth',3);
    %         %     uistack(h.selected_event,'top');
    %     end
    %     catch
    %         h.selected_event = 0;
    %     end
    try
%         ud = get(h.selected_axes,'userdata');
        label = h.selected_event{1};
        ud = get(h.selected_axes,'userdata');
        cellfun(@(x) x.unselect(),ud.etiquetas,'uniformoutput',false);
        label.toggle_select();
    catch
        h.selected_event = 0;
    end
end
end