function display_events_in_ax(ax,varargin)
global h;
if nargin>1
    op = varargin{1};
else
    op = 'default';
    if nargin==0
        ax = h.selected_axes;
    end
end
% if valid_field(h,'selected_axes')
    ud = get(ax,'userdata');
    if ~strcmpi(op,'refresh')
        if strcmpi(ud.event_display,'off')
            toggle_rect_events(ax,'showall');  
            ud.event_display = 'on';
            set(h.menu(2,8),'checked','on');
        else
            toggle_rect_events(ax,'none');
            ud.event_display = 'off';
            set(h.menu(2,8),'checked','off');
        end
    else
        if strcmpi(ud.event_display,'off')
            toggle_rect_events(ax,'none');
            set(h.menu(2,8),'checked','off');
        else
            toggle_rect_events(ax,'showall');
            set(h.menu(2,8),'checked','on');
        end
    end
    set(ax,'userdata',ud);
% end
end

