% test draw_spectrogram
close all
clear all
clc
x=0:0.001:10;
y=sin(x);
f=figure()
h=plot(x,y);

draw_spectrogram(h);