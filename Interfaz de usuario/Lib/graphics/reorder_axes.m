function reorder_axes()
global h;
%Define el espacio entre cada elemento
distance = 50;

%% A�ade el axes y realiza el push a los demas axes
%Caso en que sea el primer grafico
if isfield(h,'ax')
    largo = length(h.ax)+1;
else
    largo = 1;
end
largo=largo-1;
if largo>=1
    panel_pos = get(h.axes_panel,'position');
    first_pos = get(h.ax(1),'position');
    first_pos(2)=panel_pos(4)-first_pos(4)-distance;
    set(h.ax(1),'position',first_pos);
    
    for i=2:largo
        actual_pos = first_pos;
        actual_pos(2)=actual_pos(2)-(actual_pos(4)+distance)*(i-1);
        set(h.ax(i),'position',actual_pos);
    end
end
end

