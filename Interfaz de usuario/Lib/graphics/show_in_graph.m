function show_in_graph(names,ax)
if nargin <2
    global h;
    ax = h.selected_axes;
end
clear_axes(ax);
ch = get(ax,'children');
userdatas = get(ch,'userdata');
p=[];
for i =1:length(ch)
    if valid_field(userdatas{i},'name','')
        if sum(strcmpi(names,userdatas{i}.name))>0
            set(ch(i),'visible','on');
            type = get(ch(i),'type');
            if strcmpi(type,'line')
                p=[p;ch(i)];
            end
        else
            set(ch(i),'visible','off');
        end
    end
end
if ~isempty(p)
    ud = get(p(1),'userdata');
    if ~strcmpi(ud.name,'espectro')
        
        if length(p)<2
            try
                remember_limits(p);
            catch
            end
        else
            if length(p)==2
                remember_limits(p(1),p(2));
            end
        end
        toggle_rect_events(ax,'showall');
    else
        remember_limits(p(1));
        toggle_rect_events(ax);
    end
end
end

