function select_counter_origin( varargin )
global h;
evt_struct =varargin{2};
old_value = evt_struct.OldValue;
new_value = evt_struct.NewValue;

seleccion = get(new_value,'string');
if strcmpi(seleccion,'Lista eventos')
    h.counter_object = h.event_listbox;
else
    if valid_field(h,'selected_axes',0)
        h.counter_object = h.selected_axes;
    else
        h.counter_object = 0;
    end
end
refresh_counters();
end

