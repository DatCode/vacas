function repaintEventsInGraph(ax)
ud = get(ax,'userdata');
if valid_field(ud,'etiquetas')
    cellfun(@(x) x.repaint(),ud.etiquetas,'uniformoutput',false);
    cellfun(@(x) x.unselect(),ud.etiquetas,'uniformoutput',false);
end
end