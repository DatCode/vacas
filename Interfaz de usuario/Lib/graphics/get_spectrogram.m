function handle = get_spectrogram(ax)
if nargin<1
   global h;
   ax=h.selected_axes;
  
end

ch=get(ax,'children');
types =get(ch,'type');
idx =find(strcmpi(types,'image'));
ch=ch(idx);
if length(ch)>1
   for i=i:length(ch)
      userdata =get(ch,'userdata');
      if strmpi(userdata.name,'espectrograma')
          handle = ch(i);
          break;
      end
   end
else
    handle = ch;
end
end

