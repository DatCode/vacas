%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function joinPlots(p1,p2)
    x1 = get(p1,'xdata');
    x2 = get(p2,'xdata');
    y1 = get(p1,'ydata');
    y2 = get(p2,'ydata');
    
    idx = find(isnan(x1));
    x1(idx)=x2(2:end-1);
    y1(idx)=y2(2:end-1);
    delete(p2);
    set(p1,'xdata',x1);
    set(p1,'ydata',y1);
end