%Funcion que anima al boton al ser seleccionado
function animate_button(img)
%Pregunta si ya se esta animando el boton o no
src = get(img,'parent');
clicked = get(src,'userdata');

if isempty(clicked)
    clicked = 0;
end

%En caso de que no se este animando actualmente
if clicked == 0
    %Avisa que ahora si se encuentra animandose
    clicked = 1;
    set(src,'userdata',clicked);
    
    %Define colores de seleccion y desseleccion
    nonsel_color = [0.5 0.5 0.5];
    sel_color = 'black';
    
    %Implementa color de seleccion
    %     box(src);
    try
        set(src,'xcolor',sel_color,'ycolor',sel_color);
    catch
    end
    %Define valores para la animacion
    pos_ini = get_position(img); %Posicion inicial del elemento
    pixels = 1; %Rango de pixeles a agrandar y achicar
    nmovs = 10;%Cantidad de frames
    time = 0.05;%Tiempo para llevarlo a cabo
    ts = time/nmovs;%Tiempo por frame
    
    pos = pos_ini;%Inicializa la posicion
    
    %Realiza la animacion de enpeque�ecimiento
    for i=1:nmovs
        pix = pixels/nmovs*i;
        pos = pos - [-pix/2 -pix/2 pix pix]; %Achica el boton
        set(img,'position',pos);
        pause(ts);
    end
    
    %Realiza la animacion de agrandamiento
    for i = 1:nmovs
        pix = pixels/nmovs*i;
        pos = pos + [-pix/2 -pix/2 pix pix];%Agranda el boton
        set(img,'position',pos);
        pause(ts);
    end
    
    %Implementa color de deseleccion
    %     box(src,'off');
    try
        set(src,'xcolor',nonsel_color,'ycolor',nonsel_color);
    catch
    end
    %Avisa que se deja de ocupar la animacion
    clicked = 0;
    set(src,'userdata',clicked);
end
end

%% Funciones adicionales
function height=get_height(handle)
pos = get_position(handle);
height = pos(4);
end

function width = get_width(handle)
pos = get_position(handle);
width = pos(3);
end

function position = get_position(handle)
position = get(handle,'position');
end

function flag=isbetween(a,b,c)
    if(a<=b && a>=c)
        flag = 1;
    else
       if a>=b && a<=c
           flag = 1;
       else
           flag = 0;
       end
    end
end