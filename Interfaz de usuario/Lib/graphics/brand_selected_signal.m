%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function new_p =  brand_selected_signal(p,sel_time_range,color,p2)
global h;
xdata = get(p,'xdata');
xdata2=[];
if nargin>3
    xdata2 = get(p2,'xdata');
    xdata(isnan(xdata)) = xdata2(2:end-1);
end
if sel_time_range(1) == sel_time_range(2)
    largo = length(xdata);
    if largo>2
        sel_time_range(1) = sel_time_range(1)-1;
        sel_time_range(2) = sel_time_range(2)+1;
    end
end
idx = find(xdata>=sel_time_range(1) & xdata<=sel_time_range(2));
x = xdata(idx);

if isempty(idx) && (isbetween(sel_time_range(1),xdata(1),xdata(end)) || ...
        isbetween(sel_time_range(1),xdata(1),xdata(end)))
%     disp('Brand_selected_signal(): Empty idx');
    idx1 = find(abs(xdata-sel_time_range(1))<=(xdata(2)-xdata(1))/2);
    idx1=idx1(1);
    
    idx2 = find(abs(xdata-sel_time_range(2))<=(xdata(2)-xdata(1))/2);
    idx2=idx2(1);
    
    if idx1 == idx2
       idx = [idx1 idx2]; 
    else
        idx = idx1;
    end
    
    x = xdata(idx);
end

new_p = [];
try
    if(nargin<4)
        %new_p = slicePlot(p,x(1),x(end),color);
%         slicePlot(p,x(1),x(end),color);
        slice_linked_ax_plot(h.selected_axes,x(1),x(end),color);
    else
%         slicePlot(p,x(1),x(end),color,p2);
% slice_linked_ax_plot(h.selected_axes,ti,tf,color)
    end
catch
%     disp('brand_selected_signal(): Error: no se ha podido realizar slice');
end
pause(0.01);
end

function slice_linked_ax_plot(ax,ti,tf,color)
ud = get(ax,'userdata');
linked = ud.linked_ax;
for i = 1:length(linked)
   p = get_signal_plot(linked(i),'current');
   slicePlot(p,ti,tf,color);
end
end