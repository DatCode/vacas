function delete_marks(varargin)
global h;
if valid_field(h,'selected_axes',0)
   ud = get(h.selected_axes,'userdata');
   marklines = ud.marklines;
   arrayfun(@(x) delete(x),marklines,'UniformOutput',false);
   ud.marklines = [];
   set(h.selected_axes,'userdata',ud);
end
end

