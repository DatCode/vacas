function toggle_marks( varargin )
global h;
if valid_field(h,'selected_axes',0)
   ud = get(h.selected_axes,'userdata');
   if strcmpi(ud.markvisibility,'on');
       set(ud.marklines,'visible','off');
       ud.markvisibility = 'off';
       set(h.selected_axes,'userdata',ud);
       set(h.menu(2,7),'checked','off');
   else
       set(ud.marklines,'visible','on');
       set(h.menu(2,7),'checked','on');
       ud.markvisibility = 'on';
       set(h.selected_axes,'userdata',ud);
   end
end
end

