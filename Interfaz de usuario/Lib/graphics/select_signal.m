%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function [varargout]=select_signal(varargin)
%% function [rect,ax_range]=select_signal(varargin)
% Resumen: Esta funcion permite seleccionar, a base de una seleccion 
% rectangular con el mouse, un trozo de se�al, y  devuelve como 
% argumento de salida el rango de valores en el vector tiempo, es 
% decir, inicio y final.
% 
% Entradas: 
%   ax                     :    Handle del axes seleccionado.
%   sp                     :    Start Point, punto donde comienza el drag. Este punto es
%                                   con referencia a la ventana de la figura.
%   rect                  :    Handle del rectangulo de seleccion, si es que
%                                   existe.
%
% Salidas:
%   rect                   :    Handle del rectangulo dibujado.
%   sel_time_range :    Rango de tiempo en que se encuentra la se�al
%                                   seleccionada. En caso de existir solo un
%                                   argumento de salida, se utilizar� este.

%% MANEJA LA ENTRADA
global h;
if nargin<2
%     global h;
    global drag;
    sp = drag.start;
    ax = h.ax;
    rect = h.rect;
else
    if nargin <3
        ax = varargin{1};
        sp = varargin{2};
        rect = [];
    else
        ax = varargin{1};
        sp = varargin{2};
        rect = varargin{3};
    end
end
%% OBTIENE PUNTO INICIAL Y ACTUAL DEL PUNTERO EN DRAGGING
        %Obtiene el la ubicacion del puntero
        cp = currentpoint(ax);%Referencia: ax
        el_pos = get(ax,'position');%Obtiene la posicion del axes
%         el_pos = absolute_position(ax);
        
        %Obtiene el punto de comienzo del drag con referencia al axes
        sp = abstorelpoint(sp,ax);
        
        %% OBTIENE EL PUNTO DONDE COMIENZA A DIBUJARSE EL RECTANGULO
        if(cp(1)>sp(1))
            x= sp(1);
        else
            x = cp(1);
        end
        
        if(cp(2)>sp(2))
            y=sp(2);
        else
            y=cp(2);
        end
        
        %% OBTIENE EL ANCHO Y EL ALTO DEL RECTANGULO A DIBUJAR
        
        height = abs(cp(2)-sp(2));
        width = abs(cp(1)-sp(1));
        
        if width ==0
            width = 1;
        end
        
        if height == 0
            height = 1;
        end
        
        %% OBTIENE LOS LIMITES DEL AXES Y LA DISTANCIA ENTRE ESTOS
        xrange = get(ax,'xlim');
        yrange = get(ax,'ylim');
        xs = xrange(1);
        xdist = xrange(end)-xrange(1);
        ydist = yrange(end)-yrange(1);
        ys = yrange(1);
        
        %% PASA DE PIXELES A PUNTOS DEL AXES, 
        % CALCULA LA POSICION DEL RECTANGULO Y LO DIBUJA
%         [x y width height]
        rect_pos = [x y width height]./[el_pos(3:4) el_pos(3:4)]...
            .*[xdist ydist xdist ydist]+[xs ys 0 0];
        
        if isempty(rect)
%             disp('Se crea un rectangulo');
            rect = rectangle();
            set(rect,'position',rect_pos,'linestyle','--','edgecolor','black','linewidth',1,'parent',ax);
        else
            set(rect,'position',rect_pos);
        end
        
        %% OBTIENE EL RANGO DE TIEMPO EN QUE SE ENCUENTRA LA SELECCION
%         h_plot = findfirstchild(ax,'type','line');
%         h_plot = h.p(1);
%         xdata = get(h_plot,'xdata');
%         largo = length(xdata);
%         pix_range = [x x+width];
%         ratio_range = pix_range./[el_pos(3) el_pos(3)];
%         aprox_index_range = ratio_range*largo;
%         time_range_idx = [floor(aprox_index_range(1)) ceil(aprox_index_range(2))];
%         time_range_idx(time_range_idx==0)=1;
        sel_time_range=[rect_pos(1) rect_pos(3)+rect_pos(1)];
        
        %% MANEJA SALIDA
        if nargout<2
            varargout = {sel_time_range};
        else
            varargout = {rect,sel_time_range};
        end
end