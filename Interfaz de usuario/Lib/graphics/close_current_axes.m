function close_current_axes( varargin)
global h;
if valid_field(h,'selected_axes',0)
   if h.selected_axes~=0
      clear_counters();
      clear_selection();
      clear_dynamic_event();
      unlink(h.selected_axes);
      h.selected_event=0;
      h.ax(h.ax==h.selected_axes)=[];
      delete(h.selected_axes);
      h.selected_axes=0;
      reorder_axes();
      refresh_counters();
      refresh_fast_buttons_color();
      
      for i = 1:length(h.ax)
          checknewaxesbounds(h.ax(i))
      end
   end
end
end

function unlink(ax)
ud = get(ax,'userdata');
linked = ud.linked_ax;
linked = linked(linked~=ax);
for i = 1:length(linked)
   act_ud = get(linked(i),'userdata');
   act_ud.linked_ax = act_ud.linked_ax(act_ud.linked_ax~=ax);
   set(linked,'userdata',act_ud);
end
end