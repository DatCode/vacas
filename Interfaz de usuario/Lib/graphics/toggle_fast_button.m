function toggle_fast_button(button)
pause(0.1);
sel_color = [1 0 0];
non_sel = [0 0 0];
% ax=get(button,'parent');
ud = get(button,'userdata');

%Setea los demas botones pertenecientes al mismo grupo a off
btns_from_group = get_group_of_btns(ud.group);
bad_buttons = btns_from_group(btns_from_group~=button);
bad_bounds = get(bad_buttons,'parent');
try
cellfun(@(x) set(x,'ycolor',non_sel,'xcolor',non_sel,'box','off'),bad_bounds);
catch
    set(bad_bounds,'ycolor',non_sel,'xcolor',non_sel,'box','off')
end

bounds = get(button,'parent');
bx = get(bounds,'box');
if strcmpi(bx,'on')
    action = 'off';
    color = non_sel;
else
    action = 'on';
    color = sel_color;
end
set(bounds,'ycolor',color,'xcolor',color,'box',action)
% set(ax,'ycolor',sel_color,'xcolor',sel_color,'box','on');
end


function btns = get_group_of_btns(group)
global h;
uds = get(h.fast_button,'userdata');
btns = [];
if group>0
    for i=1:length(uds)
        
        if uds{i}.group==group
            btns = [btns; h.fast_button(i)];
        end        
    end
end
end

function groups = get_array_of_groups()
global h;

groups = [];
uds = get(h.fast_button,'userdata');
for i = 1: length(uds)
    groups = [groups;uds.group];
end

groups = unique(groups);
groups = groups(groups~=0);
end