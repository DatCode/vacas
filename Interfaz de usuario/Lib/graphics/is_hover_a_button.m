function flag=is_hover_a_button(button)
global h;
if nargin<1
    button = -1;
end
flag = false;
if isfield(h,'fast_button')
    for i = 1: length(h.fast_button)
        marco = get(h.fast_button(i),'parent');
        
        if ishover(h.fig,marco)
            if ishandle(button)
                if button == h.fast_button(i)
                    flag = h.fast_button(i);
                    break;
                end
            else
                flag = h.fast_button(i);
                break;
            end
        end
    end
end
end