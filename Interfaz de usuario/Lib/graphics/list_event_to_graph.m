function list_event_to_graph(varargin)
global h;
if ~valid_field(h,'view_axes')
    h.view_axes = [];
end
if ~ ishandle(h.view_axes)
   h.view_axes=[]; 
end

%Obtiene el evento seleccionado entre la lista de eventos
value = get(h.event_listbox,'value');
userdata = get(h.event_listbox,'userdata');
eventos = userdata.filtered_events;
evento = get_events_by_idx(eventos,value);

%Grafica el evento
h.view_axes=display_event_in_graph(evento,h.view_axes);

ud = get(h.view_axes,'userdata');
ud.purpose = 'event_view';
set(h.view_axes,'userdata',ud);
end