function clear_axes(ax)
global h;
if nargin<1
    ax = h.selected_axes;
end

ch = get(ax,'children');
set(ch,'visible','off');
end