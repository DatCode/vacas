function clear_counters()
global h;

for i=1:length(h.event_count)
    set(h.event_count(i),'string',0);
end
end