function toggle_sel_axes(ax,options)
global colores;
global flag;
global h;
warning off;
pan = get(ax,'parent');
ax_anterior = get(pan,'userdata');
set(pan,'userdata',ax);
if nargin<2
sel_color = colores.selected_axes;
else
    if strcmpi(options.mode,'sync')
        sel_color = colores.sync_axes;        
    end
end
nonsel_color = 'black';
current_color = get(ax,'ycolor');
alreadyselected = and(strcmpi(get(ax,'box'),'on'),sum(get(ax,'xcolor')==sel_color)==length(sel_color));
box(ax,'on');
if ax~=h.selected_axes
    ismainax=false;
else
    ismainax=true;
end
desicion = 'connect';
if ~isempty(ax_anterior)
    if ax_anterior==ax
        %         isbox = get(ax,'box');
        
        if ~isequal(current_color,[0 0 0]);
            %             box(ax,'off');
            if ~(ismainax&&flag.choosing_sync_axes)
                set(ax,'xcolor',nonsel_color,'ycolor',nonsel_color);
            end
            if ~ismainax
                desicion = 'disconnect';
            end
        else
            %             box(ax);
            set(ax,'xcolor',sel_color,'ycolor',sel_color);
        end
    else
        set(ax,'ycolor',sel_color,'xcolor',sel_color);
        %         box(ax_anterior,'off');
        if ishandle(ax_anterior) && ~(ismainax&&flag.choosing_sync_axes)
            if nargin<2
                set(ax_anterior,'xcolor',nonsel_color,'ycolor',nonsel_color);
                desicion = 'disconnect';
            else
                if ~strcmpi(options.mode,'sync')
                    set(ax_anterior,'xcolor',nonsel_color,'ycolor',nonsel_color);
                    desicion = 'disconnect';
                else
                    if alreadyselected
                       set(ax,'xcolor',nonsel_color,'ycolor',nonsel_color);
                       desicion = 'disconnect';
                    end
                end
            end
        end
        %         box(ax);
        
    end
else
    %     box(ax);
    set(ax,'ycolor',sel_color,'xcolor',sel_color);
    desicion = 'connect';
end
link(ax,desicion);
warning on;
end

function link(ax,option)
global h;
global flag;
if flag.choosing_sync_axes
    ud = get(h.selected_axes,'userdata');
    ud.linked_ax = unique(ud.linked_ax);
    if strcmpi(option,'connect')
        ud.linked_ax = [ud.linked_ax;ax];
    else
        if strcmpi(option,'disconnect') && ax~=h.selected_axes
            ud.linked_ax = ud.linked_ax(ud.linked_ax~=ax);
            udax = get(ax,'userdata');
            udax.linked_ax = ax;
            set(ax,'userdata',udax);
        end
    end
    for i = 1:length(ud.linked_ax)
        actud =get(ud.linked_ax(i),'userdata');
        actud.linked_ax = ud.linked_ax;
        set(ud.linked_ax(i),'userdata',actud);
    end
end
end