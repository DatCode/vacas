function hex_value = rgb_to_hex(rgb_value)
hex_value = [dec2hex(floor(rgb_value(1)*255)) dec2hex(floor(rgb_value(2)*255))  dec2hex(floor(rgb_value(3)*255))] ;
if length(hex_value)<6
    for j=1:6-length(hex_value)
        hex_value = [hex_value '0'];
    end
end
end