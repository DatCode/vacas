function p2 = draw_spect(plot_handle)
%% draw_spect: Grafica el espectro de una se�al ya graficada en otro plot.
%
%sintaxis:
% varargout = draw_spect(plot_handle)
%
%
%  Input:
%   - plot_handle: manejador del plot donde se grafic� la se�al
%  Output:
%   - varargout: si no existe el eje handles.spectro, se crea una nueva
%     ventana y se entrega el manejador del nuevo plot donde se grafic� el
%     espectro.


% configuraci�n...
%     manejador='handles';
%     eval(['global ' manejador ';']);
%     nombre_axis=[manejador '.spectro'];
    global colores;
ax = get(plot_handle,'parent');
% obteniendo informacion de la se�al
    try
%         tiempo=get(plot_handle,'XData');    % obtiene el vector de tiempo del plot_handle (eje x).
%         data=get(plot_handle,'YData');      % obtiene el vector de datos del plot_handle (eje y).
datos = get_limited_data(plot_handle);
tiempo = datos.xdata;
data = datos.ydata;
        if (isempty(data)||isempty(tiempo)) % si el plot padre est� vacio...
            warning('Espectro: No existen datos en el plot se�alado');
            return;
        end
    catch               % si no se logra obtener informaci�n.
        warning('Espectro: No se ha logrado obtener informaci�n');
        return
    end

% Calculando espectro
    Fs=100;                % Frecuencia de muestreo fija en 100 Hz
    L=length(data);        % largo del vector f (se�al full)
    N=2^nextpow2(L);       % Numero de puntos que contendr� la fft
    z=fft(data,N)/Fs;      % estimando fft normalizada por la frecuencia de muestreo
    z=z.*conj(z);          % espectro de magnitud
    z=z(1:N/2);            % seleccionando la mitad del especto
%     z=10*log(z);
    frec=linspace(0,Fs/2,N/2);  % creando vector de frecuencias
    
% graficando espectro
%     if exist(nombre_axis,'var')                 % existe un eje para el esoectro???
%         eval(['plot(' nombre_axis ',frec,z);']) % Grafica espectro en el eje
signal_userdata = get(plot_handle,'userdata');
p2=plot(ax,frec,z,'color',colores.spectre);
userdata.name = 'espectro';
userdata.refer_plot = signal_userdata.name;
set(p2,'userdata',userdata);
clear_axes();

show_in_graph({'espectro'});
xlim(ax,[0 20]);
xticks = 0:5:50;
set(ax,'xtick',xticks,'xticklabel',xticks);
%     else
%         figure();                           % crea una figura nueva entega el manejador del eje.
%         h=plot(frec,z);
%         varargout={h};
%     end
end