%CopyRight (c), 2015 Jose Martinez Lavados
function display_state_gral(handle,state,position)

if nargin<1
   state = 'Listo...'; 
end

labelStr = ['<HTML><FONT>' state '</Font></html>'];
jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
dim_label = position;

g_handles.state = labelStr;
g_handles.state_label = javacomponent(jLabel,dim_label,handle);
pause(0.001);
end