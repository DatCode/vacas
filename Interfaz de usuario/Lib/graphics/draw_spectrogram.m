function varargout = draw_spectrogram(plot_handle)
%% draw_spect: Grafica el espectro de una se�al ya graficada en otro plot.
%
%sintaxis:
% varargout = draw_spect(plot_handle)
%
%
%  Input:
%   - plot_handle: manejador del plot donde se grafic� la se�al
%  Output:
%   - varargout: si no existe el eje handles.spectrogram, se crea una nueva
%     ventana y se entrega el manejador del nuevo plot donde se grafic� el
%     espectro.

% configuraci�n...
%     manejador='handles';
%     eval(['global ' manejador ';']);
%     nombre_axis=[manejador '.spectrogram'];
    display_state('Dibujando espectrograma...');
% obteniendo informacion de la se�al
    try
        tiempo=get(plot_handle,'XData');    % obtiene el vector de tiempo del plot_handle (eje x).
        data=get(plot_handle,'YData');      % obtiene el vector de datos del plot_handle (eje y).
        if (isempty(data)||isempty(tiempo)) % si el plot padre est� vacio...
            warning('Espectrograma: No existen datos en el plot se�alado');
            return;
        end
    catch               % si no se logra obtener informaci�n.
        warning('Espectrograma: No se ha logrado obtener informaci�n');
        return
    end

% Calculando espectrograma
    [spect_S,spect_F,~,~]=spectrogram(data,...
                                    hamming(300),... %window
                                             295,... %n overlap 295
                                             512,... %nfft
                                             100,... %fs (fs fija)
                                        'yaxis');
    DEP_Sdb=10*log10(spect_S.*conj(spect_S));
    global h;
% graficando espectrograma
%     if exist(nombre_axis,'var')   % existe un eje para el esoectrograma???
%         eval(['subplot(' nombre_axis ');']);     % Grafica espectrograma en el eje
        imagesc(tiempo,spect_F,DEP_Sdb,'parent',h.selected_axes);
        set(h.selected_axes,'ylim',[min(spect_F) max(spect_F)]);
%         axis xy;
%     else
%         figure();                           % crea una figura nueva entega el manejador del eje.
%         h=imagesc(tiempo,spect_F,DEP_Sdb);
%         axis xy;
%         varargout={h};
%     end
end