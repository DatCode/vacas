function takeAStep(ax,sentido)
global h;
if nargin<3
    ax = h.selected_axes;
end
try
    normal_zoom;
end
if ishandle(ax) || isobject(ax)
    type = get(ax,'type');
    if strcmpi(type,'axes')
        ud = get(ax,'userdata');
        if ~strcmpi(ud.display,'spectre')
            linked = unique(ud.linked_ax);
            ud.linked_ax = linked;
            set(ax,'userdata',ud);
            for i = 1:length(linked)
                stepOneGraph(linked(i),sentido);
            end
            display_state('Listo...');
        end
    end
    refresh_counters
end
end

function stepOneGraph(ax,sentido)
ud = get(ax,'userdata');
% timetostep = datenum(0,0,0,0,1,0);

%plot datos filtrados
p_filt = get_signal_plot(ax,'filtered_signal');
xdata = get(p_filt,'xdata');
ydata = get(p_filt,'ydata');
dx = xdata(end)- xdata(1);
timetostep = dx*0.3;
if strcmpi(sentido,'forward')
    newlimx = [xdata(end) xdata(end)+timetostep];
    idx = xdata>=newlimx(1)+timetostep-dx;
elseif strcmpi(sentido,'back')
    newlimx = [xdata(1)-timetostep xdata(1)];
    idx = xdata<=newlimx(2)+dx-timetostep;
end
xdata = xdata(idx);
ydata = ydata(idx);

%plot datos originales
p_ori = get_signal_plot(ax,'original_signal');
xdata2 = get(p_ori,'xdata');
ydata2 = get(p_ori,'ydata');
idx2 = xdata2>=newlimx(1);
xdata2 = xdata2(idx2);
ydata2 = ydata2(idx2);

%Recorte de datos pedido
display_state('Buscando senial faltante...');
ud.request.range_x=newlimx;
datos=get_signal(ud.request);
senial = center_signal(datos.(ud.request.coly));
tiempo = datos.(ud.request.colx{1});
filt_senial = filter_signal(senial,ud.filtername);

%Actualizacion de los plots
if strcmpi(sentido,'forward')
    xdata = [xdata(2:end) tiempo];
    ydata = [ydata(2:end) filt_senial'];
    
    xdata2 = [xdata2(2:end) tiempo];
    ydata2 = [ydata2(2:end) senial'];
elseif strcmpi(sentido,'back')
    xdata = [tiempo xdata(2:end)];
    ydata = [filt_senial' ydata(2:end)];
    
    xdata2 = [tiempo xdata2(2:end)];
    ydata2 = [senial' ydata2(2:end)];
end

set(p_filt,'xdata',xdata,'ydata',ydata);
set(p_ori,'xdata',xdata2,'ydata',ydata2);


if strcmpi(ud.event_display,'true')
    refresh_events_in_graph(ax)
end
if strcmpi(ud.display,'spectrogram')
    refresh_spectrogram(ax)
else
    remember_limits(p_filt);
    display_events_in_ax(ax,'refresh')
end

ud.request.range_x=[xdata(1) xdata(end)];
set(ax,'userdata',ud);

refresh_filtername_text(ax)
refresh_date_xtick(ax)
%    display_state('Listo...');
end

function refresh_spectrogram(ax)
s = get_spectrogram(ax);
sud = get(s,'userdata');
% global h;

p = get_signal_plot(ax,sud.refer_plot);
senal = get(p,'ydata');
t = get(p,'xdata');

%Calcula el espectrograma
userdata = get(ax,'userdata');
kconver = userdata.kconver;

%     idx = senal~=0;
%     t=t(idx);
%     senal = senal(idx);%*kconver;
overlap = 295;
nfft = 512;
fs = get_fsamp(t(1));
window = 300;
[spect_S,spect_F,~,~]=spectrogram(senal,...
    hamming(window),... %window 300
    overlap,... %n overlap 295
    nfft,... %nfft 1024
    fs,... %fs (fs fija) 100
    'yaxis');
DEP_Sdb=10*log10(spect_S.*conj(spect_S));
set(s,'xdata',t,'ydata',spect_F,'cdata',DEP_Sdb);

rango_caxis = [50 100];
caxis(ax,rango_caxis+20.*log10(kconver));%Color axis rango 50-100 db
ylim(ax,[0 20]);% Limita 0 - 20Hz
xlim(ax,[t(1) t(end)]);
end