function ax=sql_plot(input)
global h;
%Para pruebas !!
if nargin<1
    input.componente = 'z';input.ti = datenum(2014,02,05,20,10,0);input.tf = datenum(2014,02,05,20,20,0);
    input.estacion = 'lav';
end
global selected_filter;
try
    display_state('Buscando senial en el servidor...');
    predial = getPredialFromRequest(input);
    datos = sel_signal(input.range_x(1),input.range_x(2),predial,input.sensor);
    if ~isfield(datos,'eje_x')
        display_state('No se ha encontrado senial...');
        error('No hay datos');
    end
%     datos = SQLdata(input.ti,input.tf,'conversion','um/s','componente',input.componente,'estacion',input.estacion);
    %     datos=SQLdata(ti,tf,'conversion','um/s');%,'componente',output.componente,'estacion',output.estacion);
catch
    datos = false;
    display_state('No se ha encontrado senial...');
end
ax = [];
in_display_event_axes = false;
if isobject(datos) || isstruct(datos) 
    %Grafica la senial
    senial = center_signal(datos.(input.coly));
    tiempo = datos.(input.colx{1});
    id_montaje = datos.id_montaje;
    if valid_field(input,'mode','')
        if strcmpi(input.mode,'replace') && valid_field(h,'display_event_axes',0)
            ax = h.display_event_axes;
            in_display_event_axes=true;
        end
    end
    
    if ~isempty(ax)
        options.ax = ax;
        %         disp('plotea')
        ax=plot_signal(tiempo,senial,options);
    else
        %         disp('plotea')
        ax=plot_signal(tiempo,senial);
        if in_display_event_axes
            h.display_event_axes = ax;
        end
    end
    %A�ade uicontextmenu para cambiar de eje
    c=uicontextmenu('parent',h.fig);
    set(ax,'uicontextmenu',c);
    campos = fields(datos);
    for i = 1:length(campos)-2
        m=uimenu('Parent',c,'Label',campos{i},'callback',{@selAxis,ax,campos{i}});
        if strcmpi(campos{i},input.coly)
           set(m,'checked','on'); 
        end
    end
    
    %Avisa que estacion se esta observando
    userdata = get(ax,'userdata');
    %     userdata.estacion = input.estacion;
    userdata.display = ['filtered_' input.coly];
    userdata.event_display = 'true';
    userdata.scf = '';
    userdata.zoom = 'off';
    userdata.filtername = selected_filter;
%     userdata.estacion = lower(input.estacion);
%     userdata.componente = lower(input.componente);
    userdata.crosshairline = -1;
    userdata.request=input;
    userdata.id_montaje = id_montaje;
    userdata.predial = predial;
    userdata.datos = datos;
    userdata.eje=input.coly;
    userdata.sensor = input.sensor;
%     userdata.kconver = datos.kconver;
    
    set(ax,'userdata',userdata);
    
    %Grafica los eventos
    refresh_events_in_graph(ax);
    
%     Refresca los contadores
    refresh_counters();
    refresh_filtername_text(ax);
    display_events_in_ax(ax,'refresh')
end

display_state('Listo...');
end

