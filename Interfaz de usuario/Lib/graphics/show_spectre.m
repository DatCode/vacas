function show_spectre(varargin)
global h;
if isfield(h,'selected_axes')
    
    if ishandle(h.selected_axes) || isobject(h.selected_axes)
        
        try
            delete(h.selected_signal);
        catch
        end
        if isnumeric(h.selected_axes)
            if h.selected_axes==0
                return;
            end
        end
        set(get_signal_plot(h.selected_axes,'selected_signal'),'visible','off');
        %         refresh_fast_button(button);
        p = get_signal_plot(h.selected_axes,'current');
        s = get_signal_plot(h.selected_axes,'espectro');
        %         p_userdata = get(p,'userdata');
        s_userdata = get(s,'userdata');
        
        if isempty(p)
            p = get_signal_plot(h.selected_axes,'filtered_signal');
        end
        
        if ~isempty(p)
            p = p(end);
            p_userdata = get(p,'userdata');
            if sum(strcmpi({'spectre','spectrogram'},p_userdata.name))>0
                if ~isempty(s)
                    p = get_signal_plot(h.selected_axes,s_userdata.refer_plot);
                end
            end
        else
            p = get_signal_plot(h.selected_axes,'filtered_signal');
        end
        userdata = get(h.selected_axes,'userdata');
        
        limx = [0 0];
        if isempty(s)            
            limx = get(h.selected_axes,'xlim');
        end
        
        if ~isempty(s)
            
            p = get_signal_plot(h.selected_axes,s_userdata.refer_plot);
            if isempty(p)
                name = 'filtered_signal';
            else
                p_userdata = get(p,'userdata');
                name = p_userdata.name;
            end
            
            visible = get(s,'visible');
            userdata = get(h.selected_axes,'userdata');
            
            spectre_userdata = get(s,'userdata');
            
            if strcmpi(visible,'on')
                limx = spectre_userdata.limx;
                show_in_graph({name});
                userdata.display = name;
                %                 userdata.event_display = 'true';
                try
                set(h.selected_axes,'userdata',userdata,'xlim',limx);
                catch
                    
                end
                refresh_date_xtick();
                
                display_state('Listo...');
                
                delete(s);
                s = get_spectrogram();
                if ~isempty(s)
                    delete(s);
                end
                refresh_fast_buttons_color();
                refresh_filtername_text;
                return;
            end
%             return;
        end
        p_zoom = get_signal_plot(h.selected_axes,'event_zoom');
        if isempty(p_zoom)
            spectre = draw_spect(p);
        else
            spectre = draw_spect(p_zoom);
        end
        
        %Recuerda el xlim
        s_ud = get(spectre,'userdata');
        s_ud.limx = limx;
        set(spectre,'userdata',s_ud);
        
        userdata = get(h.selected_axes,'userdata');
        userdata.display = 'spectre';
        %         userdata.event_display = 'false';
        set(h.selected_axes,'userdata',userdata);
        %         remember_limits(p);
        s = get_spectrogram();
        if ~isempty(s)
            delete(s);
        end
        refresh_fast_buttons_color();
    end
end
display_state('Listo...');
% refresh_date_xtick();
end

