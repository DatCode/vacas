function cp = abstorelpoint(cp,el_handle)
% parents = get_parents(el_handle);
% n = length(parents);
% if n>3
%     parents = parents(1:end-1);
%     n = length(parents);
%     for i = 1 : n
%         cp = get_rel_point(cp,parents(end-i));
%     end
%     
% else
%     cp = get_rel_point(cp,el_handle);
% end

abs_pos = absolute_position(el_handle);
cp = cp-abs_pos(1:2);
end

function point = get_rel_point(rel_point,el)
pos = absolute_position(el);
el_point = pos(1:2);
point = rel_point-el_point;
end