function clear_images(ax)
if nargin<1
   global h;
   ax = h.selected_signal;
end
ch = get(ax,'children');
types = get(ch,'type');
idx = find(strcmpi(types,'image'));
delete(ch(idx));
end

