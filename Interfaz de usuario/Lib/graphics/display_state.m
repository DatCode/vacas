%CopyRight (c), 2015 Jose Martinez Lavados
function display_state(state)
global estado;
global h;
if nargin<1
   state = 'Listo..'; 
end

labelStr = ['<HTML><FONT>' state '</Font></html>'];
jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
dim_label = [10 5 250 20];
try
delete(estado.state_label);
catch
end
estado.state = labelStr;
estado.state_label = javacomponent(jLabel,dim_label,h.information_panel);
pause(0.001);
end