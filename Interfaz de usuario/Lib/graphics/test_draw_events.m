% test draw_events.
clear all
close all
% t=0:.001:10;
% data=(sin(10*t)-cos(30*t-60)).*tan(150*t);
% E={'TR';'LP';'VT';'TE';'OT';'AV'};
% T={ 0 , 1;...
%     2 , 3;...
%     4 , 5;...
%     4.5, 7;...
%     8 , 9;...
%     9 , 10 };
user='jose';
pass = '59575351';
t1=datenum(2014,02,05,20,10,00);
t2 = datenum(2014,02,05,20,20,00);
[signal,t,samprate]=getWaveSQL(t1,t2);
[T,E,N,G,C,F,CE,CT] = findEvent(t1,t2,user, pass);

% event_=struct('tipo',E,'tiempos',T);
event_.tipo = E;
event_.tiempos=T;
f=figure;
a=axes;
% plot(a,t,data,'r',t,data+100);
plot(a,t,signal);
h = patch([t(1) t(floor(length(t)/2)) t(floor(length(t)/2)) t(1)],[ min(signal)/2  min(signal)/2 max(signal)/2 max(signal)/2],'red')
set(gcf,'renderer','zbuffer')
% opengl hardwarebasic
alpha 0.7;
delete(h)
[ handles_rectangles , eventos ] = draw_events (a,event_)

% probar aqu� la funci�n toggle_rect_events()
