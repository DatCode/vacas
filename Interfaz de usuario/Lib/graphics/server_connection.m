%CopyRight (c), 2015 Jose Martinez Lavados
function server_connection(varargin)
% server_conection: Funcion creada para manejar etiqueta de verificacion de
% la coneccion con el servidor, para la interf�z grafica.
% Input
%   la funcion no posee argumentos de entrada
%
% Output
% - connection:
% global h;
global h;
global con;

online  = '#0B610B';      % Verde
offline = '#FF0000';      % Rojo
% dbSource = 'cesar';

if nargin<3
    
    % detectando si la conexi�n fue abierta.
    if con.check_connection()
        % conectado al servidor
        labelStr = ['<HTML><FONT color=' online '>&#8226;ONLINE</Font></html>'];
        jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
        set(h.information_panel,'units','pixels');
        dim_figure = get(h.information_panel,'position');
        dim_label = [dim_figure(3)-100 10 50 20];
        try
            delete(h.server_connection);
        catch
        end
        [componente h.server_connection] = javacomponent(jLabel,dim_label,h.information_panel);
%         close(connection);
    else
        % conexion interrumpida, no hay conexi�n con el servidor
        labelStr = ['<HTML><FONT color=' offline '>&#8226;OFFLINE</Font></html>'];
        jLabel = javaObjectEDT('javax.swing.JLabel',labelStr);
        set(h.information_panel,'units','pixels');
        dim_figure = get(h.information_panel,'position');
        dim_label = [dim_figure(3)-100 10 50 20];
        try
            delete(h.server_connection);
        catch
        end
        [componente h.server_connection] = javacomponent(jLabel,dim_label,h.information_panel);
    end
end

end