function refresh_marks()
global h;
if valid_field(h,'selected_axes',0)
   ud = get(h.selected_axes,'userdata');
   limy = get(h.selected_axes,'ylim');
   set(ud.marklines,'ydata',limy);
   
   set(h.menu(2,7),'check',ud.markvisibility);
   set(ud.marklines,'visible',ud.markvisibility);
   uistack(ud.marklines,'top');
end
end