%% Chequea si un elemento sobrepasa los limites del recuadro del padre
function checknewaxesbounds(ax)
global h;
if nargin<1
%     ax  = gca;
[~,idx]=min(cellfun(@(x) x(2),arrayfun(@(x) get(x,'position'),h.ax,'uniformoutput',false)));
ax=h.ax(idx);
end
%Obtiene posiciones
pos = get(ax,'position');
panpos = get(h.axes_panel,'position');

%Define parametros slider
pan_width = panpos(3);
pan_height = panpos(4);
max_slide = abs(pos(2))+50;
slider_height_rate = pan_height/max_slide;
if max_slide>=150
    slider_step = 150/max_slide;
else
    slider_step = 0.5;
end

%Crea el slider en caso de ser necesario
if pos(2)<0
    %Crea el slider
    try
        delete(h.slider);
    catch
        disp('Cant delete slider');
    end
    h.slider = uicontrol('style','slider','parent',h.fig,'position',[panpos(1)+pan_width-20 panpos(2) 20 pan_height],...
        'backgroundcolor',[.8 .8 .8], 'Foregroundcolor',[.1 .1 .1], 'callback',{ @slider_event, max_slide},...
        'max',max_slide,'value',max_slide,'sliderstep',[slider_step slider_height_rate]);
else
    try
        delete(h.slider);
        h.slider = [];
    catch
        disp('Cant delete slider');
        h.slider = [];
    end
end
end