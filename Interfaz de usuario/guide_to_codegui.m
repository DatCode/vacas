function guide_to_codegui(filename,handles,new_name)
x = textread([filename '.m'], '%s','whitespace',' ','endofline','\n')';
texto = strjoin(x,' ');
global_var = generate_random_str(10);
new_lines = strfind(texto,sprintf('\r'))+2;
new_lines = new_lines(1:end-1);
i=1;
while(i<=length(new_lines))
    if strcmpi(texto(new_lines(i)),'%')
        if i ~=length(new_lines)
            texto = [texto(1:new_lines(i)-1) texto(new_lines(i+1):end)];
        else
            texto = texto(1:new_lines(i)-1);
        end
        new_lines = strfind(texto,sprintf('\r'))+2;
        new_lines = new_lines(1:end-1);
        i=0;
    end
    i = i+1;
end
idx2 =regexp(texto,'function\s+\w+Callback','end')+1;
idx3 =regexp(texto,'Callback\([\s+]?hObject,[\s+]?eventdata[\s+]?,[\s+]?handles[\s+]?\)','end');
i=1;
if ~isempty(idx3) && ~isempty(idx2)
    while(i<=length(idx2))
        slice =texto(idx2(i):idx3(i));
        disp(texto(idx2(i)-30:idx3(i)));
        match = regexp(slice,'\([\s+]?hObject','start');
        if ~isempty(match)
            match = match(1);
            if match ==1
                largo = length(texto(idx2(i):idx3(i)));
                try
                    %             disp([texto(idx3(i)-59:idx3(i)-9) 'handles' texto(idx3(i)+30)]);
                catch
                end
                add_str = sprintf('varargin)%s\n%s\n','',['global ' global_var ';handles=' global_var ';']);
                texto = [texto(1:idx2(i))...
                    add_str texto(idx3(i)+1:end)];
                extra_space = length ( add_str);
                idx3(i)=idx2(i)+1;
                idx3(1:length(idx3)>i)=idx3(1:length(idx3)>i)-largo+1+extra_space;
                idx2(1:length(idx2)>i)=idx2(1:length(idx2)>i)-largo+1+extra_space;
                %             disp([texto(idx2(i)-30) 'handles' texto(idx3(i))]);
            else
                idx2=idx2(idx2~=idx2(i));
                i=i-1;
            end
        else
            idx2=idx2(idx2~=idx2(i));
            i=i-1;
        end
        i=i+1;
    end
    try
        fid = fopen([new_name '.m'],'w');
        str = [set_instruction_for_handles(handles) ['global ' global_var ';' global_var '=handles;']];
        fwrite(fid,sprintf('%s\n',['function varargout = ' new_name '(varargin)']));
        cellfun(@(x) fwrite(fid,sprintf('%s\n',x)),str);
        idx =regexp(texto,'function\s+\w+_Callback\([\s+]?varargin','start');
        idx2 = regexp(texto,'function\s+\w+\(','start');
        idx2 = idx2(idx2>idx(1));
        
        for i = 1:length(idx)-1
            idx2=idx2(idx2>idx(i));
            fwrite(fid,texto(idx(i):idx2(find(min(idx2-idx(i))))-1));
        end
        fwrite(fid,texto(idx(end):end));
    catch
        warning('Error: No se ha logrado convertir la gui creada por guide a un codigo .m');
        
    end
    try
        fclose(fid);
    catch
    end
end
end