function hour=fix_excel_hour(hour)
idx_ok = cellfun(@(x) isExcelHour(x),hour);
hour(~idx_ok)=cellfun(@(x) num2str(datenum(['0000-01-00 ' strtrim(x)],'yyyy-mm-dd HH:MM:SS'))...
    ,hour(~idx_ok),'uniformoutput',false);
hour = cellfun(@(x) str2double(x),hour);
end

function bool = isExcelHour(x)
    x=strtrim(x);
    bool = false;
    if ischar(x)
        if length(x)>1
            if strcmpi(x(2),'.')
                bool = true;
            end
        else
            if strcmpi(x,'0')
                bool = true;
            end
        end
    else
        if isnumeric(x)
            bool = x>=0 & x<1;
        end
    end
end