function writeAxesInfo()
global h;
global colores;
ud = get(h.selected_axes,'userdata');
html = '<html>';
ehtml = '</html>';
try
p = ['<p style = "color:#' num2str(rgb_to_hex(colores.(lower(evento.tipo{1})))) '">'];
catch
   p= ['<p style = "color:#' num2str(rgb_to_hex(colores.unknow)) '">'];
end

str={[html p strong('Predial: ') ud.predial '</p>' ehtml];...
    [html strong('Eje: ') ud.eje ehtml];...
    [html strong('Sensor: ') ud.sensor ehtml];...
    '------------------------------'};
set(h.dynamic_event_data,'string',str);
set(h.dynamic_event_data,'value',length(str));
set(h.dynamic_event_data,'enable','on');
end