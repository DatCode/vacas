clear
close all
clc
set_cow_connector('ricardo','toffee2007');
[etiquetas,s]=get_etiquetas(datenum(2015,7,1),datenum(2016,7,1));
clases=unique(etiquetas.clase);
idx_CO=strcmpi(etiquetas.clase,'CO');
idx_CR=strcmpi(etiquetas.clase,'CR');
idx_DE=strcmpi(etiquetas.clase,'DE');
idx_GO=strcmpi(etiquetas.clase,'GO');
idx_OT=strcmpi(etiquetas.clase,'OT');
idx_aceptaMN = strcmpi(etiquetas.clase,'MN')&strcmpi(etiquetas.subclase,'montada');
idx_otraMN = strcmpi(etiquetas.clase,'MN')&~strcmpi(etiquetas.subclase,'montada');
CO=sum(idx_CO)
CR=sum(idx_CR)
DE=sum(idx_DE)
GO=sum(idx_GO)
OT=sum(idx_OT)
aceptaMN=sum(idx_aceptaMN)
otraMN=sum(idx_otraMN)

eti_CO = get_events_by_idx(etiquetas, idx_CO);
eti_CR = get_events_by_idx(etiquetas, idx_CR);
eti_DE = get_events_by_idx(etiquetas, idx_DE);
eti_GO = get_events_by_idx(etiquetas, idx_GO);
eti_OT = get_events_by_idx(etiquetas, idx_OT);
eti_aceptaMN = get_events_by_idx(etiquetas, idx_aceptaMN);
eti_otraMN = get_events_by_idx(etiquetas, idx_otraMN);

for i=1:length(eti_CR.id_etiqueta)
    ti = eti_CR.inicio_senial{i,1};
    tf = eti_CR.fin_senial{i,1};
    predial = eti_CR.id_predial{i,1};
    senal_CR{i,1}=sel_signal(ti,tf,predial,'acelerometro');
    if ~isempty(senal_CR{i,1})
        senal_CR{i,1}.predial=predial;
    end
end