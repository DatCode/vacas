addpath('C:\Users\Volcanes\Documents\etiquetas vacas agosto septiembre 2016');
files = {'Copia-de-Experimento-1-18-19-de-Agosto.xlsx',...
    'Copia-de-Experimento-2-25-29-de-Agosto.xlsx',...
    'Copia-de-Experimento-3-7-9-de-Septiembre.xlsx',...
    'Copia-de-Experimento-3-9-12-Septiembre.xlsx'};
for i=1:length(files)
    files{i}
    [status,sheets] = xlsfinfo(files{i})
    for j = 1 : length(sheets)-1        
        %Lee excel
        t = readtable(files{i},'sheet',sheets{j});
        fecha = t{:,1};
        fecha = fecha(~strcmpi(fecha,''));
        nrows = length(fecha);
        t= t(1:nrows,:);
        
        %Adapta horas excel a datenum
        t.Fecha = datenum(t.Fecha,'dd/mm/yyyy');
        
        if iscell(t.HoraInicioVideo)
           t.HoraInicioVideo = fix_excel_hour(t.HoraInicioVideo);
        end
        
        if iscell(t.HoraInicioSe_al)
           t.HoraInicioSe_al = fix_excel_hour(t.HoraInicioSe_al);
        end
        
        if iscell(t.HoraFinSe_al)
           t.HoraFinSe_al = fix_excel_hour(t.HoraFinSe_al);
        end
        
        if iscell(t.HoraFinVideo)
           t.HoraFinVideo = fix_excel_hour(t.HoraFinVideo);
        end
        
        t.HoraInicioSe_al = t.HoraInicioSe_al+t.Fecha;
        t.HoraFinSe_al = t.HoraFinSe_al+t.Fecha;
        t.HoraInicioVideo = t.HoraInicioVideo+t.Fecha;
        t.HoraFinVideo = t.HoraFinVideo+t.Fecha;
        
        %Crea estructura etiquetas
        etiquetas.inicio_senial = t.HoraInicioSe_al;
        etiquetas.fin_senial = t.HoraFinSe_al;
        subclase = arrayfun(@(x)getSubclaseByIndex(x),t.SubClase);
        clase = arrayfun(@(x)getClaseByIndex(x),cell2mat([subclase.id_clase]));
        etiquetas.clase = [clase.alias]';
        etiquetas.subclase = [subclase.subclase]';
        etiquetas.inicio_video = t.HoraInicioVideo;
        etiquetas.fin_video = t.HoraInicioVideo;
        etiquetas.comentarios = t.Comentario;
        etiquetas.id_predial = t.IDVaca;
        
        if isnumeric(etiquetas.id_predial)
           etiquetas.id_predial = arrayfun(@(x) num2str(x),...
               etiquetas.id_predial,'uniformoutput',false); 
        end
        
        %Inserta etiquetas
        addCowEvent(etiquetas);
    end
end