%Copyright � 2015, Jos� Mart�nez Lavados, All rights reserved
function interfaz_usuario_vacas(varargin)
%% A�ade las carpetas a la lista de funciones disponibles
addpath('Lib/');
addpath('Lib/fast_button_events/');
addpath('images/');
addpath('Lib/dialogs/');
% addpath('..\Source\ProcSenales\');
addpath('..\source\sql\');
% addpath('../Source/SQL/sql functions/');
% addpath('..\Source\SQL\etiquetas\');
addpath('..\source\time functions\');
% addpath('..\GUI_Historico');
addpath('Lib\signal_processing\');
addpath('Lib\graphics\');
addpath('Lib\handling_labels\');
addpath('Lib\handles_functions\');
addpath('Lib\filters\');
addpath(genpath('../video gui/'));
%btn: 23x3.1
%input:
%texto : 2.1
%lista: 3
%% Inicializa variables
bg_color = [ 0.95    0.95   0.95];
global h; %Handles h.current_signal h.selected_signal
h.display_event_axes = [];
h.selected_axes = 0;
global flag;flag.extending_signal = false; %Flags
global clicktime;
global cb; %Callbacks
flag.choosing_axes = false;
flag.selecting_signal = false;
flag.choosing_sync_axes = false;
global isdragging;
isdragging=false;
global session;
% global events;
% global filter_user;
% filter_user = false;
h.counter_object=0;% Handle del objeto escogido para contar eventos
global event_filter;event_filter = [];
global sel_color; sel_color = [.1 .78 .1];
global axes_height; axes_height = 150;
% global original_signal_color;original_signal_color = [.8 .8 .8];
global tick_per_label;tick_per_label=10;
global colores;
global font_size;font_size.buttons=10;font_size.listbox=10;
% global selected_signal;
colores.cr =     [255 255 0]./255;    % verde
colores.co =     [255 0 0]./255;    % Rojo
colores.go =     [0 255 255]./255;    % Amarillo
colores.mn =     [0.412, 0.412, 0.412];    % gris
colores.ot =     [.2 .2 .6];    % rosado
colores.de =     [0.4941    0.1843    0.5569]; %morado
colores.unknow = [.2 .2 .6];    % gris
colores.background_for_events = [.7 .7 .7];
colores.original_signal = [.8 .8 .8];
colores.selected_signal = [.1 .78 .1];
colores.filtered_signal = [0 0 1];
colores.select_event_edge = [0 0 0];
colores.selected_axes=[0 0 1];
colores.sync_axes = [.5 0 .5];
colores.crosshairline = [0 0 0];
colores.crosshairline_spectrogram = [0.7 0.3 0.1];
colores.markline = [0.5 0.2 0.9];
colores.sel_fast_btn = [1 0 0];
colores.nonsel_fast_btn = [0 0 0];
colores.spectre = colores.filtered_signal;
global timers; timers = [];
global selected_filter;
selected_filter ='Filtro_pasabanda_2_12_orden10';
%% Inicia sesion
[user,pass,grupo] = iniciar_sesion();
% iniciar_sesion;
if isempty(user)
    quit
else
%     set_conector('cesar',user,pass);
    % set_conector('local','root','');
end
session.user = user;
session.pass = pass;
session.grupo = grupo;

%% Crea la figura
h.fig = figure('windowbuttonmotionfcn',@mouse_move,...
    'windowbuttondownfcn',@buttondown,'windowbuttonupfcn',@buttonup,...
    'units','pixels','numbertitle','off','toolbar','none',...
    'menu','none','name','Vulcasufro','closerequestfcn',@close_gui,'resize','on',...
    'windowscrollwheelfcn',@scroll_fig,'color',[0.2 0.2 0.6],'renderer','painters');
colormap(h.fig,'jet');
pause(0.5)
jf = get(h.fig,'javaframe');
set(jf,'Maximized',1);
pause(1)
figuresize = get(h.fig,'position');
fwidth = figuresize(3);
fheight = figuresize(4);
%% Crea la barra de menus
% h.menu(1,1) = uimenu(h.fig,'label','Editar');
% h.menu(1,2) = uimenu(h.menu(1,1),'label','Copiar senial a clipboard');
% h.menu(1,3) = uimenu(h.menu(1,1),'label','Pegar senial');
h.menu(2,1)=uimenu(h.fig,'label','Grafico');
h.menu(2,2)=uimenu(h.menu(2,1),'label','Mover hacia arriba','callback','move_up');
h.menu(2,3)=uimenu(h.menu(2,1),'label','Mover hacia abajo','callback','move_down');
% h.menu(2,4)=uimenu(h.menu(2,1),'label','Mostrar senial original','checked','off','separator','on','callback','toggle_original_signal');
% h.menu(2,4)=uimenu(h.menu(2,1),'label','Cambiar estacion');
% h.menu(2,5)=uimenu(h.menu(2,1),'label','Cambiar componente');
h.menu(2,6)=uimenu(h.menu(2,1),'label','Eliminar marcas','separator','on','callback','delete_marks');
h.menu(2,7)=uimenu(h.menu(2,1),'label','Mostrar marcas','callback','toggle_marks','checked','on');
h.menu(2,8)=uimenu(h.menu(2,1),'label','Mostrar etiquetas','callback','display_events_in_ax','separator','on','checked','on');
h.menu(3,1) = uimenu(h.fig,'label','Filtros');
h.menu(3,2) = uimenu(h.menu(3,1),'label','Agregar filtro propio','callback','add_filter');
h.menu(3,3) = uimenu(h.menu(3,1),'label','Crear filtro','callback','filterbuilder');
h.menu(3,4) = uimenu(h.menu(3,1),'label','Escoger filtro','callback','filter_select');
h.menu(3,5) = uimenu(h.menu(3,1),'label','Filtrar','callback','filter_current_axes');
% h.menu(4,1)=uimenu(h.fig,'label','Configuracion');
h.menu(5,1)=uimenu(h.fig,'label','Ayuda');
h.menu(5,2) = uimenu(h.menu(5,1),'label','Version','callback',@version_fig);
% h.menu(5,1)=uimenu(h.fig,'label','Log out');
% image_button(h.fig,'latido.jpg',[20 20 200 200],@say_hello);

%% Crea los paneles
h.button_panel = uipanel('units','pixels','position',[148 fheight-76 fwidth-396 51],...
    'backgroundcolor',bg_color,'bordertype','none');
% h.button_panel = uipanel('units','normalized','position',100*[148 fheight-60 fwidth-396 60]/1320,...
%     'backgroundcolor',[.2 .2 .6]);
h.event_count_panel =uipanel('units','pixels','position',[0 0 150 fheight-25],...
    'backgroundcolor',bg_color);
h.option_panel = uipanel('units','pixels','position',[fwidth-250 0 252 fheight-25],...
    'backgroundcolor',bg_color);
h.axes_panel = uipanel('units','pixels','position',[148 40 fwidth-396 fheight-101],...
    'backgroundcolor',bg_color);
h.information_panel = uipanel('units','pixels','position',[148 0 fwidth-396 40+1],...
    'backgroundcolor',bg_color);

%% Crea los botones rapidos
% h.fast_button(1) = image_button(h.button_panel,'latido.jpg',[20 20 50 50],@say_hello);

%% Crea los contadores
echeight = 35;
ecwidth = 100;
ectheight = 35;
ecsdistance = 30;
bg = colores.background_for_events;
h.event_count_title(1) = uicontrol('style','text','position',[25 fheight-115-echeight ecwidth ectheight],'String','Total',...
    'backgroundcolor',bg,'foregroundcolor',[0 0 0],'fontweight','bold','fontsize',22);
h.event_count(1) = uicontrol('style','text','position',[25 fheight-115-2*ectheight ecwidth ectheight],'String','0',...
    'backgroundcolor',[0 0 0],'foregroundcolor',[.2 .6 .9],'fontsize',20);

h.event_count_title(2) = uicontrol('style','text','position',[25 fheight-115-echeight*3-ecsdistance ecwidth ectheight],'String','OT',...
    'backgroundcolor',bg,'foregroundcolor',[.2 .2 .6],'fontweight','bold','fontsize',22);
h.event_count(2) = uicontrol('style','text','position',[25 fheight-115-echeight*4-ecsdistance ecwidth ectheight],'String','0',...
    'backgroundcolor',[0 0 0],'foregroundcolor',[.2 .6 .9],'fontsize',20);

h.event_count_title(3) = uicontrol('style','text','position',[25 fheight-115-echeight*5-ecsdistance*2 ecwidth ectheight],'String','TR',...
    'backgroundcolor',bg,'foregroundcolor',[0, 1, 1],'fontweight','bold','fontsize',22);
h.event_count(3) = uicontrol('style','text','position',[25 fheight-115-echeight*6-ecsdistance*2 ecwidth echeight],'String','0',...
    'backgroundcolor',[0 0 0],'foregroundcolor',[.2 .6 .9],'fontsize',20);

h.event_count_title(4) = uicontrol('style','text','position',[25 fheight-115-echeight*7-ecsdistance*3 ecwidth ectheight],'String','VT',...
    'backgroundcolor',bg,'foregroundcolor',[1, 0, 0],'fontweight','bold','fontsize',22);
h.event_count(4) = uicontrol('style','text','position',[25 fheight-115-echeight*8-ecsdistance*3 ecwidth echeight],'String','0',...
    'backgroundcolor',[0 0 0],'foregroundcolor',[.2 .6 .9],'fontsize',20);

h.event_count_title(5) = uicontrol('style','text','position',[25 fheight-115-echeight*9-ecsdistance*4  ecwidth ectheight],'String','LP',...
    'backgroundcolor',bg,'foregroundcolor',[1, 1, 0],'fontweight','bold','fontsize',22);
h.event_count(5) = uicontrol('style','text','position',[25 fheight-115-echeight*10-ecsdistance*4  ecwidth echeight],'String','0',...
    'backgroundcolor',[0 0 0],'foregroundcolor',[.2 .6 .9],'fontsize',20);

h.event_count_title(6) = uicontrol('style','text','position',[25 fheight-115-echeight*11-ecsdistance*5 ecwidth ectheight],'String','TC',...
    'backgroundcolor',bg,'foregroundcolor',[0.412, 0.412, 0.412],'fontweight','bold','fontsize',22);
h.event_count(6) = uicontrol('style','text','position',[25 fheight-115-echeight*12-ecsdistance*5 ecwidth echeight],'String','0',...
    'backgroundcolor',[0 0 0],'foregroundcolor',[.2 .6 .9],'fontsize',20);

%% Crea radio button's del panel de contadores
h.counter_btn_group = uibuttongroup('parent',h.fig,'units','pixels','position',...
    [25-5 fheight-95 ecwidth+10 60],'titleposition',...
    'centertop','title','Fuente','fontsize',font_size.buttons,'fontweight','bold',...
    'selectionchangefcn',@select_counter_origin);
select_list_counter = uicontrol(h.counter_btn_group,'Style','radiobutton','fontsize',font_size.buttons,...
    'String','Lista eventos','Position',[1 25 100 20],'HandleVisibility','off');
select_axes_counter = uicontrol(h.counter_btn_group,'Style','radiobutton','fontsize',font_size.buttons,...
    'String','Grafico','Position',[1 1 100 20],'HandleVisibility','off','value',1);
%% Crea axes de prueba
% for i = 1: 20
%     addAxes(h.axes_panel,[fwidth-397-70 200]);
%     p=random_plot();
%     remember_limits(p);
% end

%% Crea botones rapidos
im_names = {'choose_axes.png','slide_plot.png','sound_wave.png','spectre.png','spectrogram.jpg',...
    'event_zoom.png','zoom_in.png','normal_zoom.png','link-closed-solid.png','unlink.png','step_back.jpg',...
    'step_forward.png','close.png'};
ud = {'hand','select_signal','add_axes','spectre','spectrogram','normal_zoom','event_zoom','zoom_in',...
    'synchronize_axes','unlink_axe','step_back','step_forward','close_axes'};
group = {1,1,0,2,2,3,0,0,1,1,0,0,0};
tooltip = repmat({'Sirve para algo'},length(group));
cb.fast_button = {@toggle_choosing_axes,@toggle_selecting_signal,@signal_request,@show_spectre,@show_spectrogram,...
    @event_zoom,@zoom_in,@normal_zoom,@toggle_synchronize_axes,@unlink_current_axes,@step_back,@step_forward,@close_current_axes};
btn_width = 35;
pan_pos = get(h.button_panel,'position');
pan_height = pan_pos(4);

for i = 1: length(im_names);
    [h.fast_button(i),h.fast_btn_ax(i)] = image_button(h.button_panel,[(btn_width+1)*(i-1) pan_height-btn_width+1 btn_width btn_width+1],cb.fast_button{i},im_names{i});
    userdata=[];
    userdata.name = ud{i};
    userdata.group = group{i};
    userdata.tooltip = tooltip{i};
    set(h.fast_button(i),'userdata',userdata);
end

%% Crea los componentes para el panel de eventos
% h.option_panel = uipanel('units','pixels','position',[fwidth-250 0 252 fheight+2],...
%     'backgroundcolor',bg_color);
uicontrol('style','text','units','pixels','position',...
    [20 fheight-50 212 20],'parent',h.option_panel,'string',{'Tipo|Usuario|Inicio'},'fontsize',10);
h.event_listbox = uicontrol('style','listbox','units','pixels','position',...
    [20 fheight-260 212 210],'parent',h.option_panel,'fontsize',12,'backgroundcolor',colores.background_for_events,...
    'callback',@sel_event_from_list,'fontsize',font_size.listbox);
h.search_event_button = uicontrol('style','pushbutton','units','pixels','position',...
    [19 fheight-289 floor(212/3)+1 40],'string','Buscar','parent',h.option_panel,'callback',@search_events,...
    'fontsize',font_size.buttons);
h.display_event_button = uicontrol('style','pushbutton','units','pixels','position',...
    [19+floor(212/3)+1 fheight-289 floor(212/3)+1 40],'string','Mostrar','parent',h.option_panel,...
    'fontsize',font_size.buttons,'callback',@list_event_to_graph);
h.filter_event_button = uicontrol('style','pushbutton','units','pixels','position',...
    [19+floor(212/3)*2+2 fheight-289 floor(212/3)+1 40],'string','Filtrar','parent',h.option_panel...
    ,'fontsize',font_size.buttons,'callback',@filter_listbox);

%Crea menu de evento
h.event_menu_panel = uipanel('units','pixels','position',[19 fheight-450 212 150]...
    ,'parent',h.option_panel,'backgroundcolor',bg_color,'title','Menu etiquetas en grafico',...
    'titleposition','centertop','fontsize',12);
h.create_event_button = uicontrol('style','pushbutton','units','pixels','position',...
    [10 70 95 40],'string','Crear','parent',h.event_menu_panel,'fontsize',font_size.buttons,...
    'callback',@openVideoInterface);
h.modify_event_button = uicontrol('style','pushbutton','units','pixels','position',...
    [10 10 95 40],'string','Modificar','parent',h.event_menu_panel,'fontsize',font_size.buttons,...
    'callback',@openVideoInterface);
h.refresh_event_button = uicontrol('style','pushbutton','units','pixels','position',...
    [105 70 95 40],'string','Refrescar','parent',h.event_menu_panel,'fontsize',font_size.buttons,...
    'callback',@refresh_current_ax_events);
h.delete_event_button = uicontrol('style','pushbutton','units','pixels','position',...
    [105 10 95 40],'string','Eliminar','parent',h.event_menu_panel,'fontsize',font_size.buttons,...
    'callback',@delete_event_dlg2);

%Crea panel para los datos dinamicos
% [19 10 212 250]
h.dynamic_event_data =uicontrol('style','listbox','units','pixels','position',...
    [19 fheight-725 212 250],'parent',h.option_panel,'string',{'Tipo:';'Creador:';'Revisor:';...
    'Frecuencia:';'Amplitud:';'Inicio:';'Fin';'Duracion:';'Fecha modificacion:'},'fontsize',10,...
    'backgroundcolor',colores.background_for_events,'fontsize',font_size.listbox);

state = 'Listo..';
display_state(state);

timers.check_con_timer = timer('TimerFcn', {@update},...
    'StartDelay',2,'period',15,...
    'ExecutionMode', 'FixedSpacing',...
    'BusyMode','queue');
start(timers.check_con_timer)

%% Cambia el icono de la figura
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jframe=get(h.fig,'javaframe');
jIcon=javax.swing.ImageIcon([pwd '\images\icono_volcan.png']);
jframe.setFigureIcon(jIcon);

%% Hace que la figura sea autoresizable
% set_global_units('normalized');
toggle_choosing_axes();
clear_dynamic_event();
end

function update(hObj, eventData)
server_connection();
end

function mouse_move(src,evt)
global isdragging;
global clickup;
global h;
global timers;

%reset timer tooltip
try
    stop(timers.tooltip);
    delete(timers.tooltip);    
catch
end
% disp('Elimina timer');
timers.tooltip = [];

%Cambia el cursor
ax=refresh_cursor();

%delete tooltip
try
    delete(h.tooltip);    
catch
end
h.tooltip = -1;

%Cuando esta draggeando
if(isdragging)
    isdragging = false;
    
    %Chequea si esta sobre el axes seleccionado
    if ishandle(ax)
        if ax==h.selected_axes
            repaint_select();
        end
    end
    
    if ~clickup %Chequea si ya se ha soltado el click
        isdragging = true;
    end
end
end

%Al soltar el click del mouse
function buttonup(src,evt)
global isdragging;
global clickup;
global drag;
global h;

clickup= true;
isdragging = 0;

drag.end = get(src,'currentpoint');

% if(sum(drag.end == drag.start)~=2)
%Elimina el rectangulo al soltar el click
if isfield(h,'rect')
    if(~isempty(h.rect))
        if ishandle(h.rect)
            delete(h.rect);
            h.rect = [];
        end
    end
end
% end
end

function buttondown(src,evt)
global colores;
global isdragging;
global clickup;
global drag;
global h;
global flag;
global clicktime;
try
    clicktime2 = toc(clicktime);
catch
    clicktime2 = 9999;
end
clicktimethreshold = 0.19;
if flag.selecting_signal
    isdragging = 1;
    clickup = false;
    drag.start = get(src,'currentpoint');
    ax = refresh_cursor();
    if ishandle(ax)
        if ax==h.selected_axes
            if ishandle(ax)
                ud = get(ax,'userdata');
                try
                    if clicktime2<clicktimethreshold && ~strcmpi(ud.display,'spectre')
                        linked = ud.linked_ax;
                        for i = 1:length(linked)
                            act_ud = get(linked(i),'userdata');
                            xdata = get(ud.crosshairline,'xdata');
                            ydata = get(ud.crosshairline,'ydata');
                            check = get(h.menu(2,7),'checked');
                            udmark.name = 'markline';
                            %                         udmark.referplot = get_signal_plot('current');
                            act_ud.marklines = [act_ud.marklines;line(xdata,ydata,'color',colores.markline,'parent',...
                                linked(i),'visible',check,'userdata',udmark)];
                            set(linked(i),'userdata',act_ud);
                        end
                    end
                    
                    if ~(strcmpi(ud.display,'spectrogram')||strcmpi(ud.display,'spectre'))
                        if ishover(h.fig,ax) %Chequea que el cursor este sobre el axes
                            p=get_signal_plot(ax,'filtered_signal');%Obtiene el plot con la se�al filtrada
                            clear_selection();
                            [h.rect, sel_time_range] = select_signal(ax,drag.start);%Obtiene el rango de tiempos seleccionado y crea el rect
                            try
                                if length(p)>1
                                    p(1)=brand_selected_signal(p(2),sel_time_range,colores.selected_signal);%Crea el sliced plot
                                else
                                    p(2)=brand_selected_signal(p(1),sel_time_range,colores.selected_signal);%Crea el sliced plot
                                end
                            catch
                                %                         disp('buttondown(): Error: attempt to brand_select_signal');
                            end
                        end
                    end
                catch
                    clickup = true;
                    isdragging = 0;
                end
            end
        else
            if ishandle(ax)
                ax_clicked(ax);
%                 buttondown(src,evt);
            end
        end
    end
else
    ax = is_hover_an_axes();
    if ishandle(ax)
        ax_clicked(ax);
    end
end
clicktime=tic;
end

function scroll_fig(varargin)
global h;

if valid_field(h,'slider',0)
    max_scroll = get(h.slider,'max');
    scroll_rate = max_scroll/100;
    val_actual = get(h.slider,'value');
    val = scroll_rate*varargin{2}.VerticalScrollCount*varargin{2}.VerticalScrollAmount;
    if val_actual-val<0
        val = val_actual;
    end
    
    if val_actual-val>max_scroll
        val = val_actual-max_scroll;
    end
    set(h.slider,'value',val_actual-val);
    
    slider_event(h.slider,[],max_scroll);
end
end

function close_gui(varargin)
delete(varargin{1});
global mytimer;
global cow_connector;
try
    stop(mytimer);
    delete(mytimer);
catch
    warning('No se ha podido eliminar el timer');
end
clear global clickup;
clear global h;
clear global drag;
clear global isdragging;
clear global events;
clear global session;
clear global sel_color;
clear global colores;
clear global new_axes_form;
clear global selected_filter;
clear global tick_per_label;
clear global font_size;
clear global axes_height;
clear global sel_color;
clear global event_filter;
delete('cache/*.mat')
cow_connector.close_session();
clear global cow_connector;
end

function add_custom_axes(varargin)
f = add_axes_form();
waitfor(f);
global new_axes_form;
% global events;

output = new_axes_form;
clear global new_axes_form;

if ~isempty(output)
    %Obtiene los tiempos de inicio y fin
    ti = datenum([output.fecha ' ' output.hora_inicio],'yyyy-mm-dd HH:MM');
    tf = ti + datenum(['0000-01-00 00:' output.minutos ':00'],'yyyy-mm-dd HH:MM:SS');
    
    datos.estacion=output.estacion;
    datos.componente =output.componente;
    datos.ti=ti;
    datos.tf=tf;
    
    sql_plot(datos);
end
end