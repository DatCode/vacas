function Interfaz(varargin)
figure1=figure('CloseRequestFcn','closereq','Color',[0.941176470588235 0.941176470588235 0.941176470588235],'MenuBar','none','Name','add_axes_form','NumberTitle','off','Renderer','painters','Resize','off','Tag','figure1','ToolBar','auto','Units','characters','Visible','on');set(figure1,'position',[103.8 40 134.4 21.4615384615385]);handles.figure1=figure1;
hora_inicio=uicontrol(figure1,'style','edit','BackgroundColor',[1 1 1],'Callback',@hora_inicio_Callback,'FontAngle','normal','FontName','MS Sans Serif','FontSize',12,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','20:10','Tag','hora_inicio','Units','characters','Value',0,'Visible','on');set(hora_inicio,'position',[19.8 9.84615384615385 40.2 3.92307692307692]);handles.hora_inicio=hora_inicio;
lel=uicontrol(figure1,'style','text','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'FontAngle','normal','FontName','MS Sans Serif','FontSize',14,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','Hora','Tag','lel','Units','characters','Value',0,'Visible','on');set(lel,'position',[-0.2 9.84615384615385 20 2.92307692307692]);handles.lel=lel;
Componente=uipanel(figure1,'BorderType','etchedin','BorderWidth',1,'FontAngle','normal','FontName','MS Sans Serif','FontSize',12,'FontUnits','points','FontWeight','bold','ForegroundColor',[0 0 0],'HighlightColor',[1 1 1],'Title','Componente','TitlePosition','lefttop','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'Units','characters','Visible','on','Tag','Componente');set(Componente,'position',[103.8 8.92307692307692 24.2 8.61538461538462]);handles.Componente=Componente;
text8=uicontrol(figure1,'style','text','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'FontAngle','normal','FontName','MS Sans Serif','FontSize',14,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','Minutos','Tag','text8','Units','characters','Value',0,'Visible','on');set(text8,'position',[-0.2 6 20 2.92307692307692]);handles.text8=text8;
fecha=uicontrol(figure1,'style','edit','BackgroundColor',[1 1 1],'Callback',@fecha_Callback,'FontAngle','normal','FontName','MS Sans Serif','FontSize',12,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','2014-02-05','Tag','fecha','Units','characters','Value',0,'Visible','on');set(fecha,'position',[19.8 13.6923076923077 40.2 3.92307692307692]);handles.fecha=fecha;
text6=uicontrol(figure1,'style','text','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'FontAngle','normal','FontName','MS Sans Serif','FontSize',14,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','Fecha','Tag','text6','Units','characters','Value',0,'Visible','on');set(text6,'position',[-0.2 13.6923076923077 20 2.92307692307692]);handles.text6=text6;
cancelar=uicontrol(figure1,'style','pushbutton','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'Callback',@cancelar_Callback,'FontAngle','normal','FontName','MS Sans Serif','FontSize',8,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','Cancelar','Tag','cancelar','Units','characters','Value',1,'Visible','on');set(cancelar,'position',[59.8 0.846153846153846 30.2 3.92307692307692]);handles.cancelar=cancelar;
aceptar=uicontrol(figure1,'style','pushbutton','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'ButtonDownFcn',@aceptar_ButtonDownFcn,'Callback',@aceptar_Callback,'FontAngle','normal','FontName','MS Sans Serif','FontSize',8,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','Aceptar','Tag','aceptar','Units','characters','Value',0,'Visible','on');set(aceptar,'position',[99.8 0.769230769230769 30.2 4]);handles.aceptar=aceptar;
minutos=uicontrol(figure1,'style','edit','BackgroundColor',[1 1 1],'Callback',@minutos_Callback,'FontAngle','normal','FontName','MS Sans Serif','FontSize',12,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','10','Tag','minutos','Units','characters','Value',0,'Visible','on');set(minutos,'position',[19.8 5.92307692307692 40.2 3.92307692307692]);handles.minutos=minutos;
text1=uicontrol(figure1,'style','text','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'FontAngle','normal','FontName','MS Sans Serif','FontSize',14,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','Estacion','Tag','text1','Units','characters','Value',0,'Visible','on');set(text1,'position',[79.8 15.2307692307692 15.2 2.30769230769231]);handles.text1=text1;
estacion=uicontrol(figure1,'style','popupmenu','BackgroundColor',[1 1 1],'Callback',@estacion_Callback,'FontAngle','normal','FontName','MS Sans Serif','FontSize',12,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String',{'LAV','LLA','MOT'},'Tag','estacion','Units','characters','Value',1,'Visible','on');set(estacion,'position',[79.8 13 15.2 2.23076923076923]);handles.estacion=estacion;
radiobutton10=uicontrol(Componente,'style','radiobutton','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'FontAngle','normal','FontName','MS Sans Serif','FontSize',10,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','n','Tag','radiobutton10','Units','characters','Value',0,'Visible','on');set(radiobutton10,'position',[2 0.846153846153848 17.4 1.76923076923077]);handles.radiobutton10=radiobutton10;
radiobutton9=uicontrol(Componente,'style','radiobutton','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'FontAngle','normal','FontName','MS Sans Serif','FontSize',10,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','e','Tag','radiobutton9','Units','characters','Value',0,'Visible','on');set(radiobutton9,'position',[2 2.76923076923077 17.4 1.76923076923077]);handles.radiobutton9=radiobutton9;
radiobutton1=uicontrol(Componente,'style','radiobutton','BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],'FontAngle','normal','FontName','MS Sans Serif','FontSize',10,'FontUnits','points','FontWeight','normal','ForegroundColor',[0 0 0],'ListboxTop',1,'Max',1,'Min',0,'SliderStep',[0.01 0.1],'String','z','Tag','radiobutton1','Units','characters','Value',1,'Visible','on');set(radiobutton1,'position',[2 4.69230769230769 17.4 1.76923076923077]);handles.radiobutton1=radiobutton1;

calendar_button = image_button(figure1,[300 180 50 50],@()uicalendar('destinationui',...
    handles.fecha,'selectiontype',1,'windowstyle','modal','outputdateformat','yyyy-mm-dd'),'calendar.png');

% fid = fopen([mfilename '.m'],'r');
x = textread([mfilename '.m'], '%s','whitespace',' ','endofline','\n')';
texto = strjoin(x,' ');
% funciones = regexp(texto,'function\s\w+_Callback\(hObject,\w+','match');
idx2 =regexp(texto,'function\s+\w+\(','end');
idx3 =regexp(texto,',[\s+]?handles','start');
i=1;
while(i<=length(idx2))
    slice =texto(idx2(i):idx3(i));
    match = regexp(slice,'\([\s+]?hObject','start');
    if ~isempty(match)
        match = match(1);
        if match ==1
            largo = length(texto(idx2(i)+1:idx3(i)));
            texto = [texto(1:idx2(i)) texto(idx3(i)+1:end)];
            idx3(i)=idx2(i)+1;
            idx3(1:length(idx3)>i)=idx3(1:length(idx3)>i)-largo;
            idx2(1:length(idx2)>i)=idx2(1:length(idx2)>i)-largo;
        else
            idx2=idx2(idx2~=idx2(i));
            i=i-1;
        end
    else
        idx2=idx2(idx2~=idx2(i));
        i=i-1;
    end
    i=i+1;
end
idx =regexp(texto,'function\s+\w+_Callback\([\s+]?handles','start');
fid = fopen('prueba.m','w');
for i = 1:length(idx)-1
    fwrite(fid,texto(idx(i):idx(i+1)-1));
end
 fwrite(fid,texto(idx(end):end));
% fclose(fid);
function estacion_Callback(hObject, eventdata, handles)
% hObject handle to estacion (see GCBO)
% eventdata reserved - to be defined in a future version of MATLAB
% handles structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns estacion contents as cell array
% contents{get(hObject,'Value')} returns selected item from estacion f
function minutos_Callback(hObject, eventdata, handles)
% hObject handle to minutos (see GCBO)
% eventdata reserved - to be defined in a future version of MATLAB
% handles structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of minutos as text
% str2double(get(hObject,'String')) returns contents of minutos as a double f
function hora_inicio_Callback(hObject, eventdata, handles)
% hObject handle to hora_inicio (see GCBO)
% eventdata reserved - to be defined in a future version of MATLAB
% handles structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of hora_inicio as text
% str2double(get(hObject,'String')) returns contents of hora_inicio as a double
% --- Executes on button press in aceptar. f
function aceptar_Callback(hObject, eventdata, handles)
% hObject handle to aceptar (see GCBO)
% eventdata reserved - to be defined in a future version of MATLAB
% handles structure with handles and user data (see GUIDATA)
global new_axes_form; sel_object = get(handles.Componente,'selectedobject'); new_axes_form.componente = get(sel_object,'string'); new_axes_form.hora_inicio = get(handles.hora_inicio,'String'); new_axes_form.fecha = get(handles.fecha,'string'); new_axes_form.minutos = get(handles.minutos,'string'); lista = get(handles.estacion,'string'); idx = get(handles.estacion,'value'); new_axes_form.estacion = lista{idx}; delete(handles.figure1); % --- Executes on button press in cancelar. f
function cancelar_Callback(hObject, eventdata, handles)
% hObject handle to cancelar (see GCBO)
% eventdata reserved - to be defined in a future version of MATLAB
% handles structure with handles and user data (see GUIDATA)
global new_axes_form; str = set_instruction_for_handles(handles); cellfun(@(x) disp(x),str); new_axes_form = []; clear global new_axes_form; delete(handles.figure1); f
function fecha_Callback(hObject, eventdata, handles)
% hObject handle to fecha (see GCBO)
% eventdata reserved - to be defined in a future version of MATLAB
% handles structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of fecha as text
% str2double(get(hObject,'String')) returns contents of fecha as a double
% --- Executes on button press in pushbutton3. f