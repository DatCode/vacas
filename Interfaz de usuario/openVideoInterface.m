function openVideoInterface(src,eventData)
global h;
global trans_plots;
trans_plots.ax = h.selected_axes;
trans_plots.p = get_signal_plot(h.selected_axes,'selected_signal');
if ~isempty(trans_plots.p)
   trans_plots.ti = min(get(trans_plots.p,'xdata'));
   trans_plots.tf = max(get(trans_plots.p,'xdata'));
   video_gui(); 
end
end