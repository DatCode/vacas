function url = getVideoUrl(ti,tf)
[lista,dates,~,url]=get_video_list();
hora = datenum(0,0,0,1,0,0);
idx = (ti>=dates & dates+hora>=ti) | (dates<=tf & dates+hora>=tf);

if isempty(url) || sum(idx)==0
    warning('No se ha encontrado el video :(');
    url = [];
else
    url = [url lista{idx}];
end
end