function download_video(t1,t2)
[lista,dates,extentions,url]=get_video_list();
% t1 = datenum(2015,11,11,20,10,00);
% t2=datenum(2015,11,11,20,30,00);
idx=find((dates>=t1 & dates<=t2) | ...
    (dates+datenum(0,0,0,1,0,0)>=t1 & dates+datenum(0,0,0,1,0,0)<=t2)|...
    (dates<=t1 & dates+datenum(0,0,0,1,0,0)>=t1));

h=waitbar(0,'Se verificara si existen ya los videos');
pause(0.5);
try
    for i=1:length(idx)
        download_url = [url lista{idx(i)}];
        request.url = download_url;
        request.autostart=false;
        request.extention = extentions{idx(i)};
        request.name = datestr(dates(idx(i)),'yyyymmdd_HH_MM_SS');
        request.folder = [pwd '\..\Descargas\Videos\'];
        if ~exist([pwd '\..\Descargas\Videos\' datestr(dates(idx(i)),'yyyymmdd_HH_MM_SS') '.' request.extention],'file')
            waitbar((i-1)/length(idx),h,['Descargando video ' datestr(dates(idx(i)),'yyyymmdd_HH_MM_SS') '.' request.extention]);
            download_file(request);
        else
            disp('El archivo ya existia');
        end
    end
    watibar(1,h,'Listo!');
    pause(1);
    close(h);
catch
    close(h);
end
end