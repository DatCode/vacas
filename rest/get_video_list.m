function [lista,dates,extentions,url]=get_video_list()

url =  'http://25.88.187.14/Videos/videos2/fixed/';
% url = 'http://146.83.206.92/Videos/videos2/fixed/';
% url = 'http://146.83.206.92/Videos/videos2/Originales/';
try
M=webread(url);
catch
    M=urlread(url);
end

M=strrep(M,'/table','');
M=strrep(M,'table','');
M=strrep(M,'/tr','');
M=strrep(M,'tr','');
M=strrep(M,'/td','');
M=strrep(M,'td','');
M=strrep(M,'< valign="top"><img src="/icons/movie.gif" alt="[VID]">','');
M=strrep(M,'<><><>','');

expression = '(\d+_\d+-\d+-\d+.\S+\S+)[\"]';
a= regexp(M,expression,'match');
a = strrep(a,'"','');

dates = regexp(a,'\d+_\d+-\d+-\d+','match');
dates = arrayfun(@(i) dates{i}{1},1:length(dates),'UniformOutput',false);
dates=datenum(dates,'yyyymmdd_HH-MM-SS');

b=regexp(a,'[m]\S+','match');
b=arrayfun(@(i) b{i}{1},1:length(b),'UniformOutput',false);

lista = a;
% dates=dates;
extentions = b;
end